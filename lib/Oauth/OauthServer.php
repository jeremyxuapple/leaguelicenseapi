<?php
/**
 * This is the server object for Oauth 
 */

namespace LL\lib\Oauth;

use Oauth2;
use Oauth2\Storage;
use Oauth2\GrantType;
use LL\lib\database;

class OauthServer
{
    public function __construct()
    {
        # register Oauth autoloader
        \OAuth2\Autoloader::register();

        # we create a different database object for the oauth - but it's same credentials
        $constr = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;

        $this->storage = new \OAuth2\Storage\Pdo(array('dsn' => $constr, 'username' => DB_USER, 'password' => DB_PASSWORD));

        // Pass a storage object or array of storage objects to the OAuth2 server class
        $this->server = new \OAuth2\Server($this->storage);

        // Add the "Client Credentials" grant type (it is the simplest of the grant types)
        $this->server->addGrantType(new \OAuth2\GrantType\ClientCredentials($this->storage));

        // Add the "Authorization Code" grant type (this is where the oauth magic happens)
        $this->server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($this->storage));
    }

    /**
     * This will generate a token and associate it with a specific user
     *
     * @return $token - this will output the client credentials exactly how oauth needs
     *
     */
    public function handleTokenRequest()
    {
        $request = \OAuth2\Request::createFromGlobals();
        $response = new \OAuth2\Response();

        $token = $this->server->handleTokenRequest($request);

        $token->send();
    }

    /**
     * Verifies any given token
     *
     * @return $token - returns token information
     *
     */
    public function verifyToken()
    {
        if (!$this->server->verifyResourceRequest(\OAuth2\Request::createFromGlobals())) {
            throw new \Exception("Invalid API token : ".$_GET['access_token']);
        }

        $token = $this->server->getAccessTokenData(\OAuth2\Request::createFromGlobals());

        return $token;
    }

    public function authorize()
    {
        $request = \OAuth2\Request::createFromGlobals();
        $response = new \OAuth2\Response();

        # validate the authorize request
        if (!$this->server->validateAuthorizeRequest($request, $response)) {
            throw new \Exception("Invalid authorize request");
        }

        # return the authorization code if the user has authorizes your client
        $is_authorized = ($_POST['authorized'] === 'yes');
        $res = $this->server->handleAuthorizeRequest($request, $response, $is_authorized);

        if (!$is_authorized)
            throw new \Exception("Access to API is denied");

        return true;
    }

    /**
     * This will create the client id and secret necessary to create a token
     *
     * @return $client - this will output the client credentials that were just made
     *
     */
    public function createCredentials()
    {
        # init the db
        $this->db = new \LL\lib\database\mysql();

        # let's create the insert array
        $i = array(
            "client_id" => $_POST['username'],
            "client_secret" => $this->generateSecret(),
            "redirect_uri" => $_POST['redirect'],
            "grant_types" => isset($_POST['grant_type']) ? $_POST['grant_type'] : "client_credentials",
            "scope" => isset($_POST['scope']) ? $_POST['scope'] : null,
            "user_id" => $_POST['userid']
        );

        $check = array(
            "client_id" => $_POST['username'],
            "user_id" => $_POST['userid']
        );

        $id = $this->db->CheckInsert("oauth_clients", $i, $check);

        # let's get the full row to return
        $s = "SELECT * FROM oauth_clients WHERE client_id = :client_id AND user_id = :user_id";
        $client = $this->db->FetchOne($s, $check);

        JSO($client);
    }

    /**
     * Generates a client secret
     *
     * @return $string - randomized string 16 characters long
     *
     */
    private function generateSecret()
    {
        return generateRandomString(26);
    }
}