<?php
/**
 * This will route any incoming requests to the appropriate controllers
 */

namespace LL\lib\routers;

use LL\lib\Oauth;

class Router
{
    /**
     * This will route all methods that need an ID
     * Customers, Orders, Products, Suppliers, Accounts
     *
     * @param $method - type of request sent to controller on construct
     * @param $section - which section the controller resides
     * @param $id - specific section id
     *
     * @throws - unable to process request
     *
     */
    public function idRouter($method = 'get', $section, $id = null, $extra = null, $token = false)
    {
        $controller = $section."IdController";
        $c = "\\LL\\controllers\\$section\\$controller";
                
        if (class_exists($c)) {
            
            $section = new $c($method, $id, $extra, $token);
        } else {
            throw new \Exception("Unable to process request: $c");
        }
    }

    public function textRouter($method = 'get', $section, $action = null, $extra = null, $token = false)
    {   
        $controller = $section."textController";
        $c = "\\LL\\controllers\\$section\\$controller";
        
        if (class_exists($c)) {
            $section = new $c($method, $action, $extra, $token);
        } else {
            throw new \Exception("Unable to process request: $c");
        }   
    }

    public function Oauth($section)
    {
        $server = new \LL\lib\Oauth\OauthServer();

        if (method_exists($server, $section)) {
            return $server->$section();
        } else {
            throw new \Exception("Unable to process Oauth request");
        }
    }
   
    public function registerAccountHandler($method = 'post', $section)
    {
        $controller = $section."AccountController";
        $c = "\\LL\\controllers\\$section\\$controller";

        if (class_exists($c)){
            $section = new $c($method);
        }else{
            throw new \Exception("Unable to process request: $c");
        }
    }

    public function webhooksHandler($method = 'post', $token, $section)
    {   
        $controller = $section."Controller";
        $c = "\\LL\\controllers\\$section\\$controller";
        if (class_exists($c)){
            $section = new $c($method, $token);
        } else {
            throw new \Exception("Unable to process request: $c");
        }
     }

     public function betaHandler($method = 'get', $section, $extra)
    {   
        // $section = "beta";
        $controller = $section."Controller";
        $c = "\\LL\\controllers\\$section\\$controller";
        
        if (class_exists($c)) {
            $section = new $c($method, $extra);
        } else {
            throw new \Exception("Unable to process request: $c");
        }
    }

     public function cornHandler($method = 'get', $section, $extra)
    {   
        // $section = "beta";
        $controller = $section."Controller";
        $c = "\\LL\\controllers\\$section\\$controller";
        
        if (class_exists($c)) {
            $section = new $c($method, $extra);
        } else {
            throw new \Exception("Unable to process request: $c");
        }
    }
}