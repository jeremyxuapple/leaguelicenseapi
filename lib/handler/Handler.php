<?php
/**
 * This handler will parse all url requests
 * Pass it over to the router to handle the request
 */

namespace LL\lib\handler;

use LL\lib\routers;

class Handler
{
    private $method;
    private $url;

    /**
     * On construct this reads the current url request as well as an post variables
     *
     */
    public function __construct()
    {
        # first let's determin the method
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);

        # now let's get the url
        $path = $_SERVER['REQUEST_URI'];

        # let's clean off any get requests
        $url = preg_replace('/\?.*/', '', $path);
        $query = preg_replace('/(.*)\?(.*)/', '$2', $path);
        $this->url = trim($url, "/");

        # define the router
        $this->router = new \LL\lib\routers\Router();

        # let's match routes by running the handler function and sending it to the router
        $this->matchRoute();
    }

    /**
     * We have any necessary information set, so now we 
     * will just have to define specific routes
     *
     */
    private function matchRoute()
    {   
        $split = explode("/", $this->url);
       
        # right now we only have an 'api' base so we throw an error if it's not there
        if ($split[0] !== 'api')
            throw new \Exception("Unable to find request");

        # right now we only have 1 version so this doesn't matter as much, but it's for later
        if ($split[1] !== 'v1')
            throw new \Exception("Version unavailable");

        # now we make a different function based on the next split variable
        $method = $this->method;
        $id = isset($split[3]) ? $split[3] : null;
        $extra = isset($split[4]) ? $split[4] : null;

        if (is_numeric($id) || is_null($id)) {
            switch($split[2]) {
                case "management":
                case "warnings":
                case "notifications":
                case "products":
                case "orders":
                case "returns":
                case "suppliers":
                case "accounts":
                case "reports":
                    # verify Oauth token
                    $token = $this->router->Oauth('verifyToken');
                    $this->router->idRouter($method, $split[2], $id, $extra, $token);
                    break;
                case "oauth":
                    $this->oauthHandler($id, $extra);
                    break;
                case "register":
                    # open to public to register an account
                    $this->router->registerAccountHandler($method, $split[2]);
                    break;
                case "webhooks":
                    $token = $this->router->Oauth('verifyToken');
                    $this->router->webhooksHandler($method, $token, $split[2]);
                    break;
                case "beta":
                    $this->router->betaHandler($method, $split[2], $split[3]);
                    break;
                default:
                    throw new \Exception("Unable to find request");
            }
        } else {
            switch($split[2]) {
                case "oauth":
                    $this->oauthHandler($id, $extra);
                    break;
                case "app":
                    $this->appHandler($id, $extra);
                    break;
                case "beta": 
                    $this->router->betaHandler($method, $split[2], $split[3]);
                    break;
                case "corn":
                    $this->router->cornHandler($method, $split[2], $split[3]);
                    break;
                default:
                    throw new \Exception("Unable to find request");
            }
        }
    }

    private function oauthHandler($method, $extra)
    {
        switch($method) {
            case "authorize":
                $this->router->Oauth('authorize');
                break;
            case "token":
                $this->router->Oauth('handleTokenRequest');
                break;
            case "create":
                $this->router->Oauth('createCredentials');
                break;
            default:
                $token = $this->router->Oauth('verifyToken');
                JSO($token);
        }
    }

    private function appHandler($action, $extra)
    {   
        switch($action) {
            case "install":
            case "uninstall":
            case "load":
            case "productredirect":
            case "initSetup":
            case 'savesetup':
            case 'gettest':
            case 'poll':
            case 'getstoreinfo':
            case 'testFunction':
                $this->router->textRouter($this->method, "app", $action, $extra);
                break;
            default:
                throw new \Exception("Unable to complete app task : $action");
        }
    }
}