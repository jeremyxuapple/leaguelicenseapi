<?php
/**
 * This is the db class
 * any specific db will extend this abstract class
 *
 */

namespace LL\lib\database;

interface db {

    /**
     * Construct function initiates the database and also set's config variables
     *
     */
    public function __construct();

    /**
     *  Basic Query
     *
     * @param string $s - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $q - the PDO resource that can converted 
     *
     */
    public function Query($query, $vars);

    /**
     *  Fetch Query
     *
     * @param string $s - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $r - the array of values from the database
     *
     */
    public function Fetch($query, $vars);

    /**
     *  Fetch One Query
     *
     * @param string $s - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $r - a single row from the database
     *
     */
    public function FetchOne($query, $vars);

    /**
     *  Update as Array Query
     *
     * @param string $table - the table name
     * @param array $update - the query that needs to be updated as an array
     * @param string $where - specific where statement
     * @param boolean $vars - variables that need to be inserted into the query
     *
     * @return boolean - true/false if statement was updated
     *
     */
    public function Update($table, $update, $wheres, $rowcount);

    /**
     * Insert into the Database
     *
     * @param table $table - table name
     * @param array $insert - array of key value pairs to insert
     *
     * @return int $id - return the id of the last inserted value
     *
     */
    public function Insert($table, $insert);

    /**
     * Insert into the Database - but we check if it exists before we insert
     *
     * @param table $table - table name
     * @param array $insert - array of key value pairs to insert
     *
     * @return int $id - return the id of the last inserted value
     *
     */
    public function CheckInsert($table, $insert, $wheres, $enableExistsUpdate);

    /**
     * Report error
     *
     * @param string $e - the error
     * @param string $s - The query that got passed throught
     *
     * @return not sure how i'm going to present this yet
     *
     */
    public function ReportError($error, $query);

}
