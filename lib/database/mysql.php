<?php
/**
 * This is the MySQL db class
 *
 */

namespace LL\lib\database;

class mysql implements db {

    /**
     * Construct function initiates the database and also set's config variables
     *
     */
    public function __construct()
    {
        $dbhost = DB_HOST;
        $user = DB_USER;
        $pass = DB_PASSWORD;
        $dbname = DB_NAME;
        $constr = 'mysql:host=' . $dbhost . ';dbname=' . $dbname;
        try {
            $database = new \PDO($constr, $user, $pass);
            $database->beginTransaction();
            $this->db = $database;

        } catch (PDOException $e) {
            throw new DatabaseError("Could not connect to database");
        }
    }

    /**
     * Destruct function
     * This will commit everything available to the database
     * 
     */
    public function __destruct()
    {
        $result = $this->db->commit();
    }

    /**
     *  Basic Query
     *
     * @param string $query - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $q - the PDO resource that can converted 
     *
     */
    public function Query($query, $vars = array())
    {
        $q = $this->db->prepare($query);
       
        # bind all variables the proper way
        foreach ($vars as $k => &$v)
        {
            $q->bindParam(':'.$k, $v);
        }
        
        if (!$q->execute()) {
            $this->ReportError($q->errorInfo(), $query);
            return false;
        }
        return $q;
    }

    /**
     *  Fetch Query
     *
     * @param string $query - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $r - the array of values from the database
     *
     */
    public function Fetch($query, $vars = array())
    {   
        $q = $this->Query($query, $vars);
        $r = $q->fetchAll(\PDO::FETCH_ASSOC);
        return $r;
    }

    /**
     *  Fetch One Query
     *
     * @param string $query - the mysql query passed through
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return PDO $r - a single row from the database
     *
     */
    public function FetchOne($query, $vars = array())
    {       
        $r = $this->Fetch($query, $vars);

        if (empty($r))
            return array();
        
        return $r[0];
    }

    /**
     *  Update as Array Query
     *
     * @param string $table - the table name
     * @param array $update - the query that needs to be updated as an array
     * @param string $where - specific where statement
     * @param array $vars - variables that need to be inserted into the query
     *
     * @return boolean - true/false if statement was updated
     *
     */
    public function Update($table, $update, $wheres = array(), $rowcount = false)
    {
        $cols = array();

        foreach($update as $k => $v)
        {
            $cols[] = "`$k` = :$k";
        }

        $condition = array();
        foreach($wheres as $k => $v) {
            $condition[] = "`$k` = :$k";
        }

        $condition = implode(" AND ", $condition);
        $cols = implode(",", $cols);

        $u = "UPDATE {$table} SET {$cols} WHERE {$condition}";
        $q = $this->db->prepare($u);

        # bind all variables the proper way
        // foreach ($vars as $k => &$v)
        // {
        //     $q->bindParam(':'.$k, $v);
        // }

        # also bind update values
        foreach ($update as $k => &$v)
        {
            $q->bindParam(':'.$k, $v);
        }

        foreach ($wheres as $k => &$v)
        {
            if (!isset($insert[$k]))
                $q->bindParam(':'.$k, $v);
        }

        if (!$q->execute()) {
            $this->ReportError($q->errorInfo());
            return false;
        }

        # query execute, but no records updated
        if($rowcount && !$q->rowCount()){
            //throw new \Exception("no row count");
            return false;
        }
        return true;
    }

    /**
     * Insert into the Database
     *
     * @param table $table - table name
     * @param array $insert - array of key value pairs to insert
     *
     * @return int $id - return the id of the last inserted value
     *
     */
    public function Insert($table, $insert)
    {
        $cols = array();
        $values = array();

        foreach($insert as $k => $v)
        {
            $cols[] = "`$k`";
            $values[] = ":$k";
        }

        $cols = implode(",", $cols);
        $values = implode(",", $values);

        $i = "INSERT INTO {$table} ($cols) VALUES($values)";
        $q = $this->db->prepare($i);

        foreach ($insert as $k => &$v)
        {             
            $q->bindParam(':'.$k, $v);
        }
        
        if (!$q->execute()) {

            $this->ReportError($q->errorInfo());

            return false;
        }
        if ($q->rowCount() > 1)
            return false;

        $id = $this->db->lastInsertId();

        return $id;
    }

    /**
     * Delete from database
     *
     * @param string $table - table to delete
     * @param array $wheres - array of conditions
     *
     * @return $res
     *
     */
    public function Delete($table, $wheres = array())
    {
        $condition = array();
        foreach($wheres as $k => $v) {
            $condition[] = "`$k` = :$k";
        }

        $condition = implode(" AND ", $condition);

        $query = "DELETE FROM {$table} WHERE {$condition} ";

        $q = $this->db->prepare($query);

        # bind all variables the proper way
        foreach ($wheres as $k => &$v)
        {
            $q->bindParam(':'.$k, $v);
        }
        
        if (!$q->execute()) {
            
            $this->ReportError($q->errorInfo(), $query);
            return false;
        }

        return $q;
    }

    /**
     * Insert into the Database - but we check if it exists before we insert
     *
     * @param table $table - table name
     * @param array $insert - array of key value pairs to insert
     *
     * @return int $id - return the id of the last inserted value
     *
     */
    public function CheckInsert($table, $insert, $wheres = array(), $enableExistsUpdate = false)
    {   

        $condition = array();
        foreach($wheres as $k => $v) {
            $condition[] = "`$k` = :$k";
        }
        $condition = implode(" AND ", $condition);
        
        $query = "SELECT * FROM {$table} WHERE {$condition}";
        
        $exists = $this->FetchOne($query, $wheres);

        if (empty($exists)) {
            return $this->Insert($table, $insert);
        }

        if ($enableExistsUpdate){
            return $this->Update($table, $insert, $wheres);
        }

        return $exists['id'];
    }

    /**
     * Report error
     *
     * @param string $e - the error
     * @param string $query - The query that got passed throught
     *
     * @return not sure how i'm going to present this yet
     *
     */
    public function ReportError($error, $query = null)
    {
        //RD($error);
        //RD($query);
        throw new \Exception("MYSQL Error: ".$error[2]." $query");
    }

}