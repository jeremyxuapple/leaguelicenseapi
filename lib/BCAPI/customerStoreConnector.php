<?php

/**
 * This is the connector for the customer store
 * we will have separate connectors for variable stores if necessary
 *
*/

namespace LL\lib\BCAPI;

use LL\lib\database;
use Bigcommerce\Api\Client;

class customerStoreConnector
{
	// private $settings = array();
    private $client = false;


    ### Rewrote by Michael Sep 4. Might need to change 
    public function __construct($token){

        # init the db
        $this->db = new \LL\lib\database\mysql();
        # Get User Id
        $userId = $token["user_id"];

        # Get Account hash and customer store api access token 
        // $accountQuery = "
        //     SELECT a.token as auth_token, a.code as store_hash, oa.client_id as client_id, oa.client_secret as client_secret
        //     FROM accounts a
        //     LEFT JOIN oauth_clients oa 
        //     ON a.code = oa.client_id
        //     WHERE a.account_id = $userId
        // ";
      $config = array(
            "client_id"     => "ochficprqsyl6dwicu5h9heeufx0zrk", //$accountResult['client_id'],
            "client_secret" => "ovug17ftkfloj0up30qirazej2meo5f", // $accountResult['client_secret'],
            );

        // When app install we will get the bc_token directly
        if(isset($token['bc_token'])) {
            $config["auth_token"] = $token["bc_token"];
            $config["store_hash"] = $token["client_id"];

            $this->client = new \Bigcommerce\Api\Client();
            $this->client->configure($config);

        } else {
     // if we have don't have the bc_token inside of the toke object, we will need to make the query
            $accountQuery = "SELECT token, code FROM accounts WHERE account_id = :userId";
            $vars = array("userId" => $userId);
            $accountResult = $this->db->FetchOne($accountQuery, $vars);

            // if(isset($token['access_token']) && isset($token['client_id'])){
            if(isset($accountResult['token']) && isset($accountResult['code'])){
                $config["auth_token"] = $accountResult['token'];
                $config["store_hash"] = $accountResult['code'];
                $this->client = new \Bigcommerce\Api\Client();
                $this->client->configure($config);
            }else{
                # account not found and can not conntect 
                throw new \Exception("Error Processing Request : ".$request['token']);
            }
        }
      
    }

    // public function __construct($token){
    // 	 # init the db
    //     $this->db = new \LL\lib\database\mysql();

    //     $clientId    = $token["client_id"];
    //     $userId      = $token["user_id"];
    //     $accessToken = $token["access_token"];

    //     # let's get the database serttings
    //     $s = "SELECT * FROM settings";
    //     $settings = $this->db->Fetch($s);


    //     foreach($settings as $setting) {
    //         $this->settings[$setting['setting']] = $setting['value'];
    //     }

    //     # let's get the client token
    //     $query = "SELECT token, code as store_hash FROM accounts WHERE account_id = :id";
    //     $vars = array("id" => $userId);

    //     $result = $this->db->FetchOne($query, $vars);
    //     if (isset($result["token"]) && isset($result["store_hash"])){
    //     	$config = array(
    //    			// 'client_id' => 'd7u7jilvxsx6jj40wlgmjj9t7ie7ga4',//$this->settings['client_id'],
    //       //       'client_secret' => 'g19443rcn9b3fasxzh1sbzo6cmbpz25',//$this->settings['client_secret'],
    //             "client_id" => "ochficprqsyl6dwicu5h9heeufx0zrk",
    //             //"client_secret" => "kdlm0nkq2tg738fo228xzih000xhx94",
    //             "client_secret" => "ovug17ftkfloj0up30qirazej2meo5f",

    //    			'auth_token'    => "pal3lsnj79bsbrq8u25drgjcfbsx72i", //$result["token"],

                
    //    			'store_hash'    => "mj76l0d" //$result["store_hash"]
    //    		);

    //    		$this->client = new \Bigcommerce\Api\Client();
    //     	$this->client->configure($config);

    //     }else{
    //     	# account not found and can not conntect 
    //     	throw new \Exception("Error Processing Request : ".$request['token']);
    //     }
    // }

    public function getClient()
    {
        return $this->client;
    }
}