<?php
/**
 * This is the connector for the master store
 * we will have separate connectors for variable stores if necessary
 *
 */

namespace LL\lib\BCAPI;

use LL\lib\database;
use Bigcommerce\Api\Client;

class masterConnector
{
    private $settings = array();
    private $client = false;
    public function __construct()
    {
        # init the db
        $this->db = new \LL\lib\database\mysql();

        # let's get the database settings
        $s = "SELECT * FROM settings";
        $settings = $this->db->Fetch($s);

        foreach($settings as $setting) {
            $this->settings[$setting['setting']] = $setting['value'];
        }

        $config = array(
            'client_id' => $this->settings['client_id'],
            'client_secret' => $this->settings['client_secret'],
            'auth_token' => $this->settings['access_token'],
            'store_hash' => $this->settings['store_hash'],
        );

        $this->client = new \Bigcommerce\Api\Client();

        $this->client->configure($config);
    }

    public function getClient()
    {   
        return $this->client;
    }

}