<?php
/**
 * Main Invoice Service
 */

namespace LL\services;

use LL\lib\database;

class invoiceService
{
	public function __construct()
	{ 
		# define service instances
		$this->db = new \LL\lib\database\mysql();
		$this->emailService = new \LL\services\emailService();
	}

	public function getInvoicesByAccountId($accountId, $options)
	{	
		// $query = "SELECT order_id FROM account_invoice WHERE account_id = $accountId";
		// $order_ids = $this->db->Fetch($query);
		// $invoices_orders = array();

		// $invoices_order = array();

		// foreach ($order_ids as $order_id) {

		// 	$order_id = $order_id["order_id"];
		// 	$cost_total = 0;
		// 	$shipped_qty = 0;

		// 	$invoices_groupByOrder = $this->getInvoicesByIds($order_id, $accountId);

		// 	foreach ($invoices_groupByOrder as $invoice) {
		// 		$cost_total += $invoice["total"];
		// 		$shipped_qty += $invoice["quantity"];
		// 	}

		// 	$order = $this->db->FetchOne("SELECT * FROM orders WHERE account_id = $accountId AND order_id = $order_id");

		// 	$invoices_order["customer_name"] = $order["customer_name"];
		// 	$invoices_order["order_id"] = $order_id;
		// 	$invoices_order["cost_total"] = $cost_total;
		// 	$invoices_order["shipped_qty"] = $shipped_qty;
			
		// 	array_push($invoices_orders, $invoices_order);
		// }

		// RD($invoices_orders);
		// die();
		// $this->getInvoicesByIds($accountId, 100);

		extract($options);
		$where = isset($where_query) ? "order_id LIKE '%$where_query%' OR customer_name LIKE '%$where_query%'" : "1 = 1";
		$query = "
			SELECT ai.order_id, ai.total, ai.quantity, o.id, o.customer_name, op.quantity
			FROM account_invoice ai
			LEFT JOIN orders o ON ai.order_id = o.order_id AND ai.account_id = o.account_id
			LEFT JOIN order_product op ON ai.order_id = op.order_id AND ai.account_id = op.account_id
			WHERE ai.account_id = $accountId 
			AND " . $where . 
			" ORDER BY ai.$order_option $order_sort
				LIMIT $offset, $limit
		";
		$results = $this->db->Fetch($query);
		

		$invoices = array();
		foreach ($results as $result) {
	
			$order_id = $result["order_id"];
			$invoice = array(
			"customer" 				  			=> $result["customer_name"],
			"auto_increment_order_id" => $result["id"], 
			"product_name" 	    			=> $result['product_name'],
			"order_id" 				  			=> $result["order_id"],
			"status" 				  				=> $result["status"], 
			"net_profit" 			  			=> $net_profit
			);
			array_push($invoices, $invoice);
		}

		return $invoices;
	}


	public function getInvoicesByIds($order_id, $account_id)
	{
		$query = "SELECT * FROM account_invoice WHERE order_id = $order_id AND account_id = $account_id";
		$results = $this->db->Fetch($query);

		return $results;
	}

	/**
	* Create an instance to be saved into the RESTful API for the specific store and the 
	* specific order
	*
	*/
	public function create_invoice()
	{	
			$hanlding_fee_query = "SELECT * FROM management_settings WHERE id = 1";
			$settings = $this->db->FetchOne($hanlding_fee_query);

			$account_id = $_POST['account_id'];
			$order_id = $_POST['order_id'];
			$product_id = $_POST['product_id'];
			$quantity = $_POST['quantity'];
			$base_cost_price = $_POST['base_cost_price'];

			// get the cost price from product table and the subscription level from account table
			$query_cost_price = $this->db->FetchOne("SELECT price FROM products WHERE product_id = $product_id");
		
			$query_selling_price = $this->db->FetchOne("SELECT price_inc_tax FROM order_product WHERE master_product_id = $product_id AND order_id = $order_id");

			$query_sub_lvl= $this->db->FetchOne("SELECT active_subscription_level FROM accounts WHERE account_id = $account_id");

			$sub_lvl = $query_sub_lvl["active_subscription_level"];
			$cost_price = $query_cost_price["price"];
			$selling_price = $query_selling_price["price_inc_tax"];
			$discount = 1.0;
			
			switch ($sub_lvl) {
				case 'Championship - Package':
					$discount = 0.9;
					break;
				case 'Contender - Package':
					$discount = 0.95;
					break;
					case 'Playoff - Package':
					$discount = 0.97;
					break;
				default:
					break;
			}
			$supplier_price = $cost_price * $discount;
			$total = $supplier_price * $quantity;
			$total = round($total, 2);
			$net_profit = ($selling_price - $supplier_price) * $quantity;
			$net_profit = round($net_profit, 2);

			// echo " selling_price: $selling_price\n";
			// echo " supplier_price: $supplier_price\n";
			// echo " net_profit: $net_profit\n";
			// die()
			// ;
			// $query = "SELECT price_ex_tax FROM order_product WHERE order_id = $order_id AND master_product_id = $product_id";
			// $price_array = $this->db->FetchOne($query);

			// $selling_price = $price_array["price_ex_tax"];

			// $gross_profit = $quantity * ($selling_price - $base_cost_price);
			// $total = $_POST['price_inc_tax'] * $_POST['quantity'];
			
			$invoice = array(
				'order_id' 					    => $_POST['order_id'],
				'supplier_id' 			    => $_POST['supplier_id'],
				'product_id' 				    => $_POST['product_id'],
				'product_name'					=> $_POST['product_name'],
				'account_id'            => $_POST['account_id'],
				'sku' 							    => $_POST['sku'],
				'net_profit' 						=> $net_profit,
				'supplier_price'				=> $cost_price * $discount,
				'status'								=> "tracking received",
				'quantity' 					    => $_POST['quantity'],
				'total' 								=> $total,
				'base_shipping_cost'		=> $_POST['base_shipping_cost'],
				'shipping_cost_ex_tax'  => $_POST['shipping_cost_ex_tax'],
				'shipping_cost_inc_tax' => $_POST['shipping_cost_inc_tax'],
				'carrier_company' 			=> $_POST['carrier_company'],
				'base_handling_cost'    => $settings['handling_markup'],
				'handling_cost_ex_tax'  => $settings['handling_ex_tax'],
				'handling_cost_inc_tax' => $settings['handling_inc_tax'],
				'tracking_number'       => $_POST['tracking_number'],
				// 'base_cost_price'       => $_POST['base_cost_price'],
				'price_inc_tax'         => $_POST['price_inc_tax'],
				'price_ex_tax'          => $_POST['price_ex_tax'],
				'date_created'          => date('Y-m-d, H:i:s'),
				'date_modified'         => date('Y-m-d, H:i:s'),
				);

			$this->db->update("orders", array('date_modified' => date("Y-m-d H:i:s")), array('order_id' => $order_id, 'account_id' => $account_id));

			return $invoice;


	}

	/**
	* Send out email notification to the child store when the invoice is ready after the products 
	* have been shipped out.
	* @param $account_id - the id of the chlld store which should receive the invoice
	* @param $order_id - the id of the order
	* @param $product_id - the id of the product has been shipped
	* @param $supplier_id - the id of the supplier has been shipped
	*/

	public function send_invoice_email($account_id, $order_id, $product_id, $supplier_id)
	{		
			$email_query = "SELECT contact_email FROM accounts WHERE account_id=$account_id";
			$email = $this->db->FetchOne($email_query);
		
			$invoice_query = "SELECT * FROM account_invoice 
												WHERE account_id = $account_id 
												AND order_id = $order_id
												AND product_id = $product_id
												AND supplier_id = $supplier_id";

			$invoice = $this->db->Fetch($invoice_query);
			extract($invoice[0]);

			$template_content = array(
					array(
						"name" => "order_id", 
						"content" => $order_id
					), 
					array(
						"name" => "product_id", 
						"content" => $product_id
					), 
					array(
						"name" => "sku", 
						"content" => $sku
					),
					array(
						"name" => "base_cost_price", 
						"content" => $base_cost_price
					),
					array(
						"name" => "shipping_cost_inc_tax", 
						"content" => $shipping_cost_inc_tax
					),
					array(
						"name" => "tracking_number", 
						"content" => $tracking_number
					),
					array(
						"name" => "quantity", 
						"content" => $quantity
					),
				);

			$this->emailService->sendMailTemplate("doug@beapartof.com"
				, "doug@beapartof.com", "An invoice has been created",  "Invoice(ToCustomerStore)", $template_content);

	}
}