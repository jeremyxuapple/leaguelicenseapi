<?php
/**
 * Main Notification Controller
 */

namespace LL\services;

use LL\lib\database;

class notificationService
{
  public function __construct()
  {
    # deinfe database instance
    $this->db = new \LL\lib\database\mysql();
  }
  
  public function getNotifications($userId, $options)
  {
    extract($options);

    $where = isset($where_query) ? "details LIKE '%$where_query%' 
            OR entity_name LIKE '%$where_query%' 
            OR entity_type LIKE '%$where_query%'
            OR status LIKE '%$where_query%'
            OR msg_type LIKE '%$where_query %'" : "1 = 1";


    $query = "
      SELECT id, DATE_FORMAT(date_created, '%Y %b %D') AS date_created, DATE_FORMAT(date_modified, '%Y %b %D') AS date_modified, msg_type, entity_name, status, entity_id, details
      FROM notifications
      WHERE (from_user_id = $userId OR to_user_id = $userId) AND " . $where . 
      " ORDER BY $order_option $order_sort
      LIMIT $limit OFFSET $offset
    ";

    $notifications = $this->db->Fetch($query);
    return $notifications;
  }

  public function getNotificationById($id) 
  {   
      $query = "SELECT * FROM notifications WHERE id = :id";
      $result = $this->db->FetchOne($query, array("id" => $id));

      return $result;
  }

  # $notification is the object which going to be saved into the database
  /*
  $notification = array(
      "from_user_id" => value to be filled in,
      "to_user_id" => value to be filled in,
      "msg_type" =>  value to be filled in,
      "entity_id" => value to be filled in,
      "entity_type" => value to be filled in,
      "entity_name" => optional...
      "details" => value to be filled in,
      "date_created" => value to be filled in,
      "date_modified" => value to be filled in,
      "status" => "open",
      );
  */
  public function createNotification($notification)
  {

    $notification["date_created"] = date("Y-m-d H:i:s");
    $notification["date_modified"] = date("Y-m-d H:i:s");
    // $notification["status"] = "Open";

    extract($notification);

    $where = array(
      "entity_id"     => $entity_id,
      "entity_type"   => $entity_type,
      "from_user_id"  => $from_user_id,
      "to_user_id"    => $to_user_id,
    );
  
    $this->db->CheckInsert("notifications", $notification, $where);
  }

  public function update_notification($update_notification)
  {
    $id = $update_notification["id"];
    $this->db->Update("notifications", $update_notification, array( "id" => $id));
  }

  public function generateNotificationForNewProducts($productId)
  {
    // init API connection 
    $api = new \LL\lib\BCAPI\masterConnector();
    $this->api = $api->getClient();

    #get the newest product
    $product = $this->api->getProduct($productId);
    $newProductName = $product->name;

    // $newProductQuery = "SELECT product_id, name FROM products ORDER BY id DESC LIMIT 0, 1";
    // $newProduct = $this->db->FetchOne($newProductQuery);
    // $newProductName = $newProduct["name"];
    // $newProductId = $newProduct["product_id"];

    #get all customer store id
    $allAccountsQuery = "SELECT account_id FROM accounts";
    $allAccounts = $this->db->Fetch($allAccountsQuery);
    foreach ($allAccounts as $account) {
      # init notification 
      $notification = array(
        "from_user_id" => 0, // system notification
        "to_user_id" => $account['account_id'],
        "msg_type" =>  "new product",
        "entity_id" => $productId,
        "entity_type" => "products",
        "details" => "A new Product - $newProductName is available in master store",
        "status"  => "Open"
      );

      # send notifications 
      $this->createNotification($notification);
    }
  }




}