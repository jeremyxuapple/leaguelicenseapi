<?php
/**
 * Main Accounts Service
*/

namespace LL\services;

use LL\lib\database;

class accountService
{
	public function __construct()
	{
		# define service instances
		$this->db = new \LL\lib\database\mysql();
		$api = new \LL\lib\BCAPI\masterConnector();
    	$this->api = $api->getClient();
    	$this->notificationService = new \LL\services\notificationService();
	}

	/**
	 * Get Specific Account Information By Account Id
	 *
	 * @param - $account - int
	 * @return account base information with subscription list - array
	*/
	public function getAccountByAccountId($accountId)
	{
		# Basic account info
		$queryAccount = "SELECT * FROM accounts WHERE account_id = :account_id";

		#account subscriptions 
		$queryAccountSubscriptions = "
            SELECT * 
            FROM subscriptions 
            WHERE account_id = :account_id
            ORDER BY date_created DESC
		";

		$resultAccount = $this->db->FetchOne($queryAccount, array('account_id' => $accountId));
		$resultAccount["subscriptions"] = $this->db->Fetch($queryAccountSubscriptions, array('account_id' => $accountId));

		return $resultAccount;
	}

	/**
	 * Get Account List Access By Master Only
	 *
	 * @param $option - array
	 * @return accounts - array
	 */
	public function getAccounts($options)
	{
		# extract options array
		extract($options);

		$where = isset($where_query) ? "account_id LIKE '%$where_query%' OR platform LIKE '%$where_query%' OR active_subscription_level LIKE '%$where_query%' OR name LIKE '%$where_query%'" : "1 = 1";

		$query = "
			SELECT id, account_id, platform, DATE_FORMAT(date_created, '%Y %b %D') AS date_created, status, name, life_time_orders, billing_zip, active_subscription_level
            FROM accounts " . 
            "WHERE " . $where . 
			" ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";
		
		$results = $this->db->Fetch($query);

		return $results;
	}

	/**
	 * Generate Account For Accept Data From POST Request
	 *
	 * @return account - array
	*/
	public function generateAccountInstance()
	{	
		// RD(rawurldecode($_POST["platform"]));
		// RD(rawurldecode($_POST["name"]));
		// RD(rawurldecode($_POST["contact_email"]));
		// die;
		$account = array(
            // "platform"              		=> rawurldecode($_POST["platform"]),
            "active_subscription_level"     => rawurldecode($_POST["active_subscription_level"]),
            // "name"                  		=> rawurldecode($_POST["name"]),
            "contact_first_name"    		=> rawurldecode($_POST["contact_first_name"]),
            "contact_last_name"     		=> rawurldecode($_POST["contact_last_name"]),
            // "contact_email"         		=> rawurldecode($_POST["contact_email"]),
            "contact_phone"         		=> rawurldecode($_POST["contact_phone"]),
            "tax_id"                		=> $_POST["tax_id"],
            "currency"              		=> rawurldecode($_POST["currency"]),
            "product_markup"        		=> $_POST["product_markup"],
            "shipping_markup"       		=> $_POST["shipping_markup"],
            "certificate_path"      		=> rawurldecode($_POST["certificate_path"]),
            "billing_first_name"    		=> rawurldecode($_POST["billing_first_name"]),
            "billing_last_name"     		=> rawurldecode($_POST["billing_last_name"]),
            "billing_company"       		=> rawurldecode($_POST["billing_company"]),
            "billing_phone"         		=> rawurldecode($_POST["billing_phone"]),
            "billing_address_1"     		=> rawurldecode($_POST["billing_address_1"]),
            "billing_address_2"     		=> rawurldecode($_POST["billing_address_2"]),
            "billing_city"          		=> rawurldecode($_POST["billing_city"]),
            "billing_state"         		=> rawurldecode($_POST["billing_state"]),
            "billing_zip"           		=> rawurldecode($_POST["billing_zip"]),
            // "billing_country"       		=> rawurldecode($_POST["billing_country"]),
           	"staff_notes"           		=> rawurldecode($_POST["staff_notes"]),
            // "date_modified"         => date("Y-m-d H:i:s")
        );

        return $account;
	}

	public function getAccountJoinDateById($accountId)
	{
		$query = "SELECT date_created FROM accounts WHERE account_id = $accountId";
		$result = $this->db->FetchOne($query);
		
		return $result["date_created"];
	}

	/**
	 * Generate Account information Report for master store.
	 *
	 * @param $options - array - $date_start, $date_end, $type_query
	 * @return accounts - array
	*/
	public function getAccountsForReports($options)
	{
		extract($options);

		switch ($type) {
			case 'active':
				$type_query = "status <> 'Suspended'";
				break;
			case 'retired':
				$type_query = "status = 'Suspended'";
				break;
			default:
				// http_response_code(400);
				JSO(array("Error" => "400 Bad Request. Wrong Account Type Input"));
				throw new \Exception("400 Bad Request. Wrong Account Type Input");
				
				exit;
		}

		$query = "SELECT * FROM accounts WHERE date_created > '$date_start' AND date_created < '$date_end' AND " . $type_query;
		$results = $this->db->Fetch($query);

		return $results;
	}

	/**
	 * Generate Total Subscription Sales Of Each Store For Master Store  
	 *
	 * @param $options - array - $date_start, $date_end, $type_query
	 * @return mix - array
	*/
	public function getTotalSubscriptionsSalesForReports($options)
	{
		extract($options);

		$query = "
			SELECT a.account_id, a.name, a.platform, ROUND(SUM(s.price), 2) AS total_subscriptions_paid 
			FROM accounts a 
			LEFT JOIN subscriptions s
			ON a.account_id = s.account_id
			WHERE a.date_created > '$date_start' AND a.date_created < '$date_end'
			GROUP BY a.account_id
			";
		$results = $this->db->Fetch($query);
		RD($results);
		return $results;

	}

	/**
	 * Insert An New Account Into Our APi DB
	 *
	 * @param @token - object
	 * @return response
	*/
	public function createAccount($token)
	{
		$accountInstance = $this->generateAccountInstance();
		$accessToken = $token["access_token"];
		$accountId = $token["user_id"];

		$accountInstance["account_id"] = $token["user_id"];
		$accountInstance["token"] = $access_token;
		$accountInstance["status"] = "Active"; // default for account init
		$accountInstance["date_created"] = date("Y-m-d H:i:s");
		// $accountInstance["code"] = $_GET['code']; 

		# check account exist or not, might need to check code as well
		$query = "SELECT account_id FROM accounts WHERE account_id = $accountId OR token = '$accessToken'";
		$result = $this->db->Fetch($query);

		if (isset($result["account_id"])){
			// http_response_code(409);
			JSO(array("Error" => "Account Existed, Duplicate Account"));
			throw new \Exception("Duplicate Account");
		}

		$newAccount = $this->db->Insert("accounts", $accountInstance);

		if (isset($newAccount)){
			http_response_code(200);
			exit;
		}

		// http_response_code(400);
		JSO(array("Error"=>"Fail To Create A New Account"));
		throw new \Exception("Fail To Create A New Account");
	}


	/**
	* Create a subscription payment record when MiniBC has charge the child store successfully
	*	@param - orderId: the orderId which has been created on the Big Commerce Master Store.
	*/

	public function create_subscription_payment_record($orderId, $store_hash = null, $install = false)
	{
			$account_order = $this->api->getOrder($orderId);
			// $account_id = $account_order->customer_id;
			$product_id = $account_order->products[0]->product_id;
			$product_name = $account_order->products[0]->name;
			$subscription_level = substr($product_name, 0, -10);
			$price = $account_order->products[0]->base_total;
			$status = $account_order->status;
			$payment_method = $account_order->payment_method;

		// Get the payment_method_id for as for later invoice_payment charge	

		$query = "SELECT minibc_profile_id FROM accounts WHERE code = '$store_hash'";
		
		$profile_id = $this->db->FetchOne($query);

		if (!isset($profile_id["minibc_profile_id"])) {
			# we have to check recurring is finished payment or not.
			# sometimes, BC got the order but recurring is not.
			$minibc_call_try_time = 0;
			$isNoMiniBcRecurringSubscriptionId = true;

			while (($minibc_call_try_time <= 10) && ($isNoMiniBcRecurringSubscriptionId)) {

				$header = array(
					"X-MBC-TOKEN: MTY5LmFwaV81OTZmZDAxMmJjNjQzNS43Mjc5MTI3Ni4xNTAwNDk5OTg2",
				);
				$searchURL = "https://apps.minibc.com/api/apps/recurring/v1/subscriptions/search";
				$data = array(
					"order_id" => $orderId
				);

				$response = Curl($searchURL, $data, $header);  // call recurring endpoint...
				$data = json_decode($response);

				if (isset($data[0]->id)){ // if we have subscription id, means recurring finished processed order
					$profile_id = $data[0]->payment_method->id;
					$credit_card = $data[0]->payment_method->credit_card;

					$last_digits = $credit_card->last_digits;
					$expiry_month = $credit_card->expiry_month;
					$expiry_year = $credit_card->expiry_year;

					$this->db->Update("accounts", array("minibc_profile_id" => $profile_id), array("code" => $store_hash));  //update accounts table
					$isNoMiniBcRecurringSubscriptionId = false;
					break; // break out the while loop.
				}else{
					$minibc_call_try_time += 1;
					sleep(1);
				}				
			} // End of while loop
		} // End of (!isset($profile_id["minibc_profile_id"])...

		// The regular subscription payment charge
		if(!$install) {
			$this->generate_token($account_id, $product_id);
			$products = $this->api->getCollection("/orders/".$orderId."/products");
			// Issues:
			// One. the identifier to identify the store
			// Two. how to get the credit card information for the recurring charge. do we need to store the 
			// credit card information in the database? Maybe not.
	      foreach($products as $product) {
	          if (in_array($product->product_id, array(113,114,115))) {
	              foreach($product->product_options as $option) {
	                  if ($option->display_name == "Token") {
	                      $store_hash = $option->value;
	                  }
	              }
	          	} 
	      	}
		} // End of if install

		if($status == "Completed" || $status = "Awaiting Fulfillment") {
			$subscription_payment = array (
					"store_hash" => $store_hash,
					// "account_id" => $account_id,
					"master_product_id" => $product_id,
					"subscription_level" => $subscription_level,
					"bc_order_id" => $orderId,
					"price" => $price,
					"status" => $status,
					"payment_type" => $payment_method,
					"last_digits" => $last_digits,
					"expiry_month" => $expiry_month,
					"expiry_year" => $expiry_year,
					"date_created" => date("Y-m-d H:i:s"),
					);		
			
				// RD($subscription_payment);
				// die();
				$this->db->CheckInsert("subscriptions", $subscription_payment, array("store_hash" => $store_hash));
		} // End of if($status == "Completed" || $status = "Awaiting Fulfillment") 
	}


	/**
	* Generate a new token after the subscription payment charge successfully
	*/
	public function generate_token($account_id, $product_id)
	{
		
		$update_token = array(
				"access_token" => generateRandomString(40),
				"expires" => date('Y-m-d h:i:s', mktime(date('h'),date('i'),date('s'),date('m'),date('d')+30,date('Y')))
			);
			$product = $this->db->FetchOne("SELECT * FROM products WHERE product_id=$product_id");
			$this->db->Update("oauth_access_tokens", $update_token, array("user_id" => 
				$account_id));

			// Update account table with the new token
			$update_account = array(
				"token" => $update_token["access_token"],
				"active_subscription_level" => $product['name']
				);

			$this->db->Update("accounts", $update_account, array("account_id" => $account_id));
	}

	public function getLatestSubscirptionPayment($account_id) 
	{
		$query = "SELECT * FROM subscriptions WHERE account_id = $account_id ORDER BY date_created DESC";

		$subscription = $this->db->FetchOne($query);

		return $subscription;
	}

	public function updateAccountSkuInventoryLevel($skuId, $newInventoryLevel)
	{	
        # Find all customer Store with this sku
        $customerStoresIdsQuery = "
        	SELECT DISTINCT user_id, customer_product_sku_id, customer_product_id, sku
        	FROM product_sku
        	WHERE master_product_sku_id = $skuId AND user_id <> 1
        ";

        $customerStoresIds = $this->db->Fetch($customerStoresIdsQuery);

        foreach ($customerStoresIds as $customerStoreIds) {

            # prepare ids for customer store operations
            $accountId = $customerStoreIds["user_id"];
            $customerStoreSkuId = $customerStoreIds["customer_product_sku_id"];
            $customerStoreProductId = $customerStoreIds["customer_product_id"];
            $sku = $customerStoreIds["sku"];

            $accountQuery = "
                SELECT client_id, user_id, access_token 
                FROM oauth_access_tokens 
                WHERE user_id = $accountId
            ";

            $account = $this->db->FetchOne($accountQuery);

            # make token here
            $token = array(
                "access_token" => $account["access_token"],
                "client_id"    => $account["client_id"],
                "user_id"      => $account["user_id"],
            );

            # init customer store api 
            $api = new \LL\lib\BCAPI\customerStoreConnector($token);
            $this->clientapi = $api->getClient();
            # prepare update path for api 
            $path = "/products/$customerStoreProductId/skus/$customerStoreSkuId";

            # get inventory Subtraction value
            $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
            $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);

            if ($newInventoryLevel < $inventorySubtractionResult["inventory_subtraction_value"]){
            	# disable product sku
            	$disableObject = array(
            		"is_purchasing_disabled" => true,
            		"purchasing_disabled_message" => "We're sorry, this is unavailable."
            	);
            	$this->clientapi->updateResource($path, array("inventory_level" => $newInventoryLevel));
            	$this->clientapi->updateResource($path, $disableObject);

            	#send notification to Customer store
            	$notification = array(
            		"from_user_id" => 0, //system
            		"to_user_id"   => $account["user_id"],
            		"entity_id"    => $customerStoreSkuId,
            		"entity_type"  => "sku",
            		"entity_name"  => "Your Store",
            		"details"      => "Sku: $sku - Inventory Low. Has been Disable",
            		"status"       => "Open",
            		"msg_type"     => "Disable Sku"

            	);
            	# send notification to the customer store
            	$this->notificationService->createNotification($notification);

            }else{
	            # update customer Store sku
	            $this->clientapi->updateResource($path, array("inventory_level" => $newInventoryLevel));
            }
        }
	}

	/*
	 * Handle Update Customer Store Inventory
	*/
	public function updateAccountProductInventoryLevel($productId, $newInventoryLevel)
	{
		# Find all Customer Store with this ProductId
		$customerStoreInfosQuery = "
			SELECT ap.account_id, ap.customer_product_id, p.name
			FROM account_product ap
			LEFT JOIN products p
			ON p.product_id = ap.master_product_id
			WHERE ap.master_product_id = $productId
		";

		$customerStoresInfos = $this->db->Fetch($customerStoreInfosQuery);

		if (sizeof($customerStoresInfos) > 0 ){
			foreach($customerStoresInfos as $info){

				$accountId = $info["account_id"];
				$customerProductId = $info["customer_product_id"];
				$productName = $info["name"];
				$accountQuery = "
	                SELECT client_id, user_id, access_token 
	                FROM oauth_access_tokens 
	                WHERE user_id = $accountId
	            ";

	            $account = $this->db->FetchOne($accountQuery);

	            # make token here
	            $token = array(
	                "access_token" => $account["access_token"],
	                "client_id"    => $account["client_id"],
	                "user_id"      => $account["user_id"],
	            );

	            # init customer store api 
	            $api = new \LL\lib\BCAPI\customerStoreConnector($token);
	            $this->clientapi = $api->getClient();

	            # get inventory Subtraction value
	            $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
	            $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);

	            if ($newInventoryLevel < $inventorySubtractionResult["inventory_subtraction_value"]){
	            	# disable product 
	            	$disableObject = array(
	            		"availability" => "disabled",
	            		"is_visible" => false // Hide from the List
	            	);

	            	$this->clientapi->updateProduct($customerProductId, $disableObject);
	            	$this->clientapi->updateProduct($customerProductId, array("inventory_level" => $newInventoryLevel));
	            	#send notification to Customer Store
	            	$notification = array(
	            		"from_user_id" => 0, //system
	            		"to_user_id"   => $account["user_id"],
	            		"entity_id"    => $customerProductId,
	            		"entity_type"  => "products",
	            		"entity_name"  => "Your Store",
	            		"details"      => "Product: $productName - Inventory Low. Has been Disable",
	            		"status"       => "Open",
	            		"msg_type"     => "Disable Product"
	            	);

	            	#send notification to the customer store
	            	$this->notificationService->createNotification($notification);

	            }else{
	            	# enable object
	            	$enableObject = array(
	            		"availability" => "available",
	            		"is_visible" => true,
	            	);

	            	$this->clientapi->updateProduct($customerProductId, $enableObject);
	            	# update customer Store product inventory
	            	$this->clientapi->updateProduct($customerProductId, array("inventory_level" => $newInventoryLevel));
	            }
			} // End - Foreach Loop
		} // End - if customerStoresInfos
	} // End - Function updateAccountProductInventoryLevel(...)
}
