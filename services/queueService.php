<?php
/**
 * Queue Service
 */
namespace LL\services;

use LL\lib\database;
use LL\lib\BCAPI;
use LL\services;

class queueService
{
    public function __construct()
    {
        # define service instances
        $this->db = new \LL\lib\database\mysql();
        // $api = new \LL\lib\BCAPI\masterConnector();
        // $this->api = $api->getClient();
    }

    public function runQueue()
    {
        $queue = true;
        while($queue) {
            $queue = $this->processJob();
        }
    }

    /**
     * This will run through all the jobs in the queue
     */
    public function processJob()
    {
        # first thing we do is select the first job in the queue
        $s = "SELECT * FROM worker_queue LIMIT 0,1";
        $job = $this->db->FetchOne($s);

        # if there are no jobs we return false so we stop this queue immediately
        if (empty($job)) {
            return false;
        }

        # the next thing we do is delete the job from the queue because we already have it's information
        //$d = "DELETE FROM worker_queue WHERE id = :id";
        $vars = array("id" => $job['id']);
        $this->db->Delete("worker_queue", $vars);

        # let's get the token from the database
        $t = "SELECT * FROM oauth_access_tokens WHERE access_token = :token";
        $vars = array("token" => $job['token']);
        $token = $this->db->FetchOne($t, $vars);


        # now let's run the service that we need to to finish the job
        $variables = json_decode($job['variables'], true);

        # right now we're only processing services that have 1 variable and a token
        # let's declare the class necessary
        $controller = $job['service'];
        $c = "\\LL\\services\\$controller";
                
        if (class_exists($c)) {
            $section = new $c();
        } else {
            # this means that there's an error with the controller
            # we'll just skip it
            return true;
        }

        if ($variables['variables'] == 1) {
            if ($variables['token']) {
                # let's run this service
                $section->$job['action']($variables['var1'], $token);
            } else {
                # run the service without a token
                $section->$job['action']($variables['var1']);
            }
        }

        return true;
    }
}