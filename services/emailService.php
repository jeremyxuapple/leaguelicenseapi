<?php

/**
		* Email Sevice with Mandrill API
*/

namespace LL\services;

use LL\lib\database;
use Mandrill;

class emailService
{
	# this is the default key - will need to change later
	// private $key = "eCjsjeWX5IsJlKWuhnPcGQ";
	private $key = "dVnwRV2vRGWojO08TtGV2Q"; // From Ben's account

	public function __construct()
	{
		# define database instance
		$this->db = new \LL\lib\database\mysql();

		# let's get the key from the settings is possible
		$s = "SELECT value FROM settings WHERE setting = :setting";
		$vars = array("setting" => "mandrill_api_key");
        $settings = $this->db->FetchOne($s, $vars);
        
        if ($settings)
        	$this->key = $settings["value"];

        $this->mandrill = new Mandrill($this->key);

	}

	 /**
	 * Generate the email wiht Mandrill Api 
	 * @param $from - email initializer
	 * @param $to - email receipent
	 * @param $template_name - the template name has been stored in the Mandrill account
	 * @param $template_content - the data needs to be passed inside of the email
	 * 
	*/
	public function sendMailTemplate($from, $to, $subject, $template_name, $template_content) 
	{
		# let's create the recipient array from the to variable
		$recipients = array();
		if (is_array($to)) {
			foreach($to as $recipient) {
				$recipients[] = array(
					"email" => $recipient
				);
			}
		} else {
			$recipients[] = array(
				"email" => $to
			);
		}

		# let's create the message based on the variables
		$message = array(
			"subject" => $subject,
			"from_email" => $from,
			"to" => $recipients,
			"headers" => array("Reply-To" => $from),
			'important' => false,
			"merge" => true,
			'merge_language' => 'mailchimp',
		);

		# some default values
		$async = false;
		$ip_pool = 'Main Pool';

		# now let's try sending the email
		try {
			$result = $this->mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		}
		catch(Exception $e) {
			throw new \Exception("Mandrill Error: ".$e->getMessage);
		}

		return $result;
	}

  // public function mandrillEmailGenerator($api_key, $from, $to, $template_name, $template_content)
  // {
	 //  	try {
	 //    $mandrill = new Mandrill($api_key);
	 //    $message = array(
	 //        'html' => "<h1>Example</h1>",
	 //        'text' => 'Example text content',
	 //        'subject' => '',
	 //        'from_email' => $from,
	 //        'from_name' => 'League License',
	 //        'to' => array(
	 //            array(
	 //                'email' => $to,
	 //                'name' => 'League License Master Administrator',
	 //                'type' => 'to'
	 //            )
	 //        ),
	 //        'headers' => array('Reply-To' => $from),
	 //        'important' => false,
	 //        'track_opens' => null,
	 //        'track_clicks' => null,
	 //        'auto_text' => null,
	 //        'auto_html' => null,
	 //        'inline_css' => null,
	 //        'url_strip_qs' => null,
	 //        'preserve_recipients' => null,
	 //        'view_content_link' => null,
	 //        'bcc_address' => '',
	 //        'tracking_domain' => null,
	 //        'signing_domain' => null,
	 //        'return_path_domain' => null,
	 //        'merge' => true,
	 //        'merge_language' => 'mailchimp',
	 //    );
	 //    $async = false;
	 //    $ip_pool = 'Main Pool';
	 //    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
	 //    // print_r($result);
	  
	 //    } catch(Mandrill_Error $e) {
	 //         echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
	 // 	       throw $e;
	 //    }
  // }

}