<?php

/**
	* Service file for accountOrder which is stored on the Master Store for charging the  
	* child 
	* store.
*/

	namespace LL\services;
	use LL\lib\Oauth;
	use LL\lib\database;
	use \Datetime;

class accountOrderService
{
	public function __construct()
	{
		# define service instances
		$oauth = new \LL\lib\Oauth\OauthServer();
		$token = $oauth->verifyToken();
		$this->db = new \LL\lib\database\mysql();
		$api = new \LL\lib\BCAPI\masterConnector($token);
    $this->clientapi = $api->getClient();
	}

	/**
	* Create the instance to be saved into the BigCommerce API
	* 
	* NOTE: 1. store_order refers to the order been created on the master store for charging the 
	* 				 child store and the order refers to the one been created on the child store for
	* 				 charging the customer;
	* 
	* @param $order_id - the order_id has already been created for charging the customer
	* @param $account_id - the account_id which created the order
	* @param $invoice_id - the invoice_id which store all the difference data between  
	* 										 store_order and order
	* @param $product_id - the id of the product in the master store(master_product_id in 
	* 										 order_product table)
	*/
	public function create_account_order($invoice, $account_id, $order_id, $product_id)
	{		
		// $invoice_query = "SELECT * FROM account_invoice WHERE id = $invoice_id";
		$account_billing_query = "SELECT * FROM accounts WHERE id = $account_id";
		$product_query = "SELECT * FROM order_product WHERE order_id = $order_id AND account_id = $account_id AND master_product_id = $product_id";

		// $invoice = $this->db->FetchOne($invoice_query);
		$account_billing = $this->db->FetchOne($account_billing_query);
		$products = $this->db->Fetch($product_query);
		
		// Account Store Order base information
		// $bc_invoice["customer_id"] = $invoice["account_id"];
		$bc_invoice["base_shipping_cost"] = $invoice["base_shipping_cost"];
		$bc_invoice["base_handling_cost"] = $invoice["base_handling_cost"];
		$bc_invoice["shipping_cost_ex_tax"] = $invoice["shipping_cost_ex_tax"];
		$bc_invoice["shipping_cost_inc_tax"] = $invoice["shipping_cost_inc_tax"];
		$bc_invoice["handling_cost_ex_tax"] = $invoice["handling_cost_ex_tax"];
		$bc_invoice["handling_cost_inc_tax"] = $invoice["handling_cost_inc_tax"];
		$bc_invoice["date_created"] = date(DateTime::RFC2822);
		$bc_invoice["status_id"] = "2";

		$bc_account_order = $bc_invoice;
		$bc_billing = $this->rebuild_account_billing($account_id);

		//  Assemable for the product array

		$bc_products = array();

		foreach ($products as $product) {
			$bc_product["product_id"] 	 = $product["master_product_id"];
			$bc_product["quantity"] 	   = $product["quantity"];
			$bc_product["name"] 		     = $product["name"];

			// RD($invoice);
			// the price needs to match the price of the invoice
			$bc_product["price_inc_tax"] = $invoice["price_inc_tax"];
			$bc_product["price_ex_tax"]  = $invoice["price_ex_tax"];
			array_push($bc_products, $bc_product);
		}

		$bc_account_order["billing_address"] = $bc_billing;
		$bc_account_order["products"] = $bc_products;

		// RD($bc_account_order);
		// die();

		$response = $this->clientapi->createOrder($bc_account_order);
		// RD($this->clientapi->getLastError());
		
		// RD($response);
		// die();

		$bc_order_id = $response->id;
		$amount = $response->total_inc_tax;
		// $bc_order_id = 217;
		// $amount = 40.0;
		
		$query = "SELECT minibc_profile_id, name FROM accounts WHERE account_id = $account_id";

		$account_info = $this->db->FetchOne($query);
		$account_name = $account_info["name"];
		$profile_id = $account_info["minibc_profile_id"];
		
		$header = array(
				"X-MBC-TOKEN: MTY5LmFwaV81OTZmZDAxMmJjNjQzNS43Mjc5MTI3Ni4xNTAwNDk5OTg2",
				);
		$transaction = array(
			"order_id" => $bc_order_id,
			"amount" =>  $amount,
			"desc" => "Invoice charge for $account_name"			
			);

		$chargeUrl = "https://apps.minibc.com/api/apps/recurring/v1/vault/$profile_id/transaction/charge";
		$response = Curl($chargeUrl, $transaction, $header);	

		$response = json_decode($response, true);
		$charge_status = $response["success"];

		// $charge_status = true;

 if ($charge_status) { 
			$this->update_order_status($account_id, $product_id, $order_id);
		}
}

	/**
	* Update order_product status and the order status after the invoice has been charge successfully
	* @param $success - boolean: the charge status of the invoice
	*/

	public function update_order_status($account_id, $product_id, $order_id)
	{		
			// update order_product status
			$this->db->Update("order_product", array("status" => "shipped"), array("account_id" => $account_id, "master_product_id"=> $product_id, "order_id" => $order_id));

		// Update bc_order_status if all the products have been changed status to shipped	
			$query = "SELECT * FROM order_product WHERE account_id = $account_id AND order_id = $order_id";
			$shipped_products = $this->db->Fetch($query);

			$shipped_update_needed = true;
			$partially_shipped_updated_needed = false;
			$seller_license_products = array();

			foreach ($shipped_products as $shipped_product) {
				if($shipped_product["fulfillment"] == "Seller Licence") {
					array_push($seller_license_products, $shipped_product);
				}
			}
			
			foreach ($seller_license_products as $seller_license_product) {

					if($seller_license_product["status"] == "shipped"){
					// this product has been shipped out, we need to keep checking the next product
					$partially_shipped_updated_needed = true;
				} else {
					// this product has not been shipped;
					$shipped_update_needed = false;
				}
			}

// Update original order status to partially shipped with status_id = 3 and our own database
		if($partially_shipped_updated_needed) {

			$query = "SELECT oc.client_id, oc.client_secret, oat.access_token FROM oauth_clients oc LEFT JOIN oauth_access_tokens oat ON oat.user_id = oc.user_id WHERE oc.user_id = $account_id";
				$token = $this->db->FetchOne($query);
				$token["user_id"] = $account_id;

				$customer_api = new \LL\lib\BCAPI\customerStoreConnector($token);
      	$clientapi = $customer_api->getClient();

      	// $order_id = 113;

      	$clientapi->updateOrder($order_id, array("status_id" => 3));

      	//Update our own database
      	$this->db->Update("orders", array("status" => "Partially Shipped"), array("order_id" => $order_id));
		}

// We will need to update the original order status to shipped with status_id = 2 and our own database			
			if($shipped_update_needed) {
				
				$query = "SELECT oc.client_id, oc.client_secret, oat.access_token FROM oauth_clients oc LEFT JOIN oauth_access_tokens oat ON oat.user_id = oc.user_id WHERE oc.user_id = $account_id";
				$token = $this->db->FetchOne($query);
				$token["user_id"] = $account_id;
				// print_r($token);

				$customer_api = new \LL\lib\BCAPI\customerStoreConnector($token);
        $clientapi = $customer_api->getClient();

        // $order_id = 103;
        $clientapi->updateOrder($order_id, array("status_id" => 2));
       
        // Update our own database 
				// $order_id = 101;
        $this->db->Update("orders", array("status" => "Shipped"), array("order_id" => $order_id, 'account_id' => $account_id));
			}
	}

	/**
	* Get account order from account_invoice which is the order created on the master store
	* for charging the child store.
	*/

	public function get_account_orders($options)
	{    	
    extract($options);
    $where = isset($where_query) ? 
    				"account_id LIKE '%$where_query%' OR 
    				 sku LIKE '%$where_query%' OR
    				 tracking_number LIKE '%$where_query%'
    				 " : "1 = 1";
		$query = "
			SELECT account_id, DATE_FORMAT(date_created, '%Y %b %D') AS date_created, product_id, sku, quantity, tracking_number
			FROM account_invoice
			WHERE " . $where . "
			ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";

		$account_orders = $this->db->Fetch($query);
		return $account_orders;
	}

	/**
	* Get the account_order by id
	*/
	public function get_account_order_by_id($id)
	{	
		$account_order_query = "SELECT * FROM account_invoice WHERE id = $id";
		$account_order = $this->db->Fetch($account_order_query);

		return $account_order;
	}

	/**
	*	Charge the child store base on the active subscription level
	*
	*/

	public function create_subscription_order($account_id, $token)
	{	
		$bc_billing = $this->rebuild_account_billing($account_id);
		$account_info = $this->db->FetchOne("SELECT * FROM accounts WHERE account_id=$account_id");
		
		switch ($account_info["active_subscription_level"]) {
			case 'Playoff':
				$product_id = 113;
				break;
			case 'Contender':	
				$product_id = 114;
				break;
			case 'Championship':
				$product_id = 115;
				break;
		}

		$product = array(
				"product_id" => $product_id,
				"quantity" => 1,
				"product_options" => [
						array(
								"id" => 33,
								"value" => $token["access_token"], 
							)
					],
				);

		$products = array();
		$product_options = array();
		array_push($products, $product);

		$subscription_order = array(
			"customer_id" => $account_id,
			"status_id" => 7, 
			"date_created" => date(DateTime::RFC2822),
			"billing_address" => $bc_billing,
			"products" => $products,
			);

		// JSO($subscription_order);
		// die();

		// $this->clientapi->createOrder($subscription_order);
		// RD($this->clientapi->getLastError());
	}

	/**
	* Get the billing address from account table and rebuild for pushing into Big Commerce * API
  *
	*/

	public function rebuild_account_billing($account_id)
	{
		$account_billing_query = "SELECT * FROM accounts WHERE account_id = $account_id";
		$account_billing = $this->db->FetchOne($account_billing_query);

		$bc_billing["first_name"] = $account_billing["billing_first_name"]; 
		$bc_billing["last_name"]  = $account_billing["billing_last_name"];
		$bc_billing["company"]    = $account_billing["billing_company"];
		$bc_billing["street_1"]   = $account_billing["billing_address_1"];
		$bc_billing["street_2"]   = $account_billing["billing_address_2"];
		$bc_billing["city"]       = $account_billing["billing_city"];
		$bc_billing["state"]      = $account_billing["billing_state"];
		$bc_billing["zip"]        = $account_billing["billing_zip"];
		$bc_billing["country"]    = $account_billing["billing_country"];
		$bc_billing["phone"]      = $account_billing["contact_phone"];
		$bc_billing["email"]      = rawurldecode($account_billing["contact_email"]);

		return $bc_billing;
	}




}
