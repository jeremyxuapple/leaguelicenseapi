<?php
/** 
 * Main Products Service
*/

namespace LL\services;

use LL\lib\database;
use LL\lib\BCAPI;

class productService
{
	public function __construct()
	{          
		# define service instace
		$this->db = new \LL\lib\database\mysql();
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();
        $this->notificationService = new \LL\services\notificationService();
	}

	public function getProducts($options)
	{
		# extract options array
		extract($options);

		$query = "
			SELECT id, product_id, name, sku, availability, inventory_level, price 
            FROM products
            WHERE $where_query
			ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";
        
		$results = $this->db->Fetch($query);
		return $results;
	}

    /**
     * Get product information by productId
     *
     * @param $productId - int - product_id in products table
     * @return $product - array
     *
    */
    public function getProductById($productId)
    {
        $query = "SELECT * FROM products p WHERE p.product_id = :product_id";
        $result = $this->db->FetchOne($query, array('product_id' => $productId));

        return $result;
    }
    
	/**
     * Check Product Exists or Not 
     *
     * @param int $id (optional) - Passed by URL  
     * @param Array passed by POST / PUT request
     *
     * @return int $productId not null
     * 
    */
    public function checkProductExists($id = null)
    {
        if (isset($id)){
            # check product by product id
            $query = "SELECT p.product_id FROM products p WHERE p.product_id = :product_id";
            $result = $this->db->FetchOne($query, array('product_id' => $id));
        }else{
            # check product by manufactory sku 
            $manufactorySku = $_POST["manufactorySku"];
            $query = "SELECT p.product_id FROM products p WHERE p.manufactory_sku = :manufactory_sku";
            $result = $this->db->FetchOne($query, array('manufactory_sku' => $manufactorySku));
        }
        $productId = $result["product_id"];

        return $productId;
    }

    /** 
     * Get a product from supplier_product table by productId and supplierId
     *
     * @param int - $productId
     * @param int - $supplierId
     * 
     * @return mixed - product object
     *   
    */
    public function getSupplierProductByIds($productId, $supplierId)
    {
        $query = "SELECT * FROM supplier_product sp WHERE sp.product_id = :product_id AND sp.supplier_id =: supplier_id";
        $result = $this->db->FetchOne($query, array('product_id' => $productId, 'supplier_id' => $supplierId));

        return $result;
    }

    /**
     * Calculate and Update the inventory of a product
     * 
     * @param int - required - productId
     * @param int - optional - supplierId
     * @param int - optional - qty - for the specific qty comes from orders
     * @param str - optional - type - purchasing or returning - for the product stage from order 
     *
     * @return int - productId (updated)
    */
    
    public function updateProductInventory($productId, $supplierId = null, $qty = null, $type = null)
    {
        # order processer. update inventory for the supplier_product table
        if (isset($supplierId) && isset($qty) && isset($type)){
            $supplierProduct = $this->getSupplierProductByIds($productId, $supplierId);
            $supplierProductInventory = $supplierProduct["inventory_level"];

            if ($type == "purchasing"){
                $newInventory = $supplierProductInventory - $qty;
            }
            # returning
            else{
                $newInventory = $supplierProductInventory + $qty;
            }

            #update new inventory on supplier_product table
            $this->db->Update(
                "supplier_product", 
                array("inventory_level" => $newInventory), 
                array("product_id" => $productId, "supplier_id" => $supplierId)
            );
        }

        # get the sum inventory of this product from all NOT SUSPEND suppliers
        $query = "
            SELECT sum(sp.inventory_level) AS total_inventory_level 
            FROM supplier_product sp
            LEFT JOIN suppliers s
            ON sp.supplier_id = s.supplier_id AND s.state != 'suspend' 
            WHERE sp.product_id = :product_id
        ";

        $result = $this->db->FetchOne($query, array('product_id' => $productId));
        $newTotalInventory = $result["total_inventory_level"];

        # update the inventory on RESTFUL API
        $this->db->Update(
            "products",
            array("inventory_level" => $newInventory),
            array("product_id" => $productId)
        );

        # update the inventory on master store(BC database)
        $updatedproduct = $this->api->updateProduct($productId, array("inventory_level" => $newInventory));

        return $updatedProduct;
    }

    /** 
     * Check product inventory for either overall or one specific supplier
     *
     * @param int - required - productId
     * @param int - required - qty - for the specific qty comes from orders
     * @param int - optional - supplierId 
     *
     * @return true / false
     *
    */
    public function checkProductInventory($productId, $qty, $supplierId = null)
    {
        ### PlaceHolder:  $masterStoreOverallMinInventory = getsettingInfo() ... [overwrtie priority lowest] ...

        $masterStoreProduct = $this->getProductById($productId);
        $masterStoreProductInventory = $masterStoreProduct["inventory_level"];
        $masterStoreMinInventory = $masterStoreProduct["inventory_warning_level"];

        # check master store product minimum inventory  
        if ( $masterStoreProductInventory - $qty >= $masterStoreMinInventory ){

            # check supplier product inventory 
            if (isset($supplierId)){
                $supplierProduct = $this->getSupplierProductByIds($productId, $supplierId);
                $supplierProductInventory = $supplierProduct["inventory_level"];
                $supplierProductMinInventory = $supplierProduct["inventory_warning_level"];

                if ( $supplierProductInventory - $qty >= $supplierProductMinInventory ){
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }

    public function checkOrderProductSkuToMatchLL($sku)
    {
        $query = "SELECT id FROM products WHERE sku = $sku";
        $result = $this->db->FetchOne($query);

        if ($result["id"]){
             return $result["id"];
        }
        return $result[id];
    }

    /**
     * Generate Total Product Sales Of each Store for Matster Store Report
     *
     * @param $options - array - $date_start, $date_end, $type_query
     * @return mix - array
    */ 
    public function geTotalProductSalesForReports($options)
    {
        extract($options);

        $query = "
            SELECT a.account_id, a.name, a.platform, SUM(op.quantity) AS totoal_sale_product_pieces, ROUND(SUM(base_total), 2) AS total_sale_product_amount_unit_USD
            FROM accounts a
            LEFT JOIN orders o ON a.account_id = o.account_id
            LEFT JOIN order_product op ON op.account_id = a.account_id AND op.order_id = o.order_id
            WHERE (o.status = 'Completed' OR o.origin_store_status = 'Completed') AND op.fulfillment <> 'External Product' AND a.date_created > '$date_start' AND a.date_created < '$date_end'
            GROUP BY a.account_id
        ";

        $results = $this->db->Fetch($query);
        return $results;
    }

	// public function generateProductInstance()
 //    {
 //        $product = array(
 //            "name"                      => _POST["name"],
 //            "type"                      => _POST["type"],
 //            "sku"                       => _POST["sku"],
 //            "description"               => _POST["description"],
 //            "search_keywords"           => _POST["searchKeywords"],
 //            "availability_description"  => _POST["availability_description"],
 //            "price"                     => _POST["price"],
 //            "cost_price"                => _POST["cost_price"],
 //            "retail_price"              => _POST["retail_price"],
 //            "sale_price"                => _POST["sale_price"],
 //            "calculated_price"          => _POST["calculated_price"],
 //            "sort_order"                => _POST["sort_order"],
 //            "is_visible"                => _POST["is_visible"],
 //            "is_featured"               => _POST["is_featured"],
 //            "related_products"          => _POST["related_products"],
 //            "inventory_level"           => _POST["inventory_level"],
 //            "inventory_warning_level"   => _POST["inventory_warning_level"],
 //            "warranty"                  => _POST["warranty"],
 //            "weight"                    => _POST["weight"],
 //            "width"                     => _POST["width"],
 //            "height"                    => _POST["height"],
 //            "depth"                     => _POST["depth"],
 //            "fixed_cost_shipping_price" => _POST["fixed_cost_shipping_price"],
 //            "is_free_shipping"          => _POST["is_free_shipping"],
 //            "inventory_tracking"        => _POST["inventory_tracking"],
 //            "rating_total"              => _POST["rating_total"],
 //            "rating_count"              => _POST["rating_count"],
 //            "total_sold"                => _POST["total_sold"],
 //            "brand_id"                  => _POST["brand_id"],
 //            "view_count"                => _POST["view_count"],
 //            "page_title"                => _POST["page_title"],
 //            "meta_keywords"             => _POST["meta_keywords"],
 //            "meta_description"          => _POST["meta_description"],
 //            "layout_file"               => _POST["layout_file"],
 //            "is_price_hidden"           => _POST["is_price_hidden"],
 //            "price_hidden_label"        => _POST["price_hidden_label"],
 //            "categories"                => _POST["categories"],
 //            "event_date_field_name"     => _POST["event_date_field_name"],
 //            "event_date_type"           => _POST["event_date_type"],
 //            "event_date_start"          => _POST["event_date_start"],
 //            "event_date_end"            => _POST["event_date_end"],
 //            "myob_asset_account"        => _POST["myob_asset_account"],
 //            "myob_income_account"       => _POST["myob_income_account"],
 //            "myob_expense_account"      => _POST["myob_expense_account"],
 //            "peachtree_gl_account"      => _POST["peachtree_gl_account"],
 //            "condition"                 => _POST["condition"],
 //            "is_condition_shown"        => _POST["is_condition_shown"],
 //            "preorder_release_date"     => _POST["preorder_release_date"],
 //            "is_preorder_only"          => _POST["is_preorder_only"],
 //            "preorder_message"          => _POST["preorder_message"],
 //            "order_quantity_minimum"    => _POST["order_quantity_minimum"],
 //            "order_quantity_maximum"    => _POST["order_quantity_maximum"],
 //            "open_graph_type"           => _POST["open_graph_type"],
 //            "open_graph_title"          => _POST["open_graph_title"],
 //            "open_graph_description"    => _POST["open_graph_description"],
 //            "is_open_graph_thumbnail"   => _POST["is_open_graph_thumbnail"],
 //            "upc"                       => _POST["upc"],
 //            "date_last_imported"        => _POST["date_last_imported"],
 //            "option_set_id"             => _POST["option_set_id"],
 //            "tax_class_id"              => _POST["tax_class_id"],
 //            "option_set_display"        => _POST["option_set_display"],
 //            "bin_picking_number"        => _POST["bin_picking_number"],
 //            "custom_url"                => _POST["custom_url"],  // *  we set it to the image path for now
 //            "availability"              => _POST["availability"] // * we set it to denote product selling or not selling
 //        );
        
 //        return $product;
 //    }

    /**
     * This will create a queue to add products in later for a speicifc client
     *
     * @param $categoryid - this is a specific category id
     * @param $brandid - specific brand id
     * @param $allproducts - if true will queue up all products on league licence
     * @param $token - token array
     *
     */
    public function createQueue($categoryid = null, $brandid = null, $allproducts = null, $token)
    {
        # let's start with an empty array
        $products = array();

        if ($brandid) {
            # add all products with this brand id
            $s = "SELECT product_id FROM products WHERE brand_id = :brand_id ";
            $vars = array(":brand_id" => $brandid);
            $products = $this->db->Fetch($s, $vars);
        }
        elseif ($categoryid) {
            # add all products under this category id
            $s = "SELECT product_id FROM products WHERE brand_id = :brand_id ";
            $vars = array(":brand_id" => $brandid);
            $products = $this->db->Fetch($s, $vars);
        }
        elseif ($allproducts) {
            # add all products period
            $s = "SELECT product_id FROM products ";
            $vars = array(":brand_id" => $brandid);
            $products = $this->db->Fetch($s, $vars);
        }
        
        foreach($products as $p) {
            # we're going to insert them into the queue
            $i = array(
                "service" => "productService",
                "token" => $token['access_token'],
                "action" => "importProductToAccount",
                "variables" => json_encode(
                    array(
                        "variables" => 1,
                        "token" => true,
                        "var1" => $p['product_id'],
                    )
                )
            );

            $this->db->Insert("worker_queue", $i);
        }
    }

    /**
     * Import One Product to customer store and also insert into account_product
     *
     * @param - $token - array
     * @param - productId - int
     *
     * @return response 
    */
    public function importProductToAccount($productId, $token, $queueOperation = null, $customerCategoryId, $bulkImport = null)
    {   
        // RD($productId);
        # init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();
        $accountId = $token["user_id"];
        
        # check this account's alreay import product or not
        // $accountQuery = "SELECT account_id FROM account_product WHERE account_id = $accountId";
        // $accountQueryResult = $this->db->FetchOne($accountQuery);
        $accountQuery = "SELECT is_imported_product FROM accounts WHERE account_id = $accountId";
        $accountQueryResult = $this->db->FetchOne($accountQuery);

        # duplicate product check
        // $productQuery = "SELECT id FROM account_product WHERE account_id = $accountId AND master_product_id = $productId";
        // $productResult = $this->db->FetchOne($productQuery);

        # product availablity check
            # get inventory Subtraction value
        // $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
        // $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);
        // $inventorySubtraction = $inventorySubtractionResult["inventory_subtraction_value"];

        // $productAvalableQuery = "SELECT id FROM products WHERE product_id = $productId AND inventory_level >= $inventorySubtraction";
        // $productAvalableResult = $this->db->FetchOne($productAvalableQuery);

        # check product duplicate 
        // if (!isset($productResult["id"])){

            # check product available when product imported only selected
            if (!$bulkImport){
                // if (isset($productAvalableResult["id"])){
                    # there is no product has been imported, Import global attribute in once
                    // if (!$accountQueryResult["is_imported_product"] == "true"){
                    //     #import all categories, options, optionValues, optionSets, optionSetOptions ...[product_options is read only]...
                    //     // $this->mapCategories($accountId, $token, $categoryId, $operation);
                    //     // $this->mapOptions($accountId, $token, $optionId, $operation);
                    //     // $this->mapOptionValues($accountId, $token, $optionValueId, $operation);
                    //     // $this->mapOptionSets($accountId, $token, $optionSetId, $operation);
                    //     // $this->mapOptionSetOptions($accountId, $token, $optionSetOptionId, $operation);

                    //     # change account status after all above finish
                    //     $this->db->Update("accounts", array("is_imported_product" => "true"), array("account_id" => $accountId));
                    // }
                    # import product by product Id
                    $this->mapProduct($accountId, $token, $productId, null, null, $customerCategoryId);

                    // # import Product sku By product Id
                    // $this->mapProductSkus($accountId, $token, $productId, $productSkuId, $operation);

                    // # Import and init Product Image, We dont do mapping here after insert
                    $this->mapProductImages($accountId, $token, $productId);
                // }
            }else{
                # Products imported by categories

                # we just import all products, ignore the inventory check in that case
                # import product by product Id
                $this->mapProduct($accountId, $token, $productId, null, null, $customerCategoryId, true);

                // # Import and init Product Image, We dont do mapping here after insert
                $this->mapProductImages($accountId, $token, $productId);
            }


        //If import by Select Product Only
        if (!$bulkImport){
            JSO(array("success" => true, "message" => "Product Imported, Please Check Your Store"));
        }

            # do nothing ... 
        // }else{
            # customer Store has this product ...
            // throw new \Exception("The Product Is In Your Store Already"); 
        // }

        // if (!$queueOperation){
        //     JSO(array("success" => true, "message" => "Product Imported, Please Check Your Store"));
        // }
    }

    public function getProductsByAccountId($accountId, $options)
    {
        extract($options);

        $where = isset($where_query) ? "p.product_id LIKE '%$where_query%' OR p.sku LIKE '%$where_query%' OR p.name LIKE '%$where_query%'" : "1 = 1";

        // recalculate base price 
        // $accountSubscriptionQuery = "SELECT active_subscription_level FROM accounts WHERE account_id = $accountId";
        // $accountSubscription = $this->db->FetchOne($accountSubscriptionQuery);
        // switch ($accountSubscription["active_subscription_level"]) {
        //     case "Playoff - Package":
        //         $discount = 5;
        //         break;
        //     case "Championship - Package":
        //         $discount = 10;
        //         break;
        //     default: 
        //         $discount = 0;
        //         break;
        // }

        // $query = "
        //     SELECT p.id, p.product_id, p.name, p.sku, p.availability, p.inventory_level, Round(p.price*(1-$discount/100),2) AS price, ap.customer_product_price
        //     FROM products p
        //     LEFT JOIN account_product ap ON p.product_id = ap.master_product_id
        //     WHERE ap.account_id = $accountId AND " . $where .
        //     " ORDER BY $order_option $order_sort
        //     LIMIT $offset, $limit
        // ";

        # disable discount for now.
        $query = "
            SELECT p.id, p.product_id, p.name, p.sku, p.availability, p.inventory_level, p.price, ap.customer_product_price
            FROM products p
            LEFT JOIN account_product ap ON p.product_id = ap.master_product_id
            WHERE ap.account_id = $accountId AND " . $where .
            " ORDER BY $order_option $order_sort
            LIMIT $offset, $limit
        ";

        $results = $this->db->Fetch($query);


        // RD($results);
        // foreach ($results as $result) {
        //     RD($result["price"]);
        //     $result["price"] = $result["price"] * (1 - $discount / 100);

        // }

        // RD($results);
        return $results;
    }

    /**
     * Product_Image table
     *
    */
    public function generateProductImageInstance($image, $userId)
    {
        $newImage = array(
            "user_id"              => $userId,
            "master_image_id"      => $image->id,
            "master_product_id"    => $image->product_id,
            "image_file"           => $image->image_file,
            "zoom_url"             => $image->zoom_url,
            "thumbnail_url"        => $image->thumbnail_url,
            "standard_url"         => $image->standard_url,
            "tiny_url"             => $image->tiny_url,
            // "is_thumbnail"         => $image->is_thumbnail,
            "sort_order"           => $image->sort_order,
            "description"          => $image->description,
            // "date_created"         => date("Y-m-d H:i:s"),   // No need to trace the create time for now
            // "is_sample"            => $image->is_sample,
        );

        $newImage["is_thumbnail"] = $image->is_thumbnail ? "true" : "false";

        return $newImage;
    }

    /**
     * Product_Sku tale 
     *
    */
    public function generateProductSkuInstance($sku, $userId)
    {
        $newSku = array(
            "user_id"                     => $userId,
            "master_product_sku_id"       => $sku->id,   // Read Only
            "master_product_id"           => $sku->product_id,
            "sku"                         => $sku->sku,
            "price"                       => $sku->price,
            "adjusted_price"              => $sku->adjusted_price,   // Read Only
            "cost_price"                  => $sku->cost_price,
            "upc"                         => $sku->upc,
            "inventory_level"             => $sku->inventory_level,
            "inventory_warning_level"     => $sku->inventory_warning_level,
            "bin_picking_number"          => $sku->bin_picking_number,
            "weight"                      => $sku->weight,
            "adjusted_weight"             => $sku->adjusted_weight,  // Read Only
            "is_purchasing_disabled"      => $sku->is_purchasing_disabled,
            "purchasing_disabled_message" => $sku->purchasing_disabled_message,
            "image_file"                  => $sku->image_file,    // Image attached to this sku
            // "date_created"                => date("Y-m-d H:i:s"),
            // "date_modified"               => date("Y-m-d H:i:s")
            // "options"                     => $sku->$options    // Go to product_sku_option table
        );
        return $newSku;
    }

    /**
     *  Product_Sku_Option
     *
    */
    public function generateProductSkuOptionInstance($sku, $skuOption, $userId)
    {
        $newSkuOption = array(
            "user_id"                  => $userId,
            "master_sku_id"            => $sku->id,
            "master_product_option_id" => $skuOption->product_option_id,
            "master_option_value_id"   => $skuOption->option_value_id,
        );
        return $newSkuOption;
    }

    /**
     * Product_Options table
     *
    */
    public function generateProductOptionInstance($option, $userId, $role)
    {
        $newOption = array(
            "user_id"          => $userId,
            // "master_option_id" => $option->id,
            "name"             => $option->name, 
            "display_name"     => $option->display_name,
            "type"             => $option->type
        );

        if ($role == "master"){
            $newOption["master_option_id"] = $option->id;
        }else{
            # prepare for master store query 
            $name = $option->name;
            $display_name = $option->display_name;
            $type = $option->type;

            $masterQuery = "
                SELECT master_option_id
                FROM product_options
                WHERE user_id = 1 AND name = '$name' AND display_name = '$display_name' AND type = '$type'
            ";

            $masterResult = $this->db->FetchOne($masterQuery);
            $newOption["customer_option_id"] = $option->id;
            $newOption["master_option_id"] = $masterResult["master_option_id"];
        }

        return $newOption;
    }

    /**
     * Product_OptionSets table
     *
    */
    public function generateProductOptionSetInstance($optionSet, $userId, $role)
    {
        $newOptionSet = array(
            "user_id"              => $userId,
            // "master_option_set_id" => $optionSet->id,
            "name"                 => $optionSet->name
        );

        if ($role == "master"){
            $newOptionSet["master_option_set_id"] = $optionSet->id;
        }else{
            $name = $optionSet->name;

            $masterQuery = "
                SELECT master_option_set_id
                FROM product_optionsets
                WHERE user_id = 1 AND name = '$name'
            ";

            $masterResult = $this->db->FetchOne($masterQuery);

            $newOptionSet["master_option_set_id"] = $masterResult["master_option_set_id"];
            $newOptionSet["customer_option_set_id"] = $optionSet->id;

        }
        return $newOptionSet;
    }

    /**
     * Product_optionset_option
     *
    */
    public function generateProductOptionSetOptionInstance($option, $userId, $role, $masterOptionSetId = null, $masterOptionId = null, $masterOptionSetOptionId = null, $customerOptionSetId = null, $customerOptionId = null)
    {
        $newOptionSetOption = array(
            "user_id"              =>  $userId,
            // "master_option_set_id" =>  $option->option_set_id,
            // "master_option_id"     =>  $option->option_id,
            "display_name"         =>  $option->display_name,
            "sort_order"           =>  $option->sort_order,
            "is_required"          =>  $option->is_required,
        );

        if($role == "master"){
            $newOptionSetOption["master_option_set_option_id"] = $option->id;
            $newOptionSetOption["master_option_set_id"] = $option->option_set_id;
            $newOptionSetOption["master_option_id"] = $option->option_id;
        }else{
            $newOptionSetOption["master_option_set_option_id"] = $masterOptionSetOptionId;
            $newOptionSetOption["master_option_set_id"] = $masterOptionSetId;
            $newOptionSetOption["master_option_id"] = $masterOptionId;
            $newOptionSetOption["customer_option_set_option_id"] = $option->id;
            $newOptionSetOption["customer_option_set_id"] = $customerOptionSetId;
            $newOptionSetOption["customer_option_id"] = $customerOptionId;
        }

        return $newOptionSetOption;
    }

    /**
     * Product_option_value tables
     *
    */
    public function generateProductOptionValueInstance($optionValue, $userId, $role, $masterOptionId = null, $customerOptionId = null)
    {
        $newValue = array(
            "user_id"                => $userId,
            // "master_option_value_id" => $optionValue->id,
            // "master_option_id"       => $optionValue->option_id,
            "label"                  => $optionValue->label,
            "sort_order"             => $optionValue->sort_order,
            "value"                  => $optionValue->value,
            "is_default"             => $optionValue->is_default,
        );

        if ($role == "master"){
            $newValue["master_option_value_id"] = $optionValue->id;
            $newValue["master_option_id"] = $optionValue->option_id;
        }else{
            # prepare for master query
            $label = $optionValue->label;
            $sort_order = $optionValue->sort_order;
            $value = $optionValue->value;

            $masterQuery = "
                SELECT master_option_value_id
                FROM product_option_value
                WHERE user_id = 1 AND label = '$label' AND sort_order = $sort_order AND value = '$value' AND master_option_id = $masterOptionId
            ";

            $masterResult = $this->db->FetchOne($masterQuery);

            # customer store
            $newValue["master_option_value_id"] = $masterResult["master_option_value_id"];
            $newValue["master_option_id"] = $masterOptionId;
            $newValue["customer_option_value_id"] = $optionValue->id;
            $newValue["customer_option_id"] = $customerOptionId;
        }

        return $newValue;
    }

    /**
     * Product_Product_Option tables
     *
    */
    public function generateProductProductOptionInstance($productOption, $userId)
    {
        $newProductOption = array(
            "user_id"                  => $userId,
            "master_product_option_id" => $productOption->id,
            "master_option_id"         => $productOption->option_id,
            "display_name"             => $productOption->display_name,
            "sort_order"               => $productOption->sort_order,
            "is_required"              => $productOption->is_required,
        );

        return $newProductOption;
    }

    /**
     * Products table
     *
    */
    public function generateProductInstance($product, $userId, $role, $masterProductId = null)
    {   
        // $newProduct = array(
            // // "user_id"                   => $userId,
            // "product_id"                => $product->id,
            // "name"                      => $product->name,
            // "type"                      => "physical",     // always physical
            // "sku"                       => $product->sku,  // UPC || check with the scope. Please use manufactory sku
            // "description"               => $product->description,
            // "search_keywords"           => $product->search_keywords,
            // "availability_description"  => $product->availability_description,
            // "price"                     => $product->price, // wholesale price * mark - up

            // "calculated_price"          => $product->calculated_price,  // wholesale price * mark - up

            // "is_visible"                => $product->is_visible, // 

            // "related_products"          => $product->related_products,
            // "inventory_level"           => $product->inventory_level,
            // "inventory_warning_level"   => $product->inventory_warning_level,
            // "warranty"                  => $product->warranty,
            // "weight"                    => $product->weight,
            // "width"                     => $product->width,
            // "height"                    => $product->height,
            // "depth"                     => $product->depth,
            // "fixed_cost_shipping_price" => $product->fixed_cost_shipping_price,-
            // "is_free_shipping"          => $product->is_free_shipping,
            // "inventory_tracking"        => $product->inventory_tracking,
            // "rating_total"              => $product->rating_total,
            // "rating_count"              => $product->rating_count,
            // "total_sold"                => $product->total_sold,
            // "date_created"              => $product->date_created,
            // // "brand_id"                  => $product->brand_id, // We are not support brand import yet
            // "view_count"                => $product->view_count,
            // "page_title"                => $product->page_title,
            // "meta_keywords"             => $product->meta_keywords,
            // "meta_description"          => $product->meta_description,
            // "layout_file"               => $product->layout_file,
            // "is_price_hidden"           => $product->is_price_hidden,
            // "price_hidden_label"        => $product->price_hidden_label,
            // // "categories"                => implode(",", $product->categories),  // We Save the categories to be text
            // "date_modified"             => $product->date_modified,
            // "event_date_field_name"     => $product->event_date_field_name,
            // "event_date_type"           => $product->event_date_type,
            // "event_date_start"          => $product->event_date_start,
            // "event_date_end"            => $product->event_date_end,
            // "myob_asset_account"        => $product->myob_asset_account,
            // "myob_income_account"       => $product->myob_income_account,
            // "myob_expense_account"      => $product->myob_expense_account,
            // "peachtree_gl_account"      => $product->peachtree_gl_account,
            // "condition"                 => $product->condition,
            // "is_condition_shown"        => $product->is_condition_shown,
            // "preorder_release_date"     => $product->preorder_release_date,
            // "is_preorder_only"          => $product->is_preorder_only,
            // "preorder_message"          => $product->preorder_message,
            // "order_quantity_minimum"    => $product->order_quantity_minimum,
            // "order_quantity_maximum"    => $product->order_quantity_maximum,
            // "open_graph_type"           => $product->open_graph_type,
            // "open_graph_title"          => $product->open_graph_title,
            // "open_graph_description"    => $product->open_graph_description,
            // "is_open_graph_thumbnail"   => $product->is_open_graph_thumbnail,
            // "upc"                       => $product->upc,
            // "avalara_product_tax_code"  => $product->avalara_product_tax_code,
            // "date_last_imported"        => $product->date_last_imported,
            // "option_set_id"             => $product->option_set_id,
            // "tax_class_id"              => $product->tax_class_id,
            // "option_set_display"        => $product->option_set_display,
            // "bin_picking_number"        => $product->bin_picking_number,
            // "custom_url"                => $product->custom_url,
            // "availability"              => $product->availability,

        if ($role == "master"){
            $newProduct = array(
                # List below Confirmed with Doug...
                "product_id"                => $product->id,
                "name"                      => $product->name,
                "type"                      => "physical", // Always physical
                "sku"                       => $product->sku, // UPC (double check scope doc), manufactory sku
                "description"               => $product->description,
                "search_keywords"           => $product->search_keywords,            
                "price"                     => $product->price, // wholesale price * mark-up for customer
                // "calculated_price"          => $product->calculated_price,  // wholesale price * mark-up for customer // Read Only
                "is_visible"                => $product->is_visible,
                "inventory_level"           => $product->inventory_level,
                "inventory_warning_level"   => 5, // 5 always (for now)
                "warranty"                  => $product->warranty,
                "weight"                    => $product->weight,
                "width"                     => $product->width,
                "height"                    => $product->height,
                "depth"                     => $product->depth,
                "inventory_tracking"        => $product->inventory_tracking, // YES
                "date_created"              => $product->date_created,
                "page_title"                => $product->name, //Same as Product "Name"
                "meta_keywords"             => $product->meta_keywords,
                "meta_description"          => $product->meta_description,
                "categories"                => implode(",", $product->categories),  // We Save the categories to be text
                // "date_modified"             => $product->date_modified,
                "condition"                 => $product->condition, // always NEW
                "upc"                       => $product->upc, // Same as SKU
                // "date_last_imported"        => $product->date_last_imported,  // it doesn't matter
                "option_set_id"             => $product->option_set_id,
                "bin_picking_number"        => $product->bin_picking_number,
                "availability"              => $product->availability, //|| "In-Stock" (this is a string sentenence, not a number)
            );
        }else{
            $newProduct = array(
                "account_id"             => $userId,
                "master_product_id"      => $masterProductId,
                "customer_product_id"    => $product->id,
                "customer_product_price" => $product->price,
            );
        }

        return $newProduct;
    }

    public function generateProductCategoryInstance($category, $userId, $role)
    {
        $newCategory = array(
            "user_id"              => $userId,
            // "master_category_id"   => $category->id,
            // "master_parent_id"     => $category->parent_id,
            "name"                 => $category->name,
            "description"          => $category->description,
            // "sort_order"           => $category->sort_order, // sort order is not a option we update
            "page_title"           => $category->page_title,
            "meta_keywords"        => $category->meta_keywords,
            "meta_description"     => $category->meta_description,
            "layout_file"          => $category->layout_file,
            "parent_category_list" => implode("," , $category->parent_category_list), // Read Only. We convert array into string
            "image_file"           => $category->image_file,
            // "is_visible"           => $category->is_visible,   // Read Only
            "search_keywords"      => $category->search_keywords,
            "url"                  => $category->url,
        );

        if ($role == "master"){
            $newCategory["master_category_id"] = $category->id;
            $newCategory["master_parent_id"]   = $category->parent_id;

        }else{
            # master store better to only have one parent category
            $name = $category->name;
            $description = $category->description;

            $masterQuery = "
                SELECT master_category_id, master_parent_id
                FROM product_categories
                WHERE user_id = 1 AND name = '$name' AND description = '$description'
            ";

            $masterResult = $this->db->FetchOne($masterQuery);

            # customer store
            $newCategory["customer_category_id"] = $category->id;
            $newCategory["customer_parent_id"]   = $category->parent_id;
            $newCategory["master_category_id"]   = $masterResult["master_category_id"];
            $newCategory["master_parent_id"]     = $masterResult["master_parent_id"];
        }

        return $newCategory;
    }

    /**
     * Handler Product Create/Update By Master Store, Sync Product Into DB.
     * We Can't Handle Delete in This function, We have to find another way to do the delete
     *
     * @param $userId - int - Always be MasterStore user_id
     *
    */
    public function syncProducts($userId) // We check all...
    {
        # init the sync product api.
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        ### Insert / Update product_categories table
        $categoriesAvailable = true;
        $categoriesPage = 1;

        // Loop Through 
        while ($categoriesAvailable){
            RD($categoriesPage);
            $allCategories = $this->api->getCategories(array('limit' => 250, 'page' => $categoriesPage));

            if (!isset($allCategories)){
                $categoriesAvailable = false;
                break; // break out the while loop
            }

            foreach ($allCategories as $category) {
                $categoryInstance = $this->generateProductCategoryInstance($category, $userId, "master");
                $whereCategory = array(
                    "user_id"            => $userId,
                    "master_category_id" => $category->id,
                    "master_parent_id"   => $category->parent_id,
                );
                $categoryObject = $this->db->CheckInsert("product_categories", $categoryInstance, $whereCategory, true);
            }

            $categoriesPage += 1; // go to next page
        }
        
        // die("done");

        
        ### Insert/ Update product_options table
        // $allOptions = $this->api->getOptions();
        // foreach ($allOptions as $option) {
        //     $optionInstance = $this->generateProductOptionInstance($option, $userId, "master");
        //     $whereOption = array(
        //         "user_id" => $userId,
        //         "master_option_id" => $option->id
        //     );
        //     $optionObject = $this->db->CheckInsert("product_options", $optionInstance, $whereOption, true);
        // }

        ### Insert / Update product_option_value table
        // $allOptionValue = $this->api->getOptionValues();
        // foreach ($allOptionValue as $optionValue) {
        //     $optionValueInstance = $this->generateProductOptionValueInstance($optionValue, $userId, "master");
        //     $whereOptionValue = array(
        //         "user_id"                => $userId,
        //         "master_option_value_id" => $optionValue->id,
        //         "master_option_id"       => $optionValue->option_id,
        //     );
        //     $optionValueObject = $this->db->CheckInsert("product_option_value", $optionValueInstance, $whereOptionValue, true);
        // }

        ### Insert / Update product_optionsets and product_optionset_option table
        // $allOptionSets = $this->api->getOptionSets();
        // foreach ($allOptionSets as $optionSet) {

        //     # product_optionset_option table
        //     $optionSetOptions = $optionSet->options;
        //     foreach($optionSetOptions as $optionSetOption){
        //         $optionSetOptionInstance = $this->generateProductOptionSetOptionInstance($optionSetOption, $userId, "master");
        //         $whereOptionSetOption = array(
        //             "user_id"                     =>  $userId,
        //             "master_option_set_option_id" =>  $optionSetOption->id,
        //             "master_option_set_id"        =>  $optionSetOption->option_set_id,
        //             "master_option_id"            =>  $optionSetOption->option_id,
        //         );
        //         $optionSetOptionObject = $this->db->CheckInsert("product_optionset_option", $optionSetOptionInstance, $whereOptionSetOption, true);
        //     }

            # product_optionSets table
        //     $optionSetInstance = $this->generateProductOptionSetInstance($optionSet, $userId);
        //     $whereOptionSet = array(
        //         "user_id"              => $userId,
        //         "master_option_set_id" => $optionSet->id,
        //     );
        //     $optionSetObject = $this->db->CheckInsert("product_optionsets", $optionSetInstance, $whereOptionSet, true);
        // }

        ## Insert / Update products, product skus /sku options and product_image tables
        $productsAvailable = true;
        $productsPage = 1;

        while ($productsAvailable){
            RD("==============================");
            RD($productsPage);
            $allProducts = $this->api->getProducts(array('limit'=> 250, 'page'=> $productsPage));
            // RD($allProducts);
            if (!isset($allProducts)){
                $productsAvailable = false;
                break; // break out the while loop
            }

            foreach ($allProducts as $product) {
                # product_image table
                // RD("############################");
                // RD("product id is $product->id");
                // RD("product id is $product->sku");
                // RD("product id is $product->name");
                if ($product->primary_image->id > 0) {
                    // RD($product->primary_image);
                    // $images = $this->api->getResource("/products/6818/images");
                    $image = $product->primary_image;

                    RD("------------------------");
                    RD("product id: $product->id");
                    RD("product sku: $product->sku");
                    RD("product image id: $image->id");

                    $imageInstance = array(
                        "user_id"              => $userId,
                        "master_image_id"      => $image->id,
                        "master_product_id"    => $product->id,
                        "zoom_url"             => $image->zoom_url,
                        "thumbnail_url"        => $image->thumbnail_url,
                        "standard_url"         => $image->standard_url,
                        "tiny_url"             => $image->tiny_url,
                    );

                    $whereImage = array(
                        "user_id"              => $userId,
                        "master_image_id"      => $image->id,
                        "master_product_id"    => $product->id,
                    );

                    $imageObject = $this->db->CheckInsert("product_image", $imageInstance, $whereImage, true);

                    // $images = $this->api->getCollection($product->images->resource);
                    // if (isset($images)){
                    //     foreach ($images as $image) {
                    //         $imageInstance = $this->generateProductImageInstance($image, $userId);
                    //         $whereImage = array(
                    //             "user_id"              => $userId,
                    //             "master_image_id"      => $image->id,
                    //             "master_product_id"    => $image->product_id,
                    //         );
                    //         $imageObject = $this->db->CheckInsert("product_image", $imageInstance, $whereImage, true);
                    //     }
                    // }
                }

                
                # product_product_option table
                // $productProductOptions = $product->options;  // Ignore for now. Since there is not BC APi to create product option... Read Only Purpose...

                # Product_sku table
                // $skus = $product->skus;
                # check product has skus or not
                // if (sizeof($skus) > 0){
                //     foreach ($skus as $sku) {

                //         # product_sku_option tables
                //         $skuOptions = $sku->options;
                //         foreach ($skuOptions as $skuOption) {
                //             $skuOptionInstance = $this->generateProductSkuOptionInstance($sku, $skuOption, $userId);
                //             $whereSkuOption = array(
                //                 "user_id"                  => $userId,
                //                 "master_sku_id"            => $sku->id,
                //                 "master_product_option_id" => $skuOption->product_option_id,
                //             );
                //             $skuOptionObject = $this->db->CheckInsert("product_sku_option", $skuOptionInstance, $whereSkuOption, true);
                //         }

                //         # product_sku tables
                //         $skuInstance = $this->generateProductSkuInstance($sku, $userId);
                //         $whereSku = array(
                //             "user_id"                     => $userId,
                //             "master_product_sku_id"       => $sku->id,
                //             "master_product_id"           => $sku->product_id,
                //             "sku"                         => $sku->sku,
                //         );
                //         $skuObject = $this->db->CheckInsert("product_sku", $skuInstance, $whereSku, true);
                //     }
                // }


                #products table
                $productInstance = $this->generateProductInstance($product, $userId, "master");

                $whereProduct = array(
                    "product_id" => $product->id,
                    "sku"        => $product->sku,
                );
                $productObject = $this->db->CheckInsert("products", $productInstance, $whereProduct, true);
            } // end of foreach loop

            $productsPage += 1; // go to next page 
        } // end of while loop
        RD("Sync Product Done");
    }

    public function mapCategories($accountId, $token, $categoryId = null, $operation = null)
    {  
        # init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($categoryId && $operation){
            # Insert New / Update 
            # code ... 
        }else{
            # Import all categories //* [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            $categoriesQuery = "
                SELECT master_parent_id AS parent_id, name, description, page_title, meta_keywords, meta_description, layout_file, image_file, search_keywords, url
                FROM product_categories
                WHERE user_id = 1
                ORDER BY master_parent_id ASC
            ";
            $categories = $this->db->Fetch($categoriesQuery);
            $parentId = 0; //init parent id

            foreach ($categories as $category) {
                // $category["parent_id"] = $parentId;
                // RD($category);
                // $newCategory = $this->clientapi->createCategory($category);
                // RD($newCategory);

                // if ($newCategory->parent_id == 0){
                //     $parentId = $newCategory->id;
                // }


                // $categoryInstance = $this->generateProductCategoryInstance($newCategory, $accountId, "customer");
                // $whereCategory = array(
                //     "user_id" => $accountId,
                //     "customer_category_id" => $newCategory->id,
                //     "customer_parent_id"   => $newCategory->parent_id
                // );
                // $this->db->CheckInsert("product_categories", $categoryInstance, $whereCategory, true);
            }
        }
    }

    public function mapOptions($accountId, $token, $optionId = null, $operation = null)
    {
        # init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($optionId && $operation){
            # Insert New / Update
            # code ... 
        }else{
            # Import all options //* [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            $optionsQuery = "
                SELECT name, display_name, type
                FROM  product_options
                WHERE user_id = 1
            ";

            $options = $this->db->Fetch($optionsQuery);
            foreach ($options as $option){
                $newOption = $this->clientapi->createOption($option);
                // RD($this->clientapi->getLastError());
                $optionInstance = $this->generateProductOptionInstance($newOption, $accountId, "customer");
                $whereOption = array(
                    "user_id" => $accountId,
                    "customer_option_id" => $newOption->id,
                );
                $this->db->CheckInsert("product_options", $optionInstance, $whereOption, true);
            }
        }
    }

    public function mapOptionValues($accountId, $token, $optionValueId = null, $operation = null)
    {
        # init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($optionValueId && $operation){
            # Insert New / Update
            # code ... 
        }else{ 
            # Import all option values //* [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            # get all options
            $optionsQuery = "
                SELECT master_option_id, customer_option_id
                FROM product_options
                WHERE user_id = $accountId
            ";
            $optionIds = $this->db->Fetch($optionsQuery);
            foreach ($optionIds as $optionId) {
                # go through each optionId to find/create option id
                $masterOptionId = $optionId["master_option_id"];
                $customerOptionId = $optionId["customer_option_id"];
               
                $optionValuesQuery = "
                    SELECT label, sort_order, value
                    FROM product_option_value
                    WHERE user_id = 1 AND master_option_id = $masterOptionId
                ";

                # get all option values under this option id
                $optionValues = $this->db->Fetch($optionValuesQuery);
                foreach ($optionValues as $optionValue){
                    $newOptionValue = $this->clientapi->createOptionValue($customerOptionId, $optionValue);
                    $optionValueInstance = $this->generateProductOptionValueInstance($newOptionValue, $accountId, "customer", $masterOptionId, $customerOptionId);
                    $whereOption = array(
                        "user_id" => $accountId,
                        "customer_option_value_id" => $newOptionValue->id
                    );
                    $this->db->CheckInsert("product_option_value", $optionValueInstance, $whereOption, true);
                }
            }
        }
    }

    public function mapOptionSets($accountId, $token, $optionSetId = null, $operation = null)
    {
        #init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($optionSetId && $operation){
            # Insert New / Update
            # code ...
        }else{
            # Import all option sets//* [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            # get all options
            $optionSetsQuery = "SELECT name FROM product_optionsets WHERE user_id = 1";
            $optionSets = $this->db->Fetch($optionSetsQuery);

            foreach($optionSets as $optionSet){
                $newOptionSet = $this->clientapi->createOptionSet($optionSet);
                $optionSetInstance = $this->generateProductOptionSetInstance($newOptionSet, $accountId, "customer");
                $whereOptionSet = array("user_id" => $accountId, "name" => $newOptionSet->name);
                $this->db->CheckInsert("product_optionsets", $optionSetInstance, $whereOptionSet, true);
            }
        }
    }

    public function mapOptionSetOptions($accountId, $token, $optionSetOptionId = null, $operation = null)
    {
        #init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($optionSetOptionId && $operation){
            # Insert New / Update
            # code ...
        }else{
            # Import all option sets option//* [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            $optionSetIdsQuery = "
                SELECT pos.customer_option_set_id, po.customer_option_id, pos.master_option_set_id, posp.master_option_id, posp.master_option_set_option_id, posp.display_name, posp.sort_order, posp.is_required
                FROM product_optionsets pos
                LEFT JOIN product_optionset_option posp ON pos.master_option_set_id = posp.master_option_set_id
                LEFT JOIN product_options po ON po.master_option_id = posp.master_option_id
                WHERE  pos.user_id = $accountId AND po.customer_option_id IS NOT NULL
            ";

            $optionSetIds = $this->db->Fetch($optionSetIdsQuery);
            foreach ($optionSetIds as $optionSetIdObj){
                $optionSetObjectArray = array(
                    "option_id" => $optionSetIdObj["customer_option_id"],
                    "display_name"=> $optionSetIdObj["display_name"],
                    "sort_order" => $optionSetIdObj["sort_order"],
                    // "is_required" => $optionSetIdObj["is_required"] // not allow to insert
                );
                // RD($optionSetObjectArray);
                $newOptionSetOption = $this->clientapi->createOptionSetOption($optionSetObjectArray, $optionSetIdObj["customer_option_set_id"]);
                $optionSetOptionInstance = $this->generateProductOptionSetOptionInstance($newOptionSetOption, $accountId, "customer", $optionSetIdObj["master_option_set_id"], $optionSetIdObj["master_option_id"], $optionSetIdObj["master_option_set_option_id"], $optionSetIdObj["customer_option_set_id"], $optionSetIdObj["customer_option_id"]);
                $this->db->CheckInsert("product_optionset_option", $optionSetOptionInstance, $optionSetOptionInstance);
            }
        }
    }

    public function mapProduct($accountId, $token, $productId, $customerProductId = null, $operation = null, $customerCategoryId, $bulkImport = null)
    {
        #init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($productId && $operation){
            # Insert New / Update
            # code ...
        }else{
            # Import all products // * [open issue] I hard code user_id = 1 to fetch master store thing. might need to change
            $productQuery = "
                SELECT name, type, sku, price, description,
                        inventory_level, inventory_warning_level, warranty, weight, width, height, depth, inventory_tracking, meta_keywords,
                        meta_description,upc,bin_picking_number, availability, brand_id
                FROM products
                WHERE product_id = $productId
            ";
            $product = $this->db->FetchOne($productQuery);
            $brandId = $product["brand_id"];

            # make product brand
            if (isset($brandId) && !empty($brandId)){
                $masterStoreBrandQuery = "SELECT name FROM product_brands WHERE user_id = 1 AND brand_id = $brandId";
                $masterStoreBrand = $this->db->FetchOne($masterStoreBrandQuery);
                $masterStoreBrandName = $masterStoreBrand["name"]; // get brand name. since brand name is unique

                # we check brand is in the store already or not.
                $customerBrands = $this->clientapi->getBrands(array("name" => $masterStoreBrandName));
                
                if (isset($customerBrands)){
                    $customerBrand = $customerBrands[0]; // get the return brand.
                    $product["brand_id"] = $customerBrand->id;
                }else{
                    # if this brand is not in the customer store, we have to create this brand into the customer store
                    $newCustomerStoreBrand = $this->clientapi->createBrand(array("name" => $masterStoreBrandName));
                    $product["brand_id"] = $newCustomerStoreBrand->id;
                }
            }else{
                # if there is no brand name. we just unset the brand_id from product to prevent product creating error
                unset($product["brand_id"]);
            }

            # find categories and option_set_id
            // $productSupplimentsQuery = "
            //     SELECT categories, option_set_id
            //     FROM products
            //     WHERE product_id = $productId
            // ";
            // $productSuppliments = $this->db->fetchOne($productSupplimentsQuery);

            # make customer categories array 
            // $customerCategories = array();
            // $masterCategories = split(",", $productSuppliments["categories"]);

            // foreach ($masterCategories as $masterCategoryId){
            //     // RD($masterCategoryId);
            //     $customerCategoryIdQuery = "SELECT customer_category_id FROM product_categories WHERE user_id = $accountId AND master_category_id = $masterCategoryId";
            //     $customerCategoryIdResult = $this->db->FetchOne($customerCategoryIdQuery);
            //     // RD($customerCategoryIdResult["customer_category_id"]);
            //     array_push($customerCategories, $customerCategoryIdResult["customer_category_id"]);
            // }
            // $product["categories"] = $customerCategories;

            # make customer optionset id
            // if (isset($productSuppliments["option_set_id"])){
            //     $masterOptionSetId = $productSuppliments["option_set_id"];
            //     $customerOptionSetIdQuery = "SELECT customer_option_set_id FROM product_optionsets WHERE user_id = $accountId AND master_option_set_id = $masterOptionSetId";

            //     $customerOptionSetIdResult = $this->db->FetchOne($customerOptionSetIdQuery);
            //     $product["option_set_id"] = $customerOptionSetIdResult["customer_option_set_id"];
            // }

            # assign product category
            $product["categories"] = array($customerCategoryId);

            # apply customer store map percentage 
            $mapPercentageQuery = "SELECT product_markup FROM accounts WHERE account_id = $accountId";
            $mapPercentageResult = $this->db->FetchOne($mapPercentageQuery);

            # calculate the selling price for customer store
            # Wholesale price - (WSP %) * markup
            # get account subscription from account
            $accountSubscriptionQuery = "SELECT active_subscription_level FROM accounts WHERE account_id = $accountId";
            $accountSubscription = $this->db->FetchOne($accountSubscriptionQuery);
            switch ($accountSubscription["active_subscription_level"]) {
                case "Playoff - Package":
                    $discount = 5;
                    break;
                case "Championship - Package":
                    $discount = 10;
                    break;
                default: 
                    $discount = 0;
                    break;
            }
            
            # do the calculation here
            // $product["price"] = round(($product["price"] * (1 - $discount / 100)) * (1 + $mapPercentageResult['product_markup'] / 100), 2);

            # new update. For now we will just keep it simple, no discount. no markup.
            $product["price"] = round(($product["price"] * ($mapPercentageResult['product_markup'] / 100)), 2);

            # always make it visible when import in normal case
            $product["is_visible"] = true;

            # check inventory for product
            $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
            $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);
            $inventorySubtraction = $inventorySubtractionResult["inventory_subtraction_value"];

            # if product inventory is low
            if ($product["inventory_level"] <= $inventorySubtraction){  
                $product["is_visible"] = false; // hide from customer store product list
                $product["availability"] = "disabled"; // make it no available for purchased
            }

            # create and push product to customer store
            $newProduct = $this->clientapi->createProduct($product);

            # check product is created successful or not
            if ($newProduct){
                $productInstance = $this->generateProductInstance($newProduct, $accountId, "customer", $productId);
                # Insert to account_product for mapping purpose
                $this->db->CheckInsert("account_product", $productInstance, $productInstance);
            }else{
                // Handle Product Can Be Create issue for bulk import or single select import
                if ($bulkImport){
                    # if product is imported by category. We will just send notification to customer store.
                    $notificationForCustomer = array(
                        "from_user_id" => 0, // Notication generate by system
                        "to_user_id"   => $accountId,
                        "msg_type"     => "Product Imported Error",
                        "details"      => "Product " . $product["name"] . " - " . $product["sku"] . " is already in your store, please check product name and sku. Or Products details missing, please contact customer service",
                        "status"       => "open",
                        "entity_id"    => $productId,
                        "entity_type"  => "products",
                        "entity_name"  => "Your Store"
                    );
                    $this->notificationService->createNotification($notificationForCustomer);
                    
                }else{
                    # this would hanle single select procut imported
                    JSO(array("error_msg" => "Product Imported Error. The Product is already in your store, please check product name and sku. Or Products details missing, please contact customer service"));
                }
            }
        }
    }

    public function mapProductSkus($accountId, $token, $productId, $productSkuId = null, $operation = null)
    {
        #init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        if ($productSkuId && $operation){
            # Insert or Update ...
        }else{
            # get customer product Id
            $customerProductIdQuery = "SELECT customer_product_id FROM account_product WHERE master_product_id = $productId";
            $customerProductIdResult = $this->db->FetchOne($customerProductIdQuery);
            $customerProductId = $customerProductIdResult["customer_product_id"];
            $productOptions = $this->clientapi->getProductOptions($customerProductId);// Only way to know what product option associate with this sku

            # get product sku in the master store
            $productSkusQuery = "
                SELECT *
                FROM product_sku
                WHERE user_id = 1 AND master_product_id = $productId
            ";
            $productSkus = $this->db->Fetch($productSkusQuery);

            # Check If Product has sku
            if (sizeof($productSkus) > 0){

                # for each product sku 
                foreach($productSkus as $productSku){
                    # init the sku option array
                    $productSkuObject = array(
                        "sku" => $productSku["sku"],
                        // "price" => $productSku["price"],
                        "upc" => $productSku["upc"],
                        "inventory_level" =>$productSku["inventory_level"],
                        "inventory_warning_level" =>$productSku["inventory_warning_level"],
                        "bin_picking_number" =>$productSku["bin_picking_number"],
                        "weight" => $productSku["weight"],
                        // "is_purchasing_disabled" => $productSku["is_purchasing_disabled"],
                        // "purchasing_disabled_message" => $productSku["purchasing_disabled_message"],
                        "image_file" => $productSku["image_file"]
                    );
                    # init the options here
                    $optionsArray = array(); //empty array

                    $masterProductSkuId = $productSku["master_product_sku_id"];  // prepare for option value query

                    # get option value for product option
                    $productSkuOptionValuesQuery = "
                        SELECT DISTINCT pov.customer_option_value_id
                        FROM product_option_value pov
                        LEFT JOIN product_sku_option pso
                        ON pov.master_option_value_id = pso.master_option_value_id
                        WHERE pso.master_sku_id = $masterProductSkuId AND pov.user_id = $accountId
                    ";
                    $productSkuOptionValuesIds = $this->db->Fetch($productSkuOptionValuesQuery);

                    # make options with product option and option value for sku object            
                    for ($i=0; $i < sizeof($productOptions) ; $i++) {
                        $productOptionId = $productOptions[$i]->id;
                        $optionValueId = $productSkuOptionValuesIds[$i]['customer_option_value_id'];
                        $optionValuePair = array(
                            "product_option_id" => $productOptionId,
                            "option_value_id"   => $optionValueId
                        );

                        array_push($optionsArray, $optionValuePair);
                    }

                    # make options associate with sku
                    $productSkuObject["options"] = $optionsArray;

                    // # apply customer store map percentage 
                    // $mapPercentageQuery = "SELECT product_markup FROM accounts WHERE account_id = $accountId";
                    // $mapPercentageResult = $this->db->FetchOne($mapPercentageQuery);
                    // # calculate the selling price for customer store
                    // $productSkuObject["price"] = round($productSkuObject["price"] * (1 + $mapPercentageResult['product_markup'] / 100), 2);

                    #import product sku to customer store
                    $newSku = $this->clientapi->createSku($customerProductId, $productSkuObject);

                    $newSkuInstance = array(
                        "user_id"                 => $accountId,
                        "master_product_sku_id"   => $productSku["master_product_sku_id"],
                        "master_product_id"       => $productSku["master_product_id"],
                        "customer_product_sku_id" => $newSku->id,
                        "customer_product_id"     => $customerProductId,
                        "sku"                     => $productSku["sku"]
                    );

                    # Insert to Our Local DB
                    $this->db->CheckInsert("product_sku", $newSkuInstance, $newSkuInstance);
                }
            }
        }
    }

    public function mapProductImages($accountId, $token, $productId)
    {
        #init get customer store api information
        $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $apiConnector->getClient();

        # get customer product Id
        $customerProductIdQuery = "SELECT MAX(customer_product_id) AS customer_product_id FROM account_product WHERE master_product_id = $productId AND account_id = $accountId";
        $customerProductIdResult = $this->db->FetchOne($customerProductIdQuery);
        $customerProductId = $customerProductIdResult["customer_product_id"];

        // $productImageQuery = "
        //     SELECT zoom_url as image_file, is_thumbnail, sort_order, description
        //     FROM product_image
        //     WHERE user_id = 1 AND master_product_id = $productId
        // ";

        $productImageQuery = "
            SELECT zoom_url as image_file
            FROM product_image
            WHERE user_id = 1 AND master_product_id = $productId
        ";

        $productImages = $this->db->Fetch($productImageQuery);

        foreach ($productImages as $productImage ) {
            // $productImage["is_thumbnail"] = $productImage["is_thumbnail"] == "true" ? true : false;
            $productImage["is_thumbnail"] = true;
            $this->clientapi->createProductImage($customerProductId, $productImage);
        }
    }

}