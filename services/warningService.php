<?php
/**
 * Main Warnings Service
*/

namespace LL\services;

use LL\lib\database;

class warningService
{

	public function __construct()
	{
		# define database instance
		$this->db = new \LL\lib\database\mysql();
	}

	public function get_warnings() 
	{	
		$limit = isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10;
    $page = (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0;
    $offset = $limit * $page;

    $query = "SELECT * FROM warnings WHERE status != 'suspended' ORDER BY date_created LIMIT $limit OFFSET $offset";
    $warnings = $this->db->Fetch($query);
    $count = "SELECT count(*) AS count FROM warnings";
    $max = $this->db->FetchOne($count);   
    
    $data = array(
        "items" => $warnings,
        "page" => $page + 1,
        "maxpages" => ceil($max['count'] / $limit),
        "count" => $max["count"]
        );

    http_response_code(200);
    JSO($data);
    exit;
	}
	
	public function get_warning_by_id($id)
	{
		$query = "SELECT * FROM warnings WHERE id = :id";
		$warning = $this->db->FetchOne($query, array("id" => $id));
		
		http_response_code(200);
		JSO($warning);
		exit;
	}
  # $warning is the object which contains everthing needs to be saved in the warning table
    // $warning = array(
				//     "type" => value to be filled in,
        //     "product_name" => value to be filled in,
        //     "stock_value" => value to be filled in,
        //     "description" => value to be filled in,
        //     );
	public function create_warning($warning) {
		$warning["date_created"] = date('Y-m-d H:i:s');
		$warning["date_modified"] = date('Y-m-d H:i:s');
		$warning["status"] = "open";
		$this->db->Insert("warning", $warning);
	}

	# $warning_update is the object which contains the udpate data
	public function update_warning($warning_update) {
		$id = $warning_update["id"];
		$warning_update["date_modified"] = date("Y-m-d H:i:s");
		$this->db->Update("warnings", $warning_update, array("id" => $id));
	}	
	
}
	