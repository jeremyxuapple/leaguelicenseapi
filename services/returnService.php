<?php
/**
 * Main Return Service
 */

namespace LL\services;

use LL\lib\database;
use LL\services;
use Mandrill;

class returnService
{
	public function __construct()
	{		
		# define service instances
		$this->db = new \LL\lib\database\mysql();
		$this->emailService = new \LL\services\emailService();
	}
/**
	 * Get ALL Returns sorted by date_created
	 *
	 * @param $accountId - int - 
	 * @return all the returns
	*/
	
	public function getReturns($options)
	{	
		extract($options);

		$where = isset($where_query) ? "order_id LIKE '%$where_query%' 
						OR sku LIKE '%$where_query%' 
						OR reason LIKE '%$where_query%'
						OR type LIKE '%$where_query%'" : "1 = 1";

		$query = "SELECT * FROM order_product_return 
							WHERE $where
							ORDER BY $order_option $order_sort
							LIMIT $offset, $limit";

		$returns = $this->db->Fetch($query);

		return $returns;
	}

	/**
	 * Get ALL Returns Items By AccountId And Options
	 *
	 * @param $accountId - int - 
	 *
	 *
	*/
	public function getReturnsByAccountId($accountId, $options)
	{
		extract($options);

		$where = isset($where_query) ? "order_id LIKE '%$where_query%' 
						OR sku LIKE '%$where_query%' 
						OR reason LIKE '%$where_query%'
						OR type LIKE '%$where_query%'" : "1 = 1";

		$query = "
			SELECT id, order_id, status, total_ex_tax
			FROM order_product_return
			WHERE account_id = :account_id AND " . $where . 
			" ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";

		$results = $this->db->Fetch($query, array("account_id" => $accountId));
		return $results;
	}

	/**
	 * Get All Returns Item By OrderId And AccountId
	 *
	 * @param $orderId - int
	 * @param $accountId -int
	 * 
	 * @return returns list
	 */
	public function getReturnsByIds($orderId, $accountId)
	{	
		$query = "SELECT * FROM order_product_return WHERE order_id = :order_id AND account_id = :account_id";

		$results = $this->db->Fetch($query, array(
			"order_id" => $orderId,
			"account_id" => $accountId
			));
		
		return $results;
	}

	/**
	 * Get Single One Return By Id
	 * 
	 * @param $id - int - auto_increament id from returns table
	 * @return return
	*/
	public function getReturnById($id)
	{
		$query = "SELECT * FROM order_product_return WHERE id = $id";
		$result = $this->db->FetchOne($query);
		
		return $result;
	}

	/**
	 * Generate a new instance for product_return
	 * 
	 * @return return
	*/

	public function generateProductReturnInstance() 
	{	
		$product_return = array(
				"order_id"      => $_POST["order_id"],
				"account_id"    => $_POST["account_id"],
				"shipping_id"   => $_POST["shipping_id"],
				"supplier_id"   => $_POST["supplier_id"],
				"sku"           => $_POST["sku"],
				"name"          => rawurldecode($_POST["name"]),
				"quantity_returned"      => $_POST["quantity_returned"],
				"total_ex_tax"  => $_POST["total_ex_tax"],
				"total_inc_tax" => $_POST["total_inc_tax"],
				"total_tax"     => $_POST["total_tax"],
				"reason"        => rawurldecode($_POST["reason"]),
				"type"          => rawurldecode($_POST["type"]),
				// "status"        => $_POST["status"],
				"date_created"  => date("Y-m-d H:i:s"),
				"date_modified" => date("Y-m-d H:i:s")
				);

		return $product_return;
	}

	/**
	 * Get the supplier_id By order_id, product_sku and account_id
	 * 
	 * @param order_id - the order_id which include the product to be returned
	 * @param product_sku - the product sku
	 * @param account_id - the id to identify which store create the return request
	 * @return return the supplier_id
	*/

	public function getSupplierId($order_id, $product_sku, $account_id)
	{		
		$query = "SELECT supplier_id FROM order_product 
                    WHERE account_id=$account_id 
                    AND order_id=$order_id 
                    AND sku=$product_sku";
        $supplier_id_arr = $this->db->FetchOne($query);
        extract($supplier_id_arr);
        return($supplier_id);
	}

	/**
	 * Create a new return notificaiton 
	 * 
	 * @param $returnId - int - auto_increament id in order_product_return
	 * @param $account_id - the id to identify which store create the return request
	 * @return return the return notification
	*/

	public function create_return_notification($returnId, $account_id)
	{		
		$query_store_name = "SELECT name FROM accounts WHERE account_id = $account_id";

       	$store_name_arr = $this->db->FetchOne($query_store_name);

        $store_name = $store_name_arr["name"];

        $return_query = "SELECT * FROM order_product_return WHERE id = $returnId";
        $return = $this->db->FetchOne($return_query);
        extract($return);

      	$notification = array(
          "from_user_id" => $account_id,
          "to_user_id"   => 1, // * [open issue] Need to change to mater store user_id ... 
          "msg_type"     => "Return",
          "details"      => "Store($store_name) is request a return.", 
          "status"       => "Open",
          "entity_id"    => $returnId,
          "entity_type"  => "returns",
          "entity_name"  => $store_name, 
        );

      return $notification;
	}

	/**
	 * Send out email when there is a return request be created. 
	 * 
	 * @param $returnId 
	 * @param $account_id
	 * 
	*/

	public function email_store_master($returnId, $account_id) 
  	{   
	  	$product_query = "SELECT * FROM order_product_return WHERE id=$returnId";
	    $product = $this->db->FetchOne($product_query);  

	    $account_query = "SELECT * FROM accounts WHERE account_id=$account_id";
	    $account = $this->db->FetchOne($account_query);

	    extract($product);
	    extract($account);

	    $template_content = array(
	        array(
	            'name' => "order_id",
	            'content' => $order_id
	        	),
	        array(
	            'name' => "product_name",
	            'content' => $product['name']
	        	),
	        array(
	            'name' => "quantity",
	            'content' => $quantity_returned
	        	),
	        array(
	            'name' => "reason",
	            'content' => $reason
	        	),
	        array(
	            'name' => "type",
	            'content' => $type
	        	),
	        array(
	            'name' => "accountName",
	            'content' => $account['name']
	        	),
	        array(
	            'name' => "contact_first_name",
	            'content' => $contact_first_name
	        	),
	         array(
	            'name' => "contact_last_name",
	            'content' => $contact_last_name
	        	),
	        array(
	            'name' => "contact_email",
	            'content' => $contact_email
	        	),
	        array(
	            'name' => "contact_phone",
	            'content' => $contact_phone
	        	),
	        array(
	            'name' => "active_subscription_level",
	            'content' => $active_subscription_level
	        	),
	    );

 		$this->emailService->sendMailTemplate("doug@beapartof.com", "doug@beapartof.com","A Return has been created!", "Return(CustomerMaster)",	$template_content);
  	}

  /**
	*	Master store send out email for getting approval from supplier
	* @param $return_id the id auto generated by the database
  */

  public function email_master_supplier($return_id)
  {	
  	$return_query = "SELECT * FROM order_product_return WHERE id=$return_id";
  	$return = $this->db->FetchOne($return_query);
  	extract($return);

  	$shipping_query = "SELECT * FROM order_shipping WHERE order_id=$order_id AND account_id=$account_id";
    $shipping = $this->db->FetchOne($shipping_query);
    extract($shipping);

    $template_content = array(
				array(
            'name' => "order_id",
            'content' => $order_id
        	),
        array(
            'name' => "product_name",
            'content' => $return['name']
        	),
        array(
            'name' => "quantity",
            'content' => $quantity_returned
        	),
        array(
            'name' => "reason",
            'content' => $reason
        	),
        array(
            'name' => "type",
            'content' => $type
        	),
				array(
            'name' => "first_name",
            'content' => $first_name
        	),
				array(
            'name' => "last_name",
            'content' => $last_name
        	),
				array(
            'name' => "company",
            'content' => $company
        	),
				array(
            'name' => "email",
            'content' => $email
        	),
				array(
            'name' => "phone",
            'content' => $phone
        	),
				array(
            'name' => "street_1",
            'content' => $street_1
        	),
				array(
            'name' => "street_2",
            'content' => $street_2
        	),
				array(
            'name' => "zip",
            'content' => $zip
        	),
				array(
            'name' => "state",
            'content' => $state
        	),
				array(
            'name' => "country",
            'content' => $country
        	),

			);
	$this->emailService->sendMailTemplate("doug@beapartof.com", "doug@beapartof.com", "A return request is waiting for approval", "Return(masterSupplier)",	$template_content);
  }

  /**
	*	Master store send out email for the decision of a return(either approve or decline)
	* @param $return_id the id auto generated by the database
  */

  public function email_master_customer($return_id, $status)
  {	
  		$return_status = $status;

	  	$product_query = "SELECT * FROM order_product_return WHERE id = $return_id";
	    $product = $this->db->FetchOne($product_query);  
	    $account_id = $product['account_id'];
	    $account_query = "SELECT * FROM accounts WHERE account_id = $account_id";
	    $account = $this->db->FetchOne($account_query);
	    extract($product);
	    extract($account);

			if($return_status == "Approved") {
				$title = "Congratulations! Your return has been approved!";
				$subtitle = "Your return has been approved, please see the detail information";
			} else {
				$title = "Sorry! Your order has been declined";
				$subtitle = "Your return has been declined, please see the detail information";
			}	

	    $template_content = array(
	    	 array(
	            'name' => "title",
	            'content' => $title
	        	),
	    	  array(
	            'name' => "subtitle",
	            'content' => $subtitle
	        	),
	        array(
	            'name' => "order_id",
	            'content' => $order_id
	        	),
	        array(
	            'name' => "product_name",
	            'content' => $product['name']
	        	),
	        array(
	            'name' => "quantity",
	            'content' => $quantity_returned
	        	),
	        array(
	            'name' => "reason",
	            'content' => $reason
	        	),
	        array(
	            'name' => "type",
	            'content' => $type
	        	),
	        array(
	            'name' => "accountName",
	            'content' => $account['name']
	        	),
	        array(
	            'name' => "contact_first_name",
	            'content' => $contact_first_name
	        	),
	         array(
	            'name' => "contact_last_name",
	            'content' => $contact_last_name
	        	),
	        array(
	            'name' => "contact_email",
	            'content' => $contact_email
	        	),
	        array(
	            'name' => "contact_phone",
	            'content' => $contact_phone
	        	),
	        array(
	            'name' => "active_subscription_level",
	            'content' => $active_subscription_level
	        	),
	    );

 		$this->emailService->sendMailTemplate("doug@beapartof.com", "doug@beapartof.com", $title, "Return(MasterCustomer)",	$template_content);
  	}


}

