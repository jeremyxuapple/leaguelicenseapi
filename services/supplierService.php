<?php
/** 
 * Main Supplier Service
*/

namespace LL\services;

use LL\lib\database;

class supplierService
{
	public function __construct()
	{
		# define service instace
		$this->db = new \LL\lib\database\mysql();
	}

	public function getSuppliersByProductId($productId, $options)
	{
		extract($options);

		$query = "
			SELECT s.id, s.name, s.status, sp.supplier_id, sp.inventory_level, sp.inventory_warning_level
			FROM suppliers s
			LEFT JOIN supplier_product sp
			ON s.id = sp.supplier_id
			WHERE sp.product_id =  $productId AND $where_query
			ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";

		$results = $this->db->Fetch($query);
		return $results;
	}

	public function getSuppliers($options)
	{
		extract($options);

		$where = isset($where_query) ? "name LIKE '%$where_query%' OR contact_first_name LIKE '%$where_query%' OR contact_last_name LIKE '%$where_query%' OR contact_phone LIKE '%$where_query%' OR contact_email LIKE '%$where_query%'" : "1 = 1";

		$query = "
			SELECT id, name, contact_first_name, contact_last_name, contact_phone, contact_email, status
			FROM suppliers " .
			"WHERE " . $where
			. " ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";

		$results = $this->db->Fetch($query);

		return $results;
	}

	public function getSupplierById($supplierId)
	{
		$query = "SELECT * FROM suppliers WHERE id = $supplierId";
		$supplier = $this->db->FetchOne($query);
		$supplier["locations"] = $this->getSupplierLocationsById($supplierId);

		return $supplier;
	}

	public function getSupplierLocationsById($supplierId)
	{
		$query = "SELECT * FROM supplier_location WHERE supplier_id = $supplierId";
		$results = $this->db->Fetch($query);

		return $results;
	}

	public function generateSupplierInstance()
	{	
		$supplier = array(
			"name"                   => $_POST["name"],
			"contact_first_name"     => $_POST["contact_first_name"],
			"contact_last_name"      => $_POST["contact_last_name"],
			"contact_email"          => $_POST["contact_email"],
			"contact_phone"          => $_POST["contact_phone"],
			"status"                 => $_POST["status"],
			"email_return"           => $_POST["email_return"],
			"data_feed"              => $_POST["data_feed"],
			"billing_first_name"     => $_POST["billing_first_name"],
			"billing_last_name"      => $_POST["billing_last_name"],
			"billing_company"        => $_POST["billing_company"],
			"billing_address_1"      => $_POST["billing_address_1"],
			"billing_address_2"      => $_POST["billing_address_2"],
			"billing_city"           => $_POST["billing_city"],
			"billing_state"          => $_POST["billing_state"],
			"billing_zip"            => $_POST["billing_zip"],
			"billing_country"        => $_POST["billing_country"], 
			"billing_email"          => $_POST["billing_email"],
			"billing_phone"          => $_POST["billing_phone"],
			"staff_notes"            => $_POST["staff_notes"]
		);


		foreach($supplier as $key => $value) {
			$supplier[$key] = rawurldecode($value);
		}
	
		return $supplier;
	}

	// public function generateSupplierLocationInstance()
	// {
	// 	$location = array(
	// 		"supplier_id"  => $_POST["supplier_id"],
	// 		"contact_first_name"   => $_POST["contact_first_name"],
	// 		"contact_last_name"    => $_POST["contact_last_name"],
	// 		"contact_email"        => $_POST["contact_email"],
	// 		"contact_phone"        => $_POST["contact_phone"],
	// 		"address_1"     => $_POST["address_1"],
	// 		"address_2"     => $_POST["address_2"],
	// 		"zip"          => $_POST["zip"],
	// 		"city"         => $_POST["city"],
	// 		"state"        => $_POST["state"],
	// 		"country"      => $_POST["country"],
	// 		// "country_iso2" => _POST["country_iso2"]
	// 	);
	// 	return $location;
	// }
}
