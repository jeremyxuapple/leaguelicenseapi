<?php
/** 
 * Main Orders Service
*/

namespace LL\services;

use LL\lib\database;
use LL\lib\Oauth;
use Mandrill;

class orderService
{
	public function __construct()
	{	
		# define service instance
		// $oauth = new \LL\lib\Oauth\OauthServer();
		// $token = $oauth->verifyToken();
 
		$this->invoiceService      = new \LL\services\invoiceService();
        $this->returnService       = new \LL\services\returnService();
        $this->productService      = new \LL\services\productService();
        $this->accountService      = new \LL\services\accountService();
        $this->notificationService = new \LL\services\notificationService();
        $this->emailService        = new \LL\services\emailService();

        $this->db = new \LL\lib\database\mysql();
        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();
    }

    /**
     * Get Orders Associated With Specific Account
     * 
     * @param accountId - int
     * @param $options - array
     * 
     * @return orders array 
    */ 
	public function getOrdersByAccountId($accountId, $options)
	{	
		#extract options array
		extract($options);

		$where = isset($where_query) ? "order_id LIKE '%$where_query%' OR customer_name LIKE '%$where_query%'" : "1 = 1";

		$query = "
			SELECT id, order_id, customer_name, status, total_inc_tax, DATE_FORMAT(date_created, '%Y %b %D') AS date_created
			FROM orders
			WHERE account_id = $accountId AND " . $where . 
			" ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";

		$results = $this->db->Fetch($query);
		$orders = array();

		foreach($results as $result) {

			$order_id = $result["order_id"];

			$cost_total = 0;
			$shipped_qty = 0;

			$invoices_groupByOrder = $this->invoiceService->getInvoicesByIds($order_id, $accountId);			
			
			foreach ($invoices_groupByOrder as $invoice) {	
				$cost_total += $invoice["total"];
				$shipped_qty += $invoice["quantity"];
			}

			$result["cost_total"] = $cost_total;
			$result["shipped_qty"] = $shipped_qty;

			array_push($orders, $result);

		}		

		return $orders;
	}

	/**
	 * Get a specific order by id.
	 *   
	 * @param $id - int - refered to auto increment id in orders table
	 * @return order - array
	*/
	public function getOrderById($id, $token)
	{	
		#prepare for order sync
		$orderQuery = "SELECT * FROM orders WHERE id = $id";
		$orderQueryResult = $this->db->FetchOne($orderQuery);
		$orderId = $orderQueryResult["order_id"];
		$accountId = $orderQueryResult["account_id"];

		if (isset($orderId) && isset($accountId)){
			# sync order before render it
			$this->syncOrderByIds($orderId, $accountId, $token);
		}else{

			return false;
		}

		# render order
		$orderRenderQuery = "SELECT * FROM orders WHERE id = $Id";
		$orderBillingQuery  = "SELECT * FROM order_billing WHERE order_id = $orderId AND account_id = $accountId";
		$orderShippingQuery = "SELECT * FROM order_shipping WHERE order_id = $orderId AND account_id = $accountId";

		$order                       = $this->db->FetchOne($orderQuery);
		$order["billing_address"]    = $this->db->FetchOne($orderBillingQuery); 
		$order["shipping_addresses"] = $this->db->Fetch($orderShippingQuery);  // Could be multiple shipping address for one order
		$order["products"]           = $this->getOrderProductsByIds($orderId, $accountId);
		
		$order["invoices"]           = $this->invoiceService->getInvoicesByIds($orderId, $accountId);
		$order["returns"]            = $this->returnService->getReturnsByIds($orderId, $accountId);



		return $order;
	}

	/**
	 * Get Order Base (Without Billing, Products, invoices, shippings...)
	 *
	 * @param $orderId - int -order_id in orders table
	 * @param $accountId - int
	 *
	 * @return $order - array
	*/
	public function getOrderBaseByIds($orderId, $accountId)
	{	
		$query = "SELECT * FROM orders WHERE order_id = $orderId AND account_id = $accountId";
		$result = $this->db->FetchOne($query);
		
		return $result;
	}

	public function getOrderProductsByIds($orderId, $accountId)
	{
		$query = "SELECT * FROM order_product WHERE order_id = $orderId AND account_id = $accountId";
		$results = $this->db->Fetch($query);

		return $results;
	}

	/**
	 * Get All Orders. Access By Master ONLY
	 *
	 * @param $options - array
	 * @return orders - array
	*/
	public function getOrders($options)
	{
		extract($options);
		$where = isset($where_query) ? "order_id LIKE '%$where_query%' OR customer_name LIKE '%$where_query%'" : "1 = 1";	
		$query = "
			SELECT id, order_id, customer_name, status, total_inc_tax, DATE_FORMAT(date_created, '%Y %b %D') AS date_created
			FROM orders " . 
			"WHERE " . $where
			. " ORDER BY $order_option $order_sort
			LIMIT $offset, $limit
		";
		$results = $this->db->Fetch($query);

		return $results;
	}

	/**
	 * Hanlder Order sync for both Insert or Update 
	 *
	 * @param $OrderId - int - Passed from API call or Webhooks  
	 * @param $accountId - int
	 * 
	 * @return response
	*/
	public function syncOrderByIds($orderId, $accountId, $token)
	{
		# init client api
		$api = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $api->getClient();

		# Check order exist or not ...
		$checkOrderQuery = "SELECT id FROM orders WHERE account_id = $accountId AND order_id = $orderId";
		$checkOrderResult = $this->db->FetchOne($checkOrderQuery);
		$orderProducts = $this->clientapi->getOrderProducts($orderId); // get order products from the customer store
		$checkOrderCondition = array("order_id" => $orderId, "account_id" => $accountId); // update condition

		if (isset($checkOrderResult["id"])){
			# order exist. updated this order
			$this->syncOrderUpdate($orderId, $accountId, $orderProducts, $checkOrderCondition, $token);
		}else{
			# order not exist, check order product first to see insert or ignore
			$this->syncOrderCreate($orderId, $accountId, $orderProducts, $checkOrderCondition, $token);
		}
	}

	/**
	 * Sync Order Helper Function For Insert Order
	 *
	 * @param $order - int - customer store orderId
	 * @param $orderProducts - object array - order product objects grab from BC api
	 *
	*/
	public function syncOrderCreate($orderId, $accountId, $orderProducts, $checkOrderCondition, $token)
	{
		# init client api
		$api = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $api->getClient();

		$LLproductsIncluded = false;  // default
		$orderProductInstances = array(); // init order product array

		foreach ($orderProducts as $product) {
			# get the product sku
			$sku = $product->sku;
			# check sku in our products table
			$checkLLProductQuery = "SELECT product_id FROM products WHERE sku = '$sku'";
			$checkLLProductResult = $this->db->FetchOne($checkLLProductQuery);

			# check sku in our product_sku table
			$checkLLProductSkusQuery = "SELECT master_product_id as product_id FROM product_sku WHERE sku = '$sku'";
			$checkLLProductSkusResult = $this->db->FetchOne($checkLLProductSkusQuery);

			#create order product instance
			$orderProductInstance = $this->generateOrderProductInstance($product);

			# this product is belongs to LL
			if (isset($checkLLProductResult["product_id"]) || isset($checkLLProductSkusResult["product_id"])){
				$LLproductsIncluded = true;

				# associate our product Id to this product
				if (isset($checkLLProductResult["product_id"])){
					# handle cases for product has no skus
					$orderProductInstance["master_product_id"] = $checkLLProductResult["product_id"];
				}else{
					# handle cases for product has skus
					$orderProductInstance["master_product_id"] = $checkLLProductSkusResult["product_id"];
				}
				
				$orderProductInstance["fulfillment"] = "Awaiting Fulfillment"; // default status for order create
				$orderProductInstance["account_id"]  = $accountId;
				$orderProductInstance["supplier_id"] = 1;  // default to this supplier, may change to dynamic later 
			}else{
				$orderProductInstance["fulfillment"] = "External Product";
				$orderProductInstance["account_id"]  = $accountId;
			}

			# make order produc array and wait for insert
			array_push($orderProductInstances, $orderProductInstance);
		}
		# This order has LL products
		if ($LLproductsIncluded){

			$order = $this->clientapi->getOrder($orderId);

			# orders table
			$orderInstance = $this->generateOrderInstance($order);

			# check if the order is placed by guest or customer
			# In BC. customer_id = 0 means order placed by customer
			if ($order->customer_id != 0){
				$customer = $this->clientapi->getCustomer($order->customer_id);
				$orderInstance["customer_name"]  = $customer->first_name . " " . $customer->last_name;
			}

			$orderInstance["order_id"]       = $orderId;
			$orderInstance["account_id"]     = $accountId;
			$orderInstance["status"]         = "Awaiting Fulfillment"; // default status for order create
			$orderInstance["date_created"]   = date("Y-m-d H:i:s");

			# insert order
			$orderTableIndex = $this->db->CheckInsert("orders", $orderInstance, $checkOrderCondition); // check Insert

			# order_billings table
			$billingInstance = $this->generateBillingInstance($order->billing_address);
			$billingInstance["order_id"]   = $orderId;
			$billingInstance["account_id"] = $accountId;
			# insert billing
			$this->db->CheckInsert("order_billing", $billingInstance, $billingInstance); // check Insert

			# order_shipping table
			$shipping_addresses = $this->clientapi->getOrderShippingAddresses($orderId);
			foreach ($shipping_addresses as $shipping_address) {
				$shippingInstance = $this->generateShippingInstance($shipping_address);
				$shippingInstance["order_id"] = $orderId;
				$shippingInstance["account_id"] = $accountId;
				# insert shipping
				$this->db->CheckInsert("order_shipping", $shippingInstance, $shippingInstance); // check Insert
			}

			# order_product table
			foreach ($orderProductInstances as $orderProduct) {
				# insert order product
				$this->db->CheckInsert("order_product", $orderProduct, $orderProduct);
			}
		} // End of LLproducts included

		# generate notification
		$this->createNewOrderNotification($accountId, $orderId);
	}

	/**
	 * Sync Order Helper Function For Update Order
	 *
	 * @param $order - int - customer store orderId
	 * @param $orderProducts - object array - order product objects grab from BC api
	 * @param $checkOrderCondition - array update condition
	 * 
	*/
	public function syncOrderUpdate($orderId, $accountId, $orderProducts, $checkOrderCondition, $token)
	{
		# init client api
		$api = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $api->getClient();

		## orders table
		$order = $this->clientapi->getOrder($orderId);
		$customer = $this->clientapi->getCustomer($order->customer_id);
		$orderInstance = $this->generateOrderInstance($order);
		$orderInstance["date_modified"] = date("Y-m-d H:i:s");
		$orderInstance["customer_name"]  = $customer->first_name . " " . $customer->last_name;
		$this->db->Update("orders", $orderInstance, $checkOrderCondition);

		## order_billings table
		$billingInstance = $this->generateBillingInstance($order->billing_address);
		$this->db->Update("order_billing", $billingInstance, $checkOrderCondition);

		## order_shipping table
		$shipping_addresses = $this->clientapi->getOrderShippingAddresses($orderId);
		$shipping_addresses_ref = array();  // track ddown shipping_id

		foreach ($shipping_addresses as $shipping_address) {
			$shippingInstance = $this->generateShippingInstance($shipping_address);
			$shippingInstance["order_id"] = $orderId;
			$shippingInstance["account_id"] = $accountId;

			$whereShipping = array(
				"order_id"    => $orderId,
				"account_id"  => $accountId,
				"shipping_id" => $shipping_address->id
			);

			# insert/update shipping - Handle New or Exist Update
			$this->db->CheckInsert("order_shipping", $shippingInstance, $whereShipping);
			array_push($shipping_addresses_ref, $shipping_address->id);
		}

		# get shipping_ids from api DB and prepare to check if they are in the reference - Handle Delete
		$shipping_address_api_query  = "SELECT shipping_id FROM order_shipping WHERE order_id = $orderId AND account_id = $accountId";
		$shipping_address_api_result = $this->db->Fetch($shipping_address_api_query);

		foreach ($shipping_address_api_result as $singShipping){
			if (!in_array($singShipping["shipping_id"], $shipping_addresses_ref)){
				# delete this record from Our APi DB, since the original order has removed this shipping address
				$this->db->Delete("order_shipping", array("order_id" => $orderId, "account_id"  => $accountId, "shipping_id" => $singShipping["shipping_id"]));
			}
		}

		## order_product table
		$orderProductsAPiQuery = "SELECT account_product_ref_id FROM order_product WHERE order_id = $orderId AND account_id = $accountId";
		$orderProductsAPiResult = $this->db->Fetch($orderProductsAPiQuery);
		$orderProductsAPiRef = array(); // make reference to compare for handling sync new/update/delete
		foreach ($orderProductsAPiResult as $key => $value) {
			array_push($orderProductsAPiRef, $value["account_product_ref_id"]);
		}

		# prepare where clause for update
		$whereOrderProduct = array(
			"order_id"               => $orderId,
			"account_id"             => $accountId,
		);

		foreach ($orderProducts as $product) {
			#create order product instance
			$orderProductInstance = $this->generateOrderProductInstance($product);

			# compare product is in api db ref or not
			if (in_array($product->id, $orderProductsAPiRef)){
				$whereOrderProduct["account_product_ref_id"] = $product->id; //for handle customer store copy product operation. auto_increment id from customer store

				# update the record
				$isUpdated = $this->db->Update("order_product", $orderProductInstance, $whereOrderProduct, true); // enable update row check

				if ($isUpdated){
					# when product is update, reset the fulfillment  
					$this->db->Update("order_product", array("fulfillment" => "Awaiting Fulfillment"), $whereOrderProduct);
				}
			
				# also update the $orderProductsAPiRef and prepare for Delete
				$orderProductsAPiRef = array_diff($orderProductsAPiRef, array($product->id));
			}else{

				# insert new order porduct
				$sku = $product->sku; // manufactory sku.  we are not allow to change the sku here
				# check sku in our products table
				$checkLLProductQuery = "SELECT product_id FROM products WHERE sku = '$sku'";
				$checkLLProductResult = $this->db->FetchOne($checkLLProductQuery);

				# check sku in our product_sku table
				$checkLLProductSkusQuery = "SELECT master_product_id as product_id FROM product_sku WHERE sku = '$sku'";
				$checkLLProductSkusResult = $this->db->FetchOne($checkLLProductSkusQuery);
			
				#this product is belongs to LL
				if (isset($checkLLProductResult["product_id"]) || isset($checkLLProductSkusResult["product_id"])){

					# associate our product Id to this product
					if (isset($checkLLProductResult["product_id"])){
						# handle cases for product has no skus
						$orderProductInstance["master_product_id"] = $checkLLProductResult["product_id"];
					}else{
						# handle cases for product has skus
						$orderProductInstance["master_product_id"] = $checkLLProductSkusResult["product_id"];
					}
					$orderProductInstance["fulfillment"] = "Awaiting Fulfillment"; // default status for order create
					$orderProductInstance["account_id"]  = $accountId;
					$orderProductInstance["supplier_id"] = 1;  // default to this supplier, may change to dynamic later 
				}else{
					$orderProductInstance["fulfillment"] = "External Product";
					$orderProductInstance["account_id"]  = $accountId;
				}

				# insert order product
				$this->db->Insert("order_product", $orderProductInstance);
			}
		}

		# Delete order product. if the ref array still have elements left. it means customer store already remove the product from this order

		if (sizeof($orderProductsAPiRef) > 0){
			# prepare where clause for update
			foreach ($orderProductsAPiRef as $value) {
				$whereOrderProduct["account_product_ref_id"] = $value;
				$this->db->delete("order_product", $whereOrderProduct);
			}
		}
	}

	/**
	 * Generate Order Array Prepare for DB
	 * 
	 * @param $order - object - order object grab from BC api
	 * @return order instance - array
	*/
	public function generateOrderInstance($order)
	{
		$orderBase = array(
			// "date_created"            => date("Y-m-d H:i:s"),
			// "date_modified"           => $order->date_modified,
			"origin_store_status"     => $order->status,
			"origin_store_status_id"  => $order->status_id,
			"subtotal_ex_tax"         => $order->subtotal_ex_tax,
			"subtotal_inc_tax"        => $order->subtotal_inc_tax,
			"subtotal_tax"            => $order->subtotal_tax,
			"total_ex_tax"            => $order->total_ex_tax,
			"total_inc_tax"           => $order->total_inc_tax,
			"total_tax"               => $order->total_tax,
			"items_total"             => $order->items_total,
			"items_shipped"           => $order->items_shipped,
			"payment_method"          => $order->payment_method,
			"payment_provider_id"     => $order->payment_provider_id,
			"payment_status"          => $order->payment_status,
			"refunded_amount"         => $order->refunded_amount,
			"currency_id"             => $order->currency_id,
			"currency_code"           => $order->currency_code,
			"currency_exchange_rate"  => $order->currency_exchange_rate,
			"default_currency_id"     => $order->default_currency_id,
			"default_currency_code"   => $order->default_currency_code,
			"discount_amount"         => $order->discount_amount,
			"coupon_discount"         => $order->coupon_discount,
			"staff_notes"             => $order->staff_notes,
			"customer_message"        => $order->customer_message,
			"is_deleted"              => $order->is_deleted,
			"geoip_country"           => $order->geoip_country,
			"geoip_country_iso2"      => $order->geoip_country_iso2,
		);
		return $orderBase;
	}

	/**
	 * Generate Order Billing Array Prepare for DB
	 * 
	 * @param $billing - object - order object grab from BC api
	 * @return order billing instance - array
	*/
	public function generateBillingInstance($billing)
	{
		$billingInstance = array(
			"first_name"   => $billing->first_name,
			"last_name"    => $billing->last_name,
			"company"      => $billing->company,
			"street_1"     => $billing->street_1,
			"street_2"     => $billing->street_2,
			"city"         => $billing->city,
			"state"        => $billing->state,
			"zip"          => $billing->zip,
			"country"      => $billing->country,
			"country_iso2" => $billing->country_iso2,
			"phone"        => $billing->phone,
			"email"        => $billing->email
		);

		return $billingInstance; 
	}

	/**
	 * Generate Order Shipping Array Prepare for DB
	 * 
	 * @param $shipping - object - order object grab from BC api
	 * @return order shipping instance - array
	*/
	public function generateShippingInstance($shipping)
	{
		$shippingInstance = array(
			"shipping_id"  => $shipping->id,
			"first_name"   => $shipping->first_name,
			"last_name"    => $shipping->last_name,
			"company"      => $shipping->company,
			"street_1"     => $shipping->street_1,
			"street_2"     => $shipping->street_2,
			"zip"          => $shipping->zip,
			"city"         => $shipping->city,
			"state"        => $shipping->state,
			"country"      => $shipping->country,
			"country_iso2" => $shipping->country_iso2,
			"phone"        => $shipping->phone,
			"email"        => $shipping->email
		);

		return $shippingInstance;
	}

	/**
	 * Generate Order Product Array Prepare for DB
	 * 
	 * @param $product - object - order object grab from BC api
	 * @return order product instance - array
	*/
	public function generateOrderProductInstance($product)
	{
		$orderProductInstance = array(
			"account_product_ref_id" => $product->id, // order_product auto increment id. make it as ref for each product
			"order_id"               => $product->order_id,
			"shipping_id"            => $product->order_address_id,
			"product_id"             => $product->product_id,  // customer store product id
			"return_id"              => $product->return_id,
			"name"                   => $product->name,
			"sku"                    => $product->sku,
			"type"                   => $product->type,
			"base_price"             => $product->base_price,
			"base_cost_price"        => $product->base_cost_price,
			"price_ex_tax"           => $product->price_ex_tax,
			"price_inc_tax"          => $product->price_inc_tax,
			"price_tax"              => $product->price_tax,
			"base_total"             => $product->base_total,
			"total_ex_tax"           => $product->total_ex_tax,
			"total_inc_tax"          => $product->total_inc_tax,
			"total_tax"              => $product->total_tax,
			"weight"                 => $product->weight,
			"quantity"               => $product->quantity,
			"cost_price_inc_tax"     => $product->cost_price_inc_tax,
			"cost_price_ex_tax"      => $product->cost_price_ex_tax,
			"cost_price_tax"         => $product->cost_price_tax,
			"is_refunded"            => $product->is_refunded,
			"quantity_refunded"      => $product->quantity_refunded,
			"refund_amount"          => $product->refund_amount,
			"quantity_shipped"       => $product->quantity_shipped
		);

		return $orderProductInstance;
	}

	public function createOrderUpdateNotification($accountId, $orderId)
	{
		$query_store_name = "SELECT name FROM accounts WHERE account_id = $accountId";
       	$store_name_arr = $this->db->FetchOne($query_store_name);
        $store_name = $store_name_arr["name"];
		$notificationForCustomer = array(
            "from_user_id" => 0, // Notification generate by System
            "to_user_id"   => $accountId,
            "msg_type"     => "Order Fulfillment",
            "details"      => "Order (Id: $orderId) Has Fulfillment. Sent to Supplier",
            "status"       => "Open",
            "entity_id"    => $orderId,
            "entity_type"  => "Orders",
            "entity_name"  => "Your Store"
        );
        # make system send notification to customer store
        $this->notificationService->createNotification($notificationForCustomer);
        
        // $notificationForMaster = array(
        //     "from_user_id" => 0, // Notification generate by System
        //     "to_user_id"   => 1, // * [open issue] Need to change to mater store user_id ... 
        //     "msg_type"     => "Order Fulfillment",
        //     "details"      => "Order (Id: $orderId) Has Fulfillment. Sent to Supplier",
        //     "status"       => "Open",
        //     "entity_id"    => $orderId,
        //     "entity_type"  => "Orders",
        //     "entity_name"  => $store_name,
        // );
        
        // $this->notificationService->createNotification($notificationForMaster);
	}

	public function createNewOrderNotification($accountId, $orderId)
	{
        $notification = array(
            "from_user_id" => 0, // Notification generate by System
            "to_user_id"   => $accountId,
            "msg_type"     => "Order Fulfillment",
            "details"      => "New Order (Id: $orderId) Has Been Created. Waitting For Fulfillment",
            "status"       => "Open",
            "entity_id"    => $orderId,
            "entity_type"  => "Orders",
            "entity_name"  => "Your Store"
        );

        $this->notificationService->createNotification($notification);
	}

	/**
	 * Generate an email when a product has been chosen to be fulfillmented
	 * @param $order_id
	 * @param $account_id
	 * @param $product_id
     *
	*/
	public function email_store_supplier($orderId, $accountId, $productIds)
	{
		# get shipping informaiton
		$shipping_query = "
			SELECT * FROM order_shipping
			WHERE order_id = $orderId AND account_id = $accountId
		";
		$shipping = $this->db->FetchOne($shipping_query);

		# get products array
		$products_query = "
			SELECT * FROM order_product 
			WHERE order_id = $orderId AND account_id = $accountId AND master_product_id IN (" . implode(",", $productIds) . ")
		";
		$products = $this->db->Fetch($products_query);

		# get account information
		$account_query = "
			SELECT * FROM accounts
			WHERE account_id = $accountId
		";
		$account = $this->db->FetchOne($account_query);

		# get Order information
		$order_query = "
			SELECT * FROM orders
			WHERE order_id = $orderId AND account_id = $accountId
		";
		$order = $this->db->FetchOne($order_query);

		# make customer store information
		$storeContent = "<b>From Store :&nbsp&nbsp</b>" . $account['name'] . "<br>" .
						"<b>Seller License Order # :&nbsp&nbsp</b>" . $order['id'] . "<br>" .
						"<b>Subscriber Order # :&nbsp&nbsp</b>" . $order['order_id'] . "<br>";


		# make order products title information:
		if (sizeof($products) > 1){
			$productsContentTitle = "Order Products:";
		}else{
			$productsContentTitle = "Order Product:";
		}

		# make order Products information:
		$productsContent = ""; // init str
		foreach ($products as $product) {
			$productsContent .= "<b>Product sku :&nbsp&nbsp</b>" . $product['sku'] . "<br>" .
								"<b>Product Name :&nbsp&nbsp</b>" . $product['name'] . "<br>" .
								"<b>Quantity :&nbsp&nbsp</b>" . $product['quantity'] . "<br><br><hr>";
		}

		# make shipping information:
		$shippingContent = 	"<b>Shipping First Name :&nbsp&nbsp</b>" . $shipping['first_name'] . "<br>" .
							"<b>Shipping Last Name :&nbsp&nbsp</b>" . $shipping['last_name'] . "<br>" .
							"<b>Shipping Company :&nbsp&nbsp</b>" . $shipping['company'] . "<br>" .
							"<b>Shipping Phone :&nbsp&nbsp</b>" . $shipping['phone'] . "<br>" .
							"<b>Shipping Address 1 :&nbsp&nbsp</b>" . $shipping['street_1'] . "<br>" .
							"<b>Shipping Address 2 :&nbsp&nbsp</b>" . $shipping['street_2'] . "<br>" .
							"<b>Shipping Zip :&nbsp&nbsp</b>" . $shipping['zip'] . "<br>" .
							"<b>Shipping City :&nbsp&nbsp</b>" . $shipping['city'] . "<br>" .
							"<b>Shipping State :&nbsp&nbsp</b>" . $shipping['state'] . "<br>" .
							"<b>Shipping Country :&nbsp&nbsp</b>" . $shipping['country'] . "<br>";

		# make template content for email
		$template_content = array(
			array(
				'name'    => 'storeContent',
				'content' => $storeContent
			),
			array(
				'name'    => 'productsContentTitle',
				'content' => $productsContentTitle
			),
			array(
				'name'    => 'productsContent',
				'content' => $productsContent
			),
			array(
				'name'    => 'shippingContent',
				'content' => $shippingContent
			),
		);
		
		# Send Email To Supplier
		$this->emailService->sendMailTemplate("orders@sellerlicense.com", "dropship@caseys-distributing.com", "An Order fulfillment request has been made", "Fulfill(StoreSupplier)", $template_content);
	} //End of function email_store_supplier(...)


}
