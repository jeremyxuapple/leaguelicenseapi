<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization, Content-Type, Accept, X-Auth-Client, X-Auth-Token');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, HEAD, OPTIONS');

include(dirname(__FILE__)."/init.php");

die("here");
use LL\lib\handler;

try {
    $handle = new \LL\lib\handler\Handler();
}

catch (\Exception $e) {
    $error = array(
        "error" => true,
        "file" => $e->getFile(),
        "code" => $e->getCode(),
        "line" => $e->getLine(),
        "message" => $e->getMessage(),
        "trace" => $e->getTrace(),
    );

    JSO($error);
}