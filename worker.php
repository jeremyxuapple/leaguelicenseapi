<?php
include(dirname(__FILE__)."/init.php");

use LL\services;

try {
    $service = new \LL\services\queueService();
    $service->runQueue();
    echo "end";
}

catch (\Exception $e) {
    $error = array(
        "error" => true,
        "file" => $e->getFile(),
        "code" => $e->getCode(),
        "line" => $e->getLine(),
        "message" => $e->getMessage(),
        "trace" => $e->getTrace(),
    );

    RD($error);
}