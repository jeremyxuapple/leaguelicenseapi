# LeagueLicence

Let's keep track of basic api functions

# Suppliers

## GET

##### /api/v1/suppliers
- this will get a list of the most recent suppliers (limit 10).

##### /api/v1/suppliers/{id}
- this will get all relevant data from 1 supplier.

##### /api/v1/suppliers/{id}/products
- this will give a list of all products related to this supplier (limit 10 for now).

#### /api/v1/suppliers/{id}/orders
- this will get a list of all orders related to this supplier (limit 10 for now).

##### /api/v1/suppliers/0/invoice
- note that the id has been set as 0, the master store login will get all the recent invoice while the supplier and customer_store login will get their own related invoices.

##### /api/v1/suppliers/{id}/invoice
- when the id is not 0, the endpoint will return the specific invoice if the access has been granted.

## POST

##### /api/v1/suppliers
- this will create a new supplier regardless if it exists

##### /api/v1/suppliers/{id}
- this will update a supplier with new information

##### /api/v1/suppliers/{0}/invoice
- this create invoice after the order has been shipped out. Note the id needs to be set as 0 to create a new invoice, and this endpoin will generate a order store on the Big Commerce Master Store.

## PUT

##### /api/v1/suppliers
- this will create a new supplier if it does exist already (compares all variables)

##### /api/v1/suppliers/{id}
- this will update a supplier with new information

##### /api/v1/suppliers/{id}/invoice
- this update invoice after the specific order has been shipped out.

## DELETE

##### /api/v1/suppliers/{id}
- this will delete this supplier (for now we mark it as deleted -don't delete the record)

# Products

## GET

##### /api/v1/products
- this will get a list of the most recent suppliers (limit 10)

##### /api/v1/products/{id}
- this will get all relevant data for a product from api database

##### /api/v1/products/{id}/supplier/{id}
- this will get all suppliers that sell this product

## POST

##### /api/v1/products
- this will create a new product in the master store
- this will create a new product in the local database

##### /api/v1/products/{id}
- this will update the product in the master store and the local database
- this will update/or add any related associations 

## PUT

##### /api/v1/products
- this will create a new product in the master store
- this will create a new product in the local database
- if it exists it will either update relevant product data pieces or create new associations (if has new supplier information)

##### /api/v1/products/{id}
- this will update the product in the master store and the local database
- this will update/or add any related associations 

## DELETE

##### /api/v1/products/{id}
- this will delete the product from the master store and the api database (for now we mark it as deleted -don't delete the record)

# Orders

## GET

##### /api/v1/orders
- this will get a list of the most recent orders (limit 10)

##### /api/v1/orders/{id}
- this will get a specific order 
- order information includes relevant product information, shipping information and billing information

## POST

##### /api/v1/orders
- this will insert a new order into the api database
- remember to keep the store information (which store the order was originally placed)
- this information can be BigCommerce information or Shopify information

##### /api/v1/orders/{id}
- this will update the order information for a specific order 

##### /api/v1/orders/{id}/products
- this will update the order products for a specific order

##### /api/v1/orders/{id}/shipping
- this will update the order shipping for a specific order

##### /api/v1/orders/{id}/billing
- this will update the order billing for a specific order

## PUT

##### /api/v1/orders
- this will insert a new order into the api database
- remember to keep the store information (which store the order was originally placed)
- this information can be BigCommerce information or Shopify information

##### /api/v1/orders/{id}
- this will update the order information for a specific order

##### /api/v1/orders/{id}/products
- this will update the order products for a specific order

##### /api/v1/orders/{id}/shipping
- this will update the order shipping for a specific order

##### /api/v1/orders/{id}/billing
- this will update the order billing for a specific order

## DELETE

##### /api/v1/orders/{id}
- this will delete the order from the database (for now we mark it as deleted -don't delete the record)
# Accounts

## GET 

#### /api/v1/accounts
- this will get a list of the most recent registerd accounts (limit 10)

#### /api/v1/accounts/{id}
- this will get all relevent data from 1 account

#### /api/v1/accounts/{id}/products

- this will give a list of all products related to this account (deprecated).

#### /api/v1/accounts/{id}/orders
- this will give a list of all orders related to this account

#### /api/v1/accounts/{id}/invoices
- this will give a list of all invoices related to this account

## POST 

#### /api/v1/accounts
- this will create a new account regardless if it exists

#### /api/v1/accounts/{id}
- this will update a account with new infortmation

## PUT

#### /api/v1/accounts
- this will create a new account if it does exist already (compares all variables)

#### /api/v1/accounts/{id}
- this will update a account with new infortmation

## DELETE

#### /api/v1/accounts/{id}
- this will delete this account (for now we mark it as deleted - dont't delete the record)

# Notifications

## GET 

#### /api/v1/notifications
- this will get a list of the most recent registerd accounts (limit 10)


#### /api/v1/notifications/id
- this will get the specific notification by id




# Accounts

## GET 

#### /api/v1/accounts
- this will get a list of the most recent registerd accounts (limit 10)

#### /api/v1/accounts/{id}
- this will get all relevent data from 1 account

#### /api/v1/accounts/{id}/products
- this will give a list of all products related to this account

#### /api/v1/accounts/{id}/orders
- this will give a list of all orders related to this account

#### /api/v1/accounts/{id}/invoices
- this will give a list of all invoices related to this account

## POST 

#### /api/v1/accounts
- this will create a new account regardless if it exists

#### /api/v1/accounts/{id}
- this will update a account with new infortmation

## PUT

#### /api/v1/accounts
- this will create a new account if it does exist already (compares all variables)

#### /api/v1/accounts/{id}
- this will update a account with new infortmation

## DELETE

#### /api/v1/accounts/{id}
- this will delete this account (for now we mark it as deleted - dont't delete the record)

# Composer Requirements

- composer require bigcommerce/api
- composer require bshaffer/oauth2-server-php "~1.8"