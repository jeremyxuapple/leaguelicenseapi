<?php
/**
 * Main Orders Controller
 * @param $id - auto increment id in orders table
 */

namespace LL\controllers\orders;
use LL\lib\database;
use LL\services;

class ordersIdController
{   
    /**
     * Construct function that handles supplier requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific supplier ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = null)
    {   
        $this->db = new \LL\lib\database\mysql();
        // $this->validator = new \LL\lib\helper\validator();
        $this->orderService = new \LL\services\orderService();
        $this->notificationService = new \LL\services\notificationService();
        $this->accountOrderService = new \LL\services\accountOrderService();
        // $this->invoiceService = new \LL\services\invoiceService();
        // $this->returnService  = new \LL\services\returnService();
        if($method == "put") {
            parse_str(file_get_contents('php://input'), $_POST);
        }

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessiblity = $this->checkAccessible($token);
        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
    }

    /**
     * Handle Order Get Method
     *
     * @param void / $id - int - auto increment id from orders table
     * @param $extra - string - URI
     * @param $token - object
     *
    */
    private function get($id = null, $extra = null, $token = false)
    {   
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array();

        # get orders information associate to orderId.
        if (isset($id)){

            switch($this->accessiblity) {
                case "supplier":
                    // * Need to get order product ... 
                    break;
                case "customerStore":
                
                    if ($extra == "rma"){
                        $query = "SELECT order_id, account_id FROM orders WHERE id = $id";
                        $result = $this->db->FetchOne($query);
                        $data["rma"] = $this->orderService->getOrderProductsByIds($result['order_id'], $result["account_id"]);
                    }else{
                        
                        $data["order"] = $this->orderService->getOrderById($id, $token); 
                        // auto_increment id
                    }
                    break;
                case "master":
                    $data["order"] = $this->accountOrderService->get_account_order_by_id($id);
                    break;
            }
        }

        # get orders list, accessible by master store only.
        else{
            if ($this->accessiblity === "master"){
                $orders = $this->accountOrderService->get_account_orders($options);
                
                if (isset($orders)){
                    $data["orders"] = $orders;
                    $count = "SELECT count(*) AS count FROM orders";
                    $max = $this->db->FetchOne($count);
                }
            }else{
                
                # fail for accessible check 
                // http_response_code(403);
                JSO(array("Error" => "403 Access Forbidden"));
                throw new \Exception("Unable To Identify, Access Forbidden");
            }
 
            # define basic data array for feedback
            $data["page"]          = $page + 1;
            $data["limit"]         = $limit;
            $data["count"]         = $max["count"];
            $data["maxpages"]      = ceil($max["count"] / $limit);
            $data["order_option"]  = $order_option;
            $data["order_sort"]    = $order_sort;
        }

        // if (sizeof($data["orders"]) > 0 || sizeof($data["order"]) > 0 || sizeof($data["rma"])){
            
        // }

        JSO($data);
        exit;

        # no data found
        // http_response_code(404);
        // JSO(array("Error" => "404 Not Found"));
        // exit;
    }

    /** 
     * Open Endpoint for future development
    */
    private function post()
    {
        // http_response_code(501);
        JSO(array("Error" => "Not Implemented, Might Open In The Future"));
        throw new \Exception("Not Implemented, Might Open In The Future");
    }

    /** 
     * Handler Order Product Fulfillment Update
     * @param $id - auto increament id in orders stable
     * @return response 
    */
    private function put($id = null, $extra =null, $token = null)
    {
        # Only customer Store can change order or order_product's fulfillment
        if ($this->accessiblity == "customerStore"){

            # init the client api
            $api = new \LL\lib\BCAPI\customerStoreConnector($token);
            $this->clientapi = $api->getClient();

            $accountId = $token["user_id"];
            $products = $_POST["products"];

            # check if products pass in, the we check order status
            if (sizeof($products) > 0){
                $checkOrderId = $products[0]["order_id"];
                $orderStatusQuery = "SELECT status FROM orders WHERE account_id = $accountId AND order_id = $checkOrderId";
                $orderStatusResult = $this->db->FetchOne($orderStatusQuery);
                # to prevent the products send again.
                if ($orderStatusResult["status"] == "Awaiting Shipment" || $orderStatusResult["status"] == "Internal Fulfillment"){
                    # fail cases 
                    throw new \Exception("Your Order Is Under Processing, Please Do Not Submit Again");
                }

                # first Check all waitting fulfillment products being passing.
                $passCheck = false;  // init the check status
                foreach ($products as $product) { // loop through products list
                    if (rawurldecode($product["fulfillment"]) == "Seller Licence" || rawurldecode($product["fulfillment"]) == "Internal"){
                        $passCheck = true;
                    }else{
                        $passCheck = false;
                    }
                }

                # if passCheck is true then do work on DB and send notification 
                if ($passCheck){
                    $LLfulfillmentProducts = array(); // init the SL
                    foreach ($products as $product) {
                        $productFulfillment = array("fulfillment" => rawurldecode($product["fulfillment"]));
                        $where = array(
                            "order_id"    => $product["order_id"],
                            "account_id"  => $product["account_id"],
                            "master_product_id" => $product["master_product_id"]
                        );

                        $res = $this->db->Update("order_product", $productFulfillment, $where);
                        if($res && rawurldecode($product["fulfillment"]) == "Seller Licence") {
                            $customerOrderId = $product["order_id"];

                            // #send Email
                            // $this->orderService->email_store_supplier($product["order_id"], $product["account_id"], $product["master_product_id"]);

                            # Add prodcut Id and prepare for sending email to supplier
                            array_push($LLfulfillmentProducts, $product["master_product_id"]);

                            #send notification here * [open issue. notification send to master store or customer store or Both ?];
                            $this->orderService->createOrderUpdateNotification($accountId, $product["order_id"]);
                        }else{
                            # get customer store order id. But this one is for interal update. All products being choosing for internal
                            $customerOrderIdForInteral = $product["order_id"];
                        }
                    } // end of foreach products

                    # start to handle Email Sending
                    if (sizeof($LLfulfillmentProducts) > 0){
                        $this->orderService->email_store_supplier($checkOrderId, $accountId, $LLfulfillmentProducts);
                    }
                    
                    # after Email Sent. we update the order status on DB and also changed status on the original order
                    # if not Email Sent. We also allow them make the change again
                    if (isset($customerOrderId)){ // we know it will send out email
                        $accountId = $token["user_id"];
                        $this->db->Update("orders", array("status" => "Awaiting Shipment"), array("account_id" => $accountId, "order_id" => $customerOrderId));

                        # update customer store order as well
                        $this->clientapi->updateOrder($customerOrderId, array("status_id" => 9));
                    }else{
                        # this order is fulfill by Internal
                        # update customer store order as well
                        $accountId = $token["user_id"];
                        $this->db->Update("orders", array("status" => "Internal Fulfillment"), array("account_id" => $accountId, "order_id" => $customerOrderIdForInteral));

                        # update customer store order as well
                        $this->clientapi->updateOrder($customerOrderId, array("status_id" => 9));
                    }

                }else{
                    # submit without all product being fulfilled
                    # not pass the check
                    throw new \Exception("Please Fulfill All Your Products");
                }
            } // End Of size products > 0
        }else{
            # wrong role
            throw new \Exception("Unable To Identify, Access Forbidden");
        }

        # response
        JSO(array("success" => true));
        exit;
    } // End of function put()...
    
    private function delete($id, $extra = null, $token = false)
    {   
        if ($this->accessiblity == "master"){
            $result = $this->db->Update("orders", array("status" => "Archived"), array("id" => $id));
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
        
        http_response_code(200);
        JSO(array("response" => "Order With ID: $id Has Been Archived"));
        exit;
    }

    /**
     * Accessibility check for masterstore, supplier, 
     *
     * @param $id - int - Auto Increment id in orders table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($token)
    {   
        # identify who use this toke
        $roleScope = explode("_", $token["scope"])[0];
        $userId = $token["user_id"];

        switch ($roleScope) {
            case 'master':
                return "master";
            // case 'supplier':
            //     $query = "SELECT supplier_id FROM order_product WHERE order_id = $id AND supplier_id = $userId";
            //     $result = $this->db->FetchOne($query);
            //     if (isset($result)){
            //         return "supplier";
            //     }
            //     break;
            case 'customerStore':
            $query = "SELECT account_id FROM orders WHERE account_id = $userId";
            $result = $this->db->FetchOne($query);
            
            if ($result["account_id"] == $userId){

                return "customerStore";
            }
            
        }

        # fail for accessible check 
        // http_response_code(403);
        // JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }
}
