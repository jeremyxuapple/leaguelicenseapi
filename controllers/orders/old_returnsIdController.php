<?php
/** 
 * Main Returns Controller
*/

namespace LL\controllers\returns;

use LL\lib\database;

class returnsIdController
{
	public function __construct($method = 'get', $id = null, $extra = null, $token = false)
	{  
		# define service instance
     	$this->db = new \LL\lib\database\mysql();
        $this->accountService = new \LL\services\accountService();
        $this->productService = new \LL\services\productService();
        $this->returnService  = new \LL\services\returnService();
        $this->notificationService = new \LL\services\notificationService();

		# sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessibility = $this->checkAccessible($token);

        if ($this->accessibility){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
	}

	public function get($id = null, $extra = null, $token = null)
	{   
        extract($token);
		# define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array();

        # get returns information
        if (isset($id)){
            switch ($extra) {
                // case 'account':
                //     $returns = $this->returnService->getReturnsById($id, $user_id);
                //     JSO($returns);
                //     http_response_code(200);
                //     exit;  
                //     break;
                default:
                    $singleReturn = $this->returnService->getReturnById($id);
                    JSO($singleReturn);
                    http_response_code(200);
                    exit; 
                    break;
                }              	      	
        }else{

            $returns = $this->returnService->getReturns($options);
            $data["returns"] = $returns;
            if (sizeof($data["returns"]) > 0){
                # define basic data array for feedback
                $data["page"]          = $page + 1;
                $data["limit"]         = $limit;
                $data["count"]         = $max["count"];
                $data["maxpages"]      = ceil($max["count"] / $limit);
                $data["order_option"]  = $order_option;
                $data["order_sort"]    = $order_sort;

                http_response_code(200);
                JSO($data);
                exit;
            }  
        }     
	}

	public function post($id = null, $extra = null, $token = null)
	{    
        
        extract($token);
        $returnInstance = $this->returnService->generateProductReturnInstance();
        
        #query the supplier_id based on the order_id passed in and $user_id with the product_sku.
        $returnInstance["order_id"] = $id;
        $returnInstance["account_id"] = $user_id;
        $sku = $returnInstance["sku"];

        $supplier_id = $this->returnService->getSupplierId($id, $sku, $user_id);

        $returnInstance["supplier_id"] = $supplier_id;

        $where = array(
            "account_id" => $user_id,
            "order_id" => $id,
            "supplier_id" => $supplier_id
            );

        $id = $this->db->Insert("order_product_return", $returnInstance, $where);
       
        ## After the return has been created, we need to create a new notification for the master store 

        $notification = $this->returnService->create_return_notification($user_id);
        $this->notificationService->createNotification($notification);
        
        http_response_code(200);
        JSO();
        exit;
	}

	public function put($id = null, $extra = null, $token = null)
	{    
        parse_str(file_get_contents('php://input'), $_PUT);

        extract($token);
        $return_update = $_PUT;
        $return_update["date_modified"] = date("Y:m:d H:i:s"); 
        
        if($extra == null) {
            foreach($return_update as $key => $value) {
                $return_update[$key] = rawurldecode($value);
            }
            $this->db->Update("order_product_return", $return_update, array("id" => $id));
        } else {
            switch ($extra) {
                case 'approve':
                    $return_update["status"] = "approved";
                    $this->db->Update("order_product_return", $return_update, array("id" => $id));
                    break;
                case 'refuse':
                    $return_update["status"] = "declined";  
                    $this->db->Update("order_product_return", $return_update, array("id" => $id));
                    break;

                default:
                    # code...
                    break;
            }
        }

        $notification = $this->returnService->create_return_notification($user_id);     
        
        switch ($return_update["status"]) {
    ## if the the status has been changed to the approved, a new notification needs to be created and send out to the supplier and customer store.
            case 'approved':
                $notification["details"] = "The return request has been approved!";
                $notification["status"] = "closed";
                break;
    ## if the status has been changed to declined, a new notification needs to be created and send out to the customer store.
            case 'declined':
                $notification["details"] = "The return request has been declined!";
                $notification["status"] = "closed";
                break;
            default:
                # code...
                break;
        }

        $this->notificationService->createNotification($notification);

        JSO("Update successfully!");
        http_response_code(200);
        exit;
	}  

	// public function delete($id)
	// {

	// }

	/**
     * Accessibility check for masterstore, supplier, 
     *
     * @param $id - int - auto increament id in reutrns table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($token)
    { 
    	# identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        $userId = $token["user_id"];
        // die($roleScope);
        switch ($roleScope) {
        	case "master":
        		return "master";
        	case "customerStore":
        		$query = "SELECT account_id FROM order_product_return WHERE account_id = $userId";
        		$result = $this->db->FetchOne($query);
        		if (isset($result["account_id"])){
        			return "customerStore";
        		}
        		break;
        	case "supplier":
        		$query = "SELECT supplier_id FROM order_product_return WHERE supplier_id = $userId";
        		$result = $this->db->FetchOne($query);
        		if (isset($result["supplier_id"])){
        			return "supplier";
        		}
        		break;
        }

        # fail for accessible
        http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

}
