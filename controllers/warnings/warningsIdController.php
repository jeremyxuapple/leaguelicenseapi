<?php
/**
 * Main Warning Controller
 */
namespace LL\controllers\warnings;

use LL\lib\database;
use LL\services;

class warningsIdController
{
    /**
     * Construct function that handles product requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific product ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra=null, $token = null)
    {   
        $this->db = new \LL\lib\database\mysql();
        $this->service = new \LL\services\warningService();

        if($method == "put") {
            parse_str(file_get_contents('php://input'), $_POST);
        }
        if (!method_exists($this, $method))
            throw new \Exception("Method doesn't exist: $method");

        # accessibility check
        $this->accessiblity = $this->checkAccessible($id, $token);

        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new \Exception("Unable To Identify, Access Forbidden");
        }    
    }

    public function get($id, $extra, $token) 
    {
        if($id == null) {
            $this->service->get_warnings();        
        } else {
            $this->service->get_warning_by_id($id);
        }
    }

    public function post($id, $extra, $token) 
    { 

### take the data passed in and create a new warning

        // $product_name = $_POST["product_name"];
        // $stock_val = $_POST["stock_val"];
        // $type = $_POST["type"];
        // $status = $_POST["status"];
        // $description = $_POST["description"];
        // $date_modified = date("Y-m-d H:i:s");

        // $warning = array(
        //     "product_name" => $product_name,
        //     "stock_value" => $stock_val,
        //     "status" => $status,
        //     "description" => $description,
        //     "date_modified" => $date_modified,
        //     );

        // $warning["date_created"] = date("Y-m-d H:i:s");

        // $wheres = array("product_name" => $product_name);

        // $this->db->CheckInsert("warnings", $warning, $wheres);
        
        // $this->db->Update("warnings", $warning, $wheres);
    }

    public function put($id, $extra, $token) {
        // $this->post($id, $extra, $token);
    }

    public function delete($id, $extra)
    {   
        $date_modified = date("Y-m-d H:i:s");
    
        if ($this->accessiblity == "master"){
            
            if($extra == "active") {

                $status = "Active";
                $warning = array(
                    "status" => $status,
                    "date_modified" => $date_modified
                    );
                ## reactive the warning status
                $result = $this->db->Update("warnings", $warning, array("id" => $id));
                  if ($result){
            # updated successed
                    http_response_code(200);
                    JSO(array("response" => "Warning With $id Has Been Activated!"));
                    exit;
                    }
            } 

            if(!isset($extra)) {
                $status = "Suspended";
                $warning = array(
                    "status" => $status,
                    "date_modified" => $date_modified
                    );
                $result = $this->db->Update("warnings", $warning, array("id" => $id));
                if ($result){
                # updated successed
                http_response_code(200);
                JSO(array("response" => "Warning With $id Has Been Suspended"));
                exit;
                }
            }
           
        }else{
            throw new \Exception("Unable To Identify, Access Forbidden");
        }

        http_response_code(404);
        JSO(array("Error" => "Warning Already Suspended Or Not Exist"));
        exit;
    }


      private function checkAccessible($warningId = null, $token)
    {  
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        switch($roleScope) {
            case "master":
                return "master";
                break;
        }

        // http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }
}
