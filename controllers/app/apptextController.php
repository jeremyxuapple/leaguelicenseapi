<?php
/**
 * Main App Controller
 */

namespace LL\controllers\app;

use Firebase\JWT\JWT;
use Oauth2;
use Oauth2\Storage;
use Oauth2\GrantType;
use LL\lib\database;
use LL\lib\BCAPI;
use LL\services;

class apptextController
{
    /**
     * Construct function that handles product requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific product ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $action = null, $extra = null, $token = false)
    {   

        $this->db = new \LL\lib\database\mysql();
        // $this->jwt = new \Firebase\JWT\JWT;
        $this->token = $token;
        $this->action = $action;
        $this->extra = $extra;
        // $this->clientapi = null;

        $this->accountService = new \LL\services\accountService;
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();
        
        if (!method_exists($this, $method))
            throw new \Exception("Method does not exist: $method");

        $this->$method();
    }

    private function setToken()
    {
        $server = new \LL\lib\Oauth\OauthServer();

        $this->token = $server->verifyToken();
    }

    private function get()
    {
        # there are only a few actions possible
        $action = $this->action;

        if (!method_exists($this, $action))
            throw new \Exception("Unable to complete app request: $action");

        $this->$action();
    }

    private function post()
    {
        # there are only a few actions possible
        $action = $this->action;

        if (!method_exists($this, $action))
            throw new \Exception("Unable to complete app request");

        $this->$action();
    }

    private function gettest()
    {
        JSO(array(
            "test" => "Test",
        ));
    }

    private function initSetup()
    {
        # check the token to see if this is in the installation process or not
        $this->setToken();

        $s = "SELECT * FROM settings WHERE setting = 'agreement' ";
        $agreement = $this->db->FetchOne($s);

        $scope = explode("_", $this->token['scope']);

        if (in_array("installation", $scope)) {
            JSO(array(
                "agreement" => $agreement['value'],
            ));
        } else {
            JSO(false);
        }
    }

    private function install()
    {           
        # get the store hash
        $store = str_replace('stores/', '', $_GET['context']);
        $createaccount = false;

        # let's delete all data from other accounts

        if (!isset($_GET['keep'])) {
            $delete = "DELETE FROM accounts WHERE code = :store;
            DELETE FROM oauth_access_tokens WHERE client_id = :store;
            DELETE FROM oauth_clients WHERE client_id = :store;
            DELETE FROM subscriptions WHERE store_hash = :store";

            $vars = array("store" => $store);
            $this->db->Query($delete, $vars);
        }

        # for the installation we have to post to their oauth with the client details
        $s = "SELECT id, client_id, client_secret, redirect_uri, account_scope FROM registered_app_details WHERE store = :store ";
        $vars = array("store" => $store);
        $data = $this->db->FetchOne($s, $vars);


        if (empty($data)) {
            # this means that this is a customer store - in this case we have to create the account and also create the customer token for the BC API
            # let's create the auth token with the main store client_id and secret
            $s = "SELECT id, client_id, client_secret, redirect_uri, account_scope FROM registered_app_details WHERE store = :store ";
            $vars = array("store" => 'pbbto38xen');
            $data = $this->db->FetchOne($s, $vars);

            // for testing purposes
            $data = array(
                 "client_id" => $data['client_id'],
                // "client_id" => "ochficprqsyl6dwicu5h9heeufx0zrk",
                "client_secret" => $data['client_secret'],
                // "client_secret" => "ovug17ftkfloj0up30qirazej2meo5f",
                
                "redirect_uri" => "https://automatedarmy.com/api/v1/app/install/bc",
                "account_scope" => "",
            );

            $createaccount = true;

            # let's check if the account exists because if it doesn't we have to make them pay first
            $s = "SELECT * FROM accounts WHERE code = :store ";
            $vars = array("store" => $store);
            $account = $this->db->FetchOne($s, $vars);

            if (empty($account)) {
                # show payment page
                $this->showPaymentPage($store);
                die();
            }
        }

        # add in the code we receive from initiation
        $data['code'] = $_GET['code'];
        $data['scope'] = 'store_v2_products';//$_GET['scope'];
        $data['grant_type'] = "authorization_code";
        $data['context'] = $_GET['context'];

        # now we post this to the store
        $response = Curl("https://login.bigcommerce.com/oauth2/token", $data);
        $response = json_decode($response, true);

        if (isset($response['error'])) {
            throw new \Exception($response['error']);
            RD($data);
            RD($response);
            exit();
        }

        # Michael - Sep 9 2017. Change the logic for prevent dulplicate account id
        $resAccountId = $response['user']['id'];
        $isDulplicate = true;

        while ($isDulplicate) {
            $accountIdDuplicateQuery = "
                SELECT id FROM accounts WHERE account_id = $resAccountId
            ";
            $accountDulplicateCheck = $this->db->FetchOne($accountIdDuplicateQuery);

            if (isset($accountDulplicateCheck['id'])){
                $resAccountId += 1;
            }else{
                $isDulplicate = false;
                break;
            }
        }


        # after we update the user information let's create an oauth token for this client
        $i = array(
            "client_id" => $store,
            "client_secret" => generateRandomString(26),
            "redirect_uri" => $data['redirect_uri'],
            "grant_types" => "client_credentials",
            "scope" => $data['account_scope'],
            "user_id" => $resAccountId
        );

        $check = array(
            "client_id" => $store,
            "user_id" => $response['user']['id']
        );

        # this inserts the oauth client crednetials into the database
        $id = $this->db->CheckInsert("oauth_clients", $i, $check);

        # let's get the full row to return
        $s = "SELECT * FROM oauth_clients WHERE client_id = :client_id AND user_id = :user_id";
        $client = $this->db->FetchOne($s, $check);

        # insert a token
        $token = array(
            "access_token" => generateRandomString(40),
            "client_id" => $store,
            "user_id" => $resAccountId,
            "expires" => date('Y-m-d h:i:s'),
            "scope" => "customerStore_installation",//$data['account_scope'],
        );

        $tokenid = $this->db->Insert('oauth_access_tokens', $token);

        # we now have to insert this response into the database
        $u = array(
            "access_token" => $response['access_token'],
            "scope" => str_replace(' ', ',', $_GET['scope']),
            "user_id" => $resAccountId,
            "username" => $response['user']['username'],
            "email" => $response['user']['email'],
            "context" => $response['context'],
            "token" => $token['access_token'],
        );

        # this registers the app into the database
        if ($createaccount) {
            $account = array(
                "account_id" => $u['user_id'],
                "token" => $u['access_token'],
                "platform" => "BC",
                "code" => $store
            );

            $res = $this->db->Update("accounts", $account, array("code" => $store));

            //let's update subscription table
            $this->db->Update("subscriptions", array("account_id" => $u['user_id']), array("store_hash" => $store));

        } else {
            $tokenid = $this->db->Update("registered_app_details", $u, array("store" => $store));
        }

        # let's at least update the token to a date that's 30 days from now
        $update = array(
            "expires" => date('Y-m-d h:i:s', mktime(date('h'),date('i'),date('s'),date('m'),date('d')+30,date('Y')))
        );

        $this->db->Update("oauth_access_tokens", $update, array("user_id" => $u['user_id']));

        # the last thing we have to do is create a webhook for updated orders for this account
        $s = "SELECT t.access_token as token, t.user_id, t.client_id, a.token as bc_token FROM accounts a 
        LEFT JOIN oauth_access_tokens t ON t.client_id = a.code
        WHERE a.code = :hash";
        $vars = array("hash" => $store);
        $token = $this->db->FetchOne($s, $vars);
     
        $webhook_data_order = array(
            'scope' => 'store/order/*',
            'destination' => 'https://automatedarmy.com/api/v1/webhooks?access_token='.$token['token'],  // redirect to Test server
            'is_active' => true
        );

         $webhook_data_product = array(
            'scope' => 'store/product/deleted',
            'destination' => 'https://automatedarmy.com/api/v1/webhooks?access_token='.$token['token'],  // redirect to Test server
            'is_active' => true
        );

        $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        $this->clientapi = $api->getClient();

        $store_info = $this->clientapi->getStore();

        $store_name = $store_info->name;

        $this->db->Update("accounts", array("name" => $store_name), array("code" => $store));

        $this->clientapi->createWebhook($webhook_data_order);
        $this->clientapi->createWebhook($webhook_data_product);
       
        $this->load(false);
        // $this->load(true);
    }

    private function load($checktoken = true)
    {   
            if (isset($_GET['signed_payload'])){

                $payload = explode('.', $_GET['signed_payload']);
                $payload = json_decode(base64_decode($payload[0]));

            // //we can get the store information from the referrer
            // if (isset($_GET['context'])) {
            //     $hash = str_replace('stores/', '', $_GET['context']);
            // } else {
            //     preg_match_all("/store-(.*).mybigcommerce.com/", $_SERVER['HTTP_REFERER'], $store);
            //     $hash = $store[1][0];
            // }
                $hash = $payload->store_hash;
            } // End if isset payload signed_payload...

            #let's define master store access store hash
            if (isset($_GET['store'])){
                $hash = $_GET['store'];
            }

            # now let's get the token from the store hash information
            $s = "SELECT * FROM registered_app_details WHERE store = :hash";
            $vars = array("hash" => $hash);
            $data = $this->db->FetchOne($s, $vars);

            if (empty($data)) {
                # this is a customer store
                $s = "SELECT t.access_token as token, a.account_id FROM accounts a 
                LEFT JOIN oauth_access_tokens t ON t.client_id = a.code
                WHERE a.code = :hash";
                $vars = array("hash" => $hash);
                $data = $this->db->FetchOne($s, $vars);
            }

            # set token
            $_GET['access_token'] = $data['token'];

            # this is only in because the database didn't quite update the token time yet
            if ($checktoken)
                $this->setToken();

            # for the admin app let's check the token and get the appropriate app
            if ($this->token['scope'] === 'master') {
                $index = file_get_contents(ADMIN_APP_PATH.'/index.html');
                $index = str_replace('%APP_PATH%', ADMIN_APP_URL, $index);
            } else {
                $index = file_get_contents(APP_PATH.'/index.html');
                $index = str_replace('%APP_PATH%', APP_URL, $index);
            }

            $index = str_replace('%APP_STORE_PATH%', STORE_PATH, $index);
            $index = str_replace('%APP_TOKEN%', $data['token'], $index);

            if ($checktoken){
                # let's pass the userid into the template
                $index = str_replace('%USER_ID%', $this->token['user_id'], $index);
            }else{
                # let's pass the userid into the template
                $index = str_replace('%USER_ID%', $data['account_id'], $index);
            }

            echo $index;
        // } // End of if (isset($_GET['signed_payload']))
    }// end of function load()

    private function productredirect()
    {
        # get customer information
        $this->setToken();

        ### update by michael -- Sep 4 -- might be temporary
        ### Get customer Id by token
        ### Get store hash (code) by token
        $accountId = $this->token['user_id'];
        $customerIdQuery = "SELECT bc_customer_id, code FROM accounts WHERE account_id = $accountId";
        $customer = $this->db->FetchOne($customerIdQuery);
        $cusomerStoreHash = $customer['code']; // Define customer store hash
            // RD($customerId);
            // die;
        ### End Get customer Id by accesss token

        # get the settings
        $s = "SELECT * FROM settings";
        $settings = $this->db->Fetch($s);

        foreach($settings as $setting) {
            $this->settings[$setting['setting']] = $setting['value'];
        }

        // RD($customerId);
        // die;
        $payload = array(
            'iss' => $this->settings['client_id'],
            'iat' => time(),
            'jti' => bin2hex(random_bytes(32)),
            'operation' => 'customer_login',
            'store_hash' => $this->settings['store_hash'],
            // 'customer_id' => 1,
            'customer_id' => $customer['bc_customer_id'],
            'redirect_to' => "/#$cusomerStoreHash", // Passing store hash to url. We have to grab it from Store front
        );

        $token = JWT::encode($payload, $this->settings['client_secret'], 'HS256');

        # now we redirect with the token
        header("location: ".$this->settings['master_store']."/login/token/{$token}");
        die();
    }

    private function showPaymentPage($clientid)
    {
        $query = http_build_query($_GET)."&store=".str_replace('stores/', '', $_GET['context']);
        //echo 'https://league-licence-master-store4.mybigcommerce.com/app/?'.$query;
        header('location: https://master.sellerlicense.com/app/?'.$query);
        die();
    }

    private function poll()
    {   
        # what we have to do here is check if a particular order has gone through
        $store = $_POST['store'];
        // die;
        # let's query the big commerce master store
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        //$date = date('D, d M Y H:i:s +0000', time() - (3600 * 30));
        $date = date('Y-m-d', time() - (3600 * 24 * 20));

        $orders_awaiting = $this->api->getCollection("/orders?min_date_created=$date&status_id=11");
        $orders_completed = $this->api->getCollection("/orders?min_date_created=$date&status_id=10");

        if (!isset($orders_awaiting)){
            $orders_awaiting = array();
        }

        if (!isset($orders_completed)){
            $orders_completed = array();
        }

        $orders = array_merge($orders_completed, $orders_awaiting);

        # process when there is orders 
        if (sizeof($orders) > 0){
            $createaccount = false;
            $productActive = false;
            foreach($orders as $order) {
                # now we have to get the products for this order
                $products = $this->api->getCollection("/orders/".$order->id."/products");
                foreach($products as $product) {

                    if (in_array($product->product_id, array(113,114,115))) {
                        foreach($product->product_options as $option) {
                            if ($option->value == $store) {
                                $productActive = $product;
                                $createaccount = $order->id;
                                break;
                            }
                        }
                    }
                    if ($createaccount)
                        break;
                }
                if ($createaccount)
                    break;
            } // End of Foreach

            if ($createaccount) {
                $ia = array(
                    "platform" => "Big Commerce",
                    "code" => $store,
                    "account_id" => rand(888888, 999999),
                    "bc_customer_id"    => $order->customer_id,
                    // "name"     => $store_name,
                    // let's add some order information to the account
                    "contact_first_name" => $order->billing_address->first_name,
                    "contact_last_name" => $order->billing_address->last_name,
                    "contact_email" => $order->billing_address->email,
                    "contact_phone" => $order->billing_address->phone,
                    "billing_first_name" => $order->billing_address->first_name,
                    "billing_last_name" => $order->billing_address->last_name,
                    "billing_company" => $order->billing_address->company,
                    "billing_phone" => $order->billing_address->first_name,
                    "billing_address_1" => $order->billing_address->street_1,
                    "billing_address_2" => $order->billing_address->street_2,
                    "billing_city" => $order->billing_address->city,
                    "billing_state" => $order->billing_address->state,
                    "billing_zip" => $order->billing_address->zip,
                    "billing_country" => $order->billing_address->country,
                    "status" => "Active",
                    "active_subscription_level" => $productActive->name,
                    "date_created" => date('Y-m-d H:i:s'),
                    "date_modified" => date('Y-m-d H:i:s'),
                );

                $res = $this->db->CheckInsert("accounts", $ia, array("code" => $store));
                $this->accountService->create_subscription_payment_record($order->id, $store, true); 
                JSO(array("order" => $createaccount));
                die();
            } else {
                JSO(false);
                die();
            }
        } // End of if orders
    } // End of function poll(...)

    private function getstoreinfo()
    {
        $hash = $_REQUEST['store'];

        $s = "SELECT t.access_token as token, t.user_id as account FROM accounts a 
        LEFT JOIN oauth_access_tokens t ON t.client_id = a.code
        WHERE a.code = :hash";
        $vars = array("hash" => $hash);
        $data = $this->db->FetchOne($s, $vars);

        JSO($data);
    }

    private function savesetup()
    {
        # set some token information
        $this->setToken();

        # let's take off the installation scope on the token
        $s = "SELECT * FROM oauth_access_tokens WHERE access_token = :token";
        $vars = array("token" => $_GET['access_token']);
        $access = $this->db->FetchOne($s, $vars);

        $access['scope'] = str_replace("_installation", "", $access['scope']);

        # now let's update
        $u = $this->db->Update("oauth_access_tokens", $access, array("access_token" => $_GET['access_token']));

        # all we have to do here is save the account information into the account
        $account = array(
            "tax_id" => $_POST['taxid']['taxid'],
            "currency" => $_POST['setupOptions']['currency'],
            "product_markup" => $_POST['setupOptions']['map_percentage'],
        );

        $u = $this->db->Update("accounts", $account, array("account_id" => $this->token['user_id']));

        JSO(array("success" => true));
    }

    private function testFunction()
    {
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        $orders = $this->api->getCollection("/orders?min_id=119");

        RD($orders);

        foreach($orders as $order) {
            $products = $this->api->getCollection("/orders/".$order->id."/products");

            RD($products);
            exit();
        }
    }

    public function uninstall()
    {
        echo 'working';
    }
}
