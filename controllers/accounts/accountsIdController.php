<?php
/**
 * Main Accounts Controller
 * @param $id - account_id in accounts / token["user_id"] with scope customerStore
 */

namespace LL\controllers\accounts;

class accountsIdController
{
    /**
     * Construct function that handles account requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific account ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = false)
    {

        # define service instances 
        $this->db = new \LL\lib\database\mysql();
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();


        $this->orderService        = new \LL\services\orderService();
        $this->invoiceService      = new \LL\services\invoiceService();
        $this->returnService       = new \LL\services\returnService();
        $this->accountService      = new \LL\services\accountService();
        $this->productService      = new \LL\services\productService();
        $this->notificationService = new \LL\services\notificationService();

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessiblity = $this->checkAccessible($id, $token);

        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
    }

    private function get($id = null, $extra = null, $token = false)
    {   
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array();

        # get information associate to this account, accessible by master and customer store itself
        if (isset($id)){
            switch ($extra) {
                case 'orders':
                    $orders = $this->orderService->getOrdersByAccountId($id, $options);
                    if ($orders){
                        $data["orders"] = $orders;
                        $count = "SELECT count(*) AS count FROM orders WHERE account_id = $id";
                        $max = $this->db->FetchOne($count);
                    }
                    break;
                case 'invoices':
                    $invoices = $this->invoiceService->getInvoicesByAccountId($id, $options);
                    if ($invoices){
                        $data["invoices"] = $invoices;
                        $count = "SELECT count(*) AS count FROM account_invoice WHERE account_id = $id";
                        $max = $this->db->FetchOne($count);
                        }
                    break;
                case "returns":
                    $returns = $this->returnService->getReturnsByAccountId($id, $options);
                    if ($returns){
                        $data["returns"] = $returns;
                        $count = "SELECT count(*) AS count FROM order_product_return WHERE account_id = $id";
                        $max = $this->db->FetchOne($count);
                    }
                    break;
                case "notifications":
                    $notifications = $this->notificationService->getNotifications($id, $options);
                    if ($notifications){
                        $data["notifications"] = $notifications;
                        $count = "SELECT count(*) AS count FROM notifications WHERE from_user_id = $id OR to_user_id = $id";
                        $max = $this->db->FetchOne($count);
                    } 
                    break;
                case "products":
                    $products = $this->productService->getProductsByAccountId($id, $options);
                    if ($products){
                        $data["products"] = $products;
                        $count = "SELECT count(*) AS count FROM account_product WHERE account_id = $id";
                        $max = $this->db->FetchOne($count);
                    }
                case "subscription":
                    $subscription = $this->accountService->getLatestSubscirptionPayment($id);
                    if($subscription) {
                        $data["subscription"] = $subscription;
                    }
                    break;
                case null:
                    # get store information by account Id
                    $account = $this->accountService->getAccountByAccountId($id);
                    if ($account){
                        $data["account"] = $account;
                        // http_response_code(200);
                        JSO($data);
                        exit;
                    }
                    break;
                default:
                    // http_response_code(400);
                    // JSO(array("Error" => "400 Bad Request"));
                    throw new \Exception("400 Bad Request");
                    break;
            }
        }
        # get accounts list, accessible by master store only.
        else {
            if ($this->accessiblity == "master"){
                $accounts = $this->accountService->getAccounts($options);
                if ($accounts){
                    $data["accounts"] = $accounts;
                    $count = "SELECT count(*) AS count FROM accounts";
                    $max = $this->db->FetchOne($count);
                }
            }else{
                // http_response_code(403);
                // JSO(array("Error" => "403 Access Forbidden"));
                throw new \Exception("403 Access Forbidden");
                
                exit;
            }
        }

        # define basic data array for feedback
        $data["page"]          = $page + 1;
        $data["limit"]         = $limit;

        if (isset($max["count"])){
            $data["count"]         = $max["count"];
            $data["maxpages"]      = ceil($max["count"] / $limit);
        }

        $data["order_option"]  = $order_option;
        $data["order_sort"]    = $order_sort;

        JSO($data);
        exit;        
    }

    /** 
     * Open Endpoint for future development
    */
    private function post($id = null, $extra = null, $token = null)
    {   
        $action = false;
        switch ($extra) {
            # import products to an account from master store
            case 'products':
                # initial account id
                $accountId = $id;

                # product id in api DB table products (master store product id)
                if (isset($_POST["productId"])){
                    $productId = $_POST["productId"]; 
                }

                if (isset($_POST['category_id']) || isset($_POST['all_products']) || isset($_POST['brand_id']))  {
                    # in this case we'll need to create a queue

                    # Only for Demo. We will enable queue in the future [open issue]
                    if (isset($_POST['all_products'])){

                        // Disable for now. This is super complicated, maybe enable later...
                        throw new \Exception("Method Disable. You Can Not Import ALL Products For Now");
                    }


                    # Only for Demo. We will enable Queue in the future [open issue]
                    if ($_POST['category_id']){
                        $categoryId = $_POST['category_id'];

                        //Section for build up categories tree
                                #init get customer store api information
                                $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
                                $this->clientapi = $apiConnector->getClient();

                                ########### Init The Categories Tree ###############

                                # create League Licence Parent Categories
                                $leagueLicenceParentCat = array(
                                    "name" => "League License Products",
                                    "parent_id" => 0
                                );

                                // call api to create category
                                $leagueLicenceParentCategory = $this->clientapi->createCategory($leagueLicenceParentCat);

                                // if already exist
                                if (!$leagueLicenceParentCategory){
                                    // get category id from db
                                    $llcatIdQuery = "
                                        SELECT customer_category_id 
                                        FROM product_categories 
                                        WHERE name = 'League License Products' AND user_id = $accountId AND customer_parent_id = 0
                                        ORDER BY customer_category_id DESC
                                    ";
                                    $llcatIdResult = $this->db->FetchOne($llcatIdQuery);
                                    $categoryParentId = $llcatIdResult["customer_category_id"]; // init the category parent Id
                                }else{
                                    // LL category Got created
                                    // save to Our DB
                                    $categoryInstance = array(
                                        "user_id" => $accountId,
                                        "name"    => "League License Products",
                                        "customer_category_id" => $leagueLicenceParentCategory->id,
                                        "customer_parent_id" => 0
                                    );
                                    $this->db->CheckInsert("product_categories", $categoryInstance, $categoryInstance, true);
                                    $categoryParentId = $leagueLicenceParentCategory->id; // init the category parent Id
                                }

                                ########### Build Categories Tree ################

                                // find master store category parent list
                                $categoryQuery = "SELECT * FROM product_categories WHERE master_category_id = $categoryId AND user_id = 1";
                                $categoryResult = $this->db->FetchOne($categoryQuery);

                                $categoryResult["parent_category_list"];
                                $categoriesList = explode(",", $categoryResult["parent_category_list"]);

                                foreach ($categoriesList as $masterCategoryId) {

                                    $masterCategoryQuery = "
                                        SELECT name
                                        FROM product_categories
                                        WHERE user_id = 1 AND master_category_id = $masterCategoryId
                                    ";
                                    $masterCategory = $this->db->FetchOne($masterCategoryQuery);

                                    $masterCategory["parent_id"] = $categoryParentId;
                                    $masterCategoryName = $masterCategory["name"]; // define name

                                    $newCategory = $this->clientapi->createCategory($masterCategory);

                                    if (!($newCategory)){
                                        // Exist then we grab it from DB ...
                                        $customerCategoryQuery = "
                                            SELECT customer_category_id 
                                            FROM product_categories
                                            WHERE user_id = $accountId AND name = '$masterCategoryName' AND customer_parent_id = $categoryParentId
                                            ORDER BY customer_category_id DESC
                                        ";

                                        $customerCategoryResult = $this->db->FetchOne($customerCategoryQuery);
                                        $categoryParentId = $customerCategoryResult["customer_category_id"];
                                    }else{
                                        // Save to Our DB 
                                        $categoryInstance = array(
                                            "user_id" => $accountId,
                                            "name"    => $newCategory->name,
                                            "customer_category_id" => $newCategory->id,
                                            "customer_parent_id" => $categoryParentId
                                        );

                                        $this->db->CheckInsert("product_categories", $categoryInstance, $categoryInstance, true);
                                        $categoryParentId = $newCategory->id;
                                    } 
                                } // End of foreach 

                                // we get the customer store categories id and assign to the product import
                                $customerCategoryId = $categoryParentId;
                        //End Categories Tree Section 

                        $productsIdQuery = "SELECT product_id FROM products WHERE FIND_IN_SET($categoryId, categories)";
                        $productsId = $this->db->Fetch($productsIdQuery);

                        $productsAmount = sizeof($productsId);

                        # If too many products in one category (more than one page, which is 12.) we save them into products_import_queue table
                        if ($productsAmount > 12){
                            $productsIdPreImportedArray = array(); // init array to store products id

                            foreach ($productsId as $productId){
                                array_push($productsIdPreImportedArray, $productId["product_id"]);
                            }

                            # make customer import product instance
                            $customerImportProductsInfo = array(
                                "master_product_ids"    => implode(",", $productsIdPreImportedArray),
                                "account_id"           => $accountId,
                                "customer_category_id" => $customerCategoryId,
                            );

                            $this->db->CheckInsert("products_import_queue", $customerImportProductsInfo, $customerImportProductsInfo);
                            JSO(array("success" => true, "message" => "Importing this category will complete in the background. You can continue adding other categories or products while this process completes."));

                        }else{
                            # imported product only has less than 12.
                            if (sizeof($productsId) > 0){
                                foreach ($productsId as $productId){
                                    $this->productService->importProductToAccount($productId['product_id'], $token, true, $customerCategoryId, true);
                                }
                                JSO(array("success" => true, "message" => "$productsAmount products have been importing to your store"));

                            }else{
                                throw new \Exception("Products Category Not Match. Please Check If You Are Under Parents category");
                            }
                        } // end of else for product amount < 12
                    }

                }else{

                    // Import Product Only By Select. In this cases, we don't assign category tree to this product. But only assign League License Products to it
                    #init get customer store api information
                    $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
                    $this->clientapi = $apiConnector->getClient();

                    ########### Init The Categories Tree ###############

                    # create League Licence Parent Categories
                    $leagueLicenceParentCat = array(
                        "name" => "League License Products",
                        "parent_id" => 0
                    );

                    // call api to create category
                    $leagueLicenceParentCategory = $this->clientapi->createCategory($leagueLicenceParentCat);

                    // if already exist
                    if (!$leagueLicenceParentCategory){
                        // get category id from db
                        $llcatIdQuery = "
                            SELECT customer_category_id 
                            FROM product_categories 
                            WHERE name = 'League License Products' AND user_id = $accountId AND customer_parent_id = 0
                            ORDER BY customer_category_id DESC
                        ";
                        $llcatIdResult = $this->db->FetchOne($llcatIdQuery);
                        $categoryParentId = $llcatIdResult["customer_category_id"]; // init the category parent Id
                    }else{
                        // LL category Got created
                        // save to Our DB
                        $categoryInstance = array(
                            "user_id" => $accountId,
                            "name"    => "League License Products",
                            "customer_category_id" => $leagueLicenceParentCategory->id,
                            "customer_parent_id" => 0
                        );
                        $this->db->CheckInsert("product_categories", $categoryInstance, $categoryInstance, true);
                        $categoryParentId = $leagueLicenceParentCategory->id; // init the category parent Id
                    }

                    // get League License Products category id
                    $customerCategoryId = $categoryParentId;

                    # For import product individually, we handle the error separately
                    # duplicate product check
                    $productQuery = "SELECT id FROM account_product WHERE account_id = $accountId AND master_product_id = $productId";
                    $productResult = $this->db->FetchOne($productQuery);

                    if (!isset($productResult['id'])){
                        // import Product
                        $this->productService->importProductToAccount($productId, $token, false, $customerCategoryId);
                    }else{
                        JSO(array("error_msg" => "The Product Is Already In Your Store"));
                    }

                }
                break;

            default:
                # code...
                break;
        }

        exit();
    }

    /**
     * Update Account
     *
     * @param id - int - account_id(token["user_id"])
     * @return response
     */
    private function put($id, $extra = null, $token = false)
    {   
        $date_modified = date("Y-m-d H:i:s");

        # we allow to change the contact information filed for now. But not the subscription and statistic info
        parse_str(file_get_contents('php://input'), $_POST);        

        switch ($this->accessiblity) {
            case 'master':
                $account = array(
                    "staff_notes" => rawurldecode($_POST["staff_notes"]),
                    "status" => $_POST["status"],
                    "date_modified" => $date_modified
                    );
                
                 $result = $this->db->Update("accounts", $account, array("account_id" => $id));

                if ($result){
                        # updated successed
                        // http_response_code(202);
                        JSO(array("Success" => "Account has been updated!"));
                        exit;
                    }

                if($extra == "active") {
                    $status = "Active";
                    $account = array(
                        "status" => $status, 
                        "date_modified" => $date_modified
                    );
                    ## reactive the account status
                    $result = $this->db->Update("accounts", array("status" => "Active"), array("account_id" => $id));

                    if ($result){
                        # updated successed
                        // http_response_code(202);
                        JSO(array("Success" => "Account With $id Has Been Activated!"));
                        exit;
                    }
                } 
                break;
            case 'customerStore':
                # init client api
                $api = new \LL\lib\BCAPI\customerStoreConnector($token);
                $this->clientapi = $api->getClient();

                # get old Map Percentage from account
                $getOldMapPercentageQuery = "SELECT product_markup FROM accounts WHERE account_id = $id";
                $getOldMapPercentageResult = $this->db->FetchOne($getOldMapPercentageQuery);

                # generate account array and preparing for data update 
                $account = $this->accountService->generateAccountInstance();
                $result = $this->db->Update("accounts", $account, array("account_id" => $id), true); // check update or not
                if ($result){
                    # updated successed check product percentage
                    if (isset($account["product_markup"]) && $account["product_markup"] != $getOldMapPercentageResult["product_markup"]){

                        # option would be  ==> "update all products now" / "save for future product import"
                        if (isset($_POST["product_markup_update_options"]) && rawurldecode($_POST["product_markup_update_options"]) == "Update All The Products Now"){

                            # update all customer products and skus price
                            $productIdsQuery = "SELECT master_product_id, customer_product_id FROM account_product WHERE account_id = $id";
                            $productIdsResult = $this->db->Fetch($productIdsQuery);

                            # update for all products
                            foreach ($productIdsResult as $productIds) {
                                $masterProductId = $productIds["master_product_id"];
                                $customerProductId = $productIds["customer_product_id"];
                                #get product price 
                                $productPriceQuery = "SELECT price FROM products WHERE product_id = $masterProductId";
                                $productPriceResult = $this->db->FetchOne($productPriceQuery);
                                $productPrice = $productPriceResult["price"];

                                # calculate the selling price for customer store
                                # Wholesale price - (WSP %) * markup
                                # get account subscription from account
                                $accountSubscriptionQuery = "SELECT active_subscription_level FROM accounts WHERE account_id = $id";
                                $accountSubscription = $this->db->FetchOne($accountSubscriptionQuery);
                                switch ($accountSubscription["active_subscription_level"]) {
                                    case "Playoff - Package":
                                        $discount = 5;
                                        break;
                                    case "Championship - Package":
                                        $discount = 10;
                                        break;
                                    default: 
                                        $discount = 0;
                                        break;
                                }

                                # it will update the skus price as well
                                // $productPrice = round(($productPrice * (1 - $discount / 100))* (1 + $account['product_markup'] / 100), 2);

                                # new update. For now we will just keep it simple, no discount. no markup.
                                $productPrice = round(($productPrice * ($account['product_markup'] / 100)), 2);

                                $this->clientapi->updateProduct($customerProductId, array("price" => $productPrice));

                                # update Our DB customer product price
                                $this->db->Update("account_product", array("customer_product_price" => $productPrice), array("master_product_id" => $masterProductId, "customer_product_id" => $customerProductId));
                            }
                        }
                    }
                }
            
                http_response_code(202);
                JSO(array("Success" => "Account With $id Has Been Updated!"));
                exit;
                break;

        }

        throw new \Exception("Unable To Identify, Access Forbidden"); 
        http_response_code(404);
        JSO(array("Error" => "Account Already Suspended Or Not Exist"));
        exit;
    }


   
    /**
     * Suspend This Customer Store, Not Delete From DB
     * 
     * @param id - int - accountId
     * @return response
    */
    private function delete($id, $extra)
    {
        $date_modified = date("Y-m-d H:i:s");
        $status = "Suspended";

        $account = array(
            "status" => $status,
            "date_modified" => $date_modified
            );

    if ($this->accessiblity == "master"){
            $result = $this->db->Update("accounts", $account, array("account_id" => $id));
             # updated successed
            http_response_code(200);
            JSO(array("Success" => "Account With $id Has Been Suspended!"));
            exit;
        } 
        else { 
            throw new Exception("Unable To Identify, Access Forbidden");
            http_response_code(404);
            JSO(array("Error" => "Account Already Suspended Or Not Exist"));
            exit;
        }
    }


    /**
     * Accessibility Check For Masterstore, Supplier, 
     *
     * @param $id - int - auto increament id in reutrns table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($accountId = null, $token)
    {   
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        switch($roleScope) {
            case "master":
                return "master";
            case "customerStore":
                if ($accountId){
                    if ($token["user_id"] == $accountId){
                        return "customerStore";
                    }
                }else{
                    return "customerStore";
                }
                break;
        }

        // http_response_code(403);
        // JSO(array("Error" => "403 Access Forbidden"));
        throw new Exception("Unable To Identify, Access Forbidden");
    }
}