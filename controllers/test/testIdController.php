<?php
/**
 * Testing Controller
 * @param $id - account_id in accounts / token["user_id"] with scope customerStore
 */

namespace LL\controllers\test;

use LL\services;

class testIdController
{
    /**
     * Construct function that handles account requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific account ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = false)
    {
        # define service instances 
        $this->db = new \LL\lib\database\mysql();
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->emailService = new \LL\services\emailService();

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("TESTING => Invalid method : $method");
        
        # accessibility check
        $this->$method($id, $extra, $token);
    }

    private function get($id, $extra, $token)
    {
         $template_content = array(
            array(
                'name' => "order_id",
                'content' => 123456
                ),
            array(
                'name' => "product_name",
                'content' => "Test Product"
                ),
            array(
                'name' => "quantity",
                'content' => 3
                ),
            array(
                'name' => "reason",
                'content' => "I'm returning this"
                ),
            array(
                'name' => "type",
                'content' => "type"
                ),
            array(
                'name' => "accountName",
                'content' => "Account name"
                ),
            array(
                'name' => "contact_first_name",
                'content' => "Robin"
                ),
             array(
                'name' => "contact_last_name",
                'content' => "QC Testing"
                ),
            array(
                'name' => "contact_email",
                'content' => "robin@beapartof.com"
                ),
            array(
                'name' => "contact_phone",
                'content' => "555-555-5555"
                ),
            array(
                'name' => "active_subscription_level",
                'content' => "Subscription Level"
                ),
        );

        $response = $this->emailService->sendMailTemplate("robin@beapartof.com", "robin@beapartof.com", "Test Subject", "Return(masterSupplier)",  $template_content);

        RD($response);

         echo file_get_contents(BASE_PATH."/templates/email/returns_template.html");
    }

    private function post($id, $extra, $token)
    {

    }
}