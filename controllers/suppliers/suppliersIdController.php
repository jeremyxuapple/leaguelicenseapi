<?php
/**
 * Main Suppliers Controller
 */

namespace LL\controllers\suppliers;

class suppliersIdController
{
    /**
     * Construct function that handles supplier requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific supplier ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = null)
    {    
        $this->db                  = new \LL\lib\database\mysql();
        // $this->validator        = new \LL\lib\helper\Validator();
        $this->orderService        = new \LL\services\orderService();
        $this->accountOrderService = new \LL\services\accountOrderService();
        $this->supplierService     = new \LL\services\supplierService();
        $this->invoiceService      = new \LL\services\invoiceService();

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        if($method == "put") {
            parse_str(file_get_contents('php://input'), $_POST);
        }

        # accessibility check
        $this->accessiblity = $this->checkAccessible($id, $extra, $token);
        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
    }

    private function get($id = null, $extra = null, $token = false)
    {
        
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options); 
        $data = array();

        if (isset($id)){
            switch ($extra) {
                case 'orders':
                    // $orders = $this->order->getOrdersBySupplierId($id, $options);
                    // $data["orders"] = $orders;
                    break;
                default:
                    $supplier = $this->supplierService->getSupplierById($id);
                    if (sizeof($supplier)){
                        $data["supplier"] = $supplier;
                        http_response_code(200);
                        JSO($data);
                        exit;
                    }
                    break;
            }
        }
        # get supplier list, accessible by master store only
        else {
            if ($this->accessiblity == "master"){
                $suppliers = $this->supplierService->getSuppliers($options);
                $data["suppliers"] = $suppliers;
                $count = "SELECT count(*) AS count FROM suppliers";
                $max = $this->db->FetchOne($count);
            }
        }

      # define basic data array for feedback
        $data["page"]          = $page + 1;
        $data["limit"]         = $limit;
        $data["count"]         = $max["count"];
        $data["maxpages"]      = ceil($max["count"] / $limit);
        $data["order_option"]  = $order_option;
        $data["order_sort"]    = $order_sort;

        JSO($data);
        exit;

        // if (sizeof($data["suppliers"]) > 0 || sizeof($data["orders"]) > 0){
        //     http_response_code(200);           
        // }

        // http_response_code(404);
        // JSO(array("Error" => "404 Not Found"));
        // exit;
    }

 // Better to receive payload
    private function post($id = null, $extra = null, $token = null)
{   
    if($extra == null) {      
        $supplier = $this->supplierService->generateSupplierInstance();
        $location = $_POST["locations"][0];
        foreach($location as $key => $value) {
            $location[$key] = rawurldecode($value);    
        }

        if ($this->accessiblity == "master"){
            $supplier = $this->supplierService->generateSupplierInstance();
            $supplier["date_created"] = date("Y-m-d H:i:s");

            # insert to supplier tables
            $supplierId = $this->db->CheckInsert("suppliers", $supplier, array("contact_phone" => $_POST["contact_phone"]));
                     
            $this->db->Update("suppliers", $supplier, array(
                'id' => $supplierId
                ));
            // # insert to supplier_location table
            $supplier_id = $location["supplier_id"];
           
            $location_id = $this->db->CheckInsert("supplier_location", $location, array(
                "contact_email" => $location["contact_first_name"],
                "contact_phone" => $location["contact_phone"],
                ));

            $this->db->Update("supplier_location", $location, array(
                'id' => $location_id
                ));

            http_response_code(200);
            JSO($_POST);
            exit;
        }else{
            // http_response_code(403);
            JSO(array("Error" => "403 Access Forbidden"));
            throw new \Exception("Unable To Identify, Access Forbidden");
        }
    }else {

        switch ($extra) {
            case 'invoice':
                $invoice = $this->invoiceService->create_invoice();

                extract($invoice);
                $where = array(
                    "order_id"    => $order_id,
                    "supplier_id" => $supplier_id,
                    "account_id"  => $account_id,
                    "product_id"  => $product_id,
                    );
        ## 1. Create the record into the API database

                $invoice_id = $this->db->CheckInsert("account_invoice", $invoice, $where);
                
        ## 2. Send out the email to notify the the child store        
                $this->invoiceService->send_invoice_email($account_id, $order_id, $product_id, $supplier_id);
        ## 3. Push an order store into the Big Commerce Master Store
        ## 4. Call MiniBC to charge the order        
                $this->accountOrderService->create_account_order($invoice, $account_id, $order_id, $product_id);
                // $this->accountOrderService->create_account_order(1, 4, 107, 111);
                break;
            default:
                # code...
                break;
        }
    }    
}

    private function put($id)
    {
        
    }

    /**
     * Suspend This Supplier, Not Delete From DB
     *
     * @param id - int
     * @return response
    */
    private function delete($id, $extra)
    {
        if ($this->accessiblity == "master"){

            if($extra == "active") {
                ## reactive the supplier status
                $result = $this->db->Update("suppliers", array("status" => "Active"), array("id" => $id));
                  if ($result){
            # updated successed
                    http_response_code(200);
                    JSO(array("response" => "Supplier With $id Has Been Activated!"));
                    exit;
                    }
            } 

            if(!isset($extra)) {

                $result = $this->db->Update("suppliers", array("status" => "Suspended"), array("id" => $id));
                if ($result){
                # updated successed
                http_response_code(200);
                JSO(array("response" => "Supplier With $id Has Been Suspended"));
                exit;
                }
            }
           
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }

        // http_response_code(404);
        throw new Exception("Supplier Already Suspended Or Not Exist");
        exit;
    }

    private function checkAccessible($supplierId = null, $extra, $token)
    {
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        switch($roleScope) {
            case "master":
                return "master";
            case "supplier":
                if ($token["user_id"] == $supplierId){
                    return "supplier";
                }
                break;
        }

        // http_response_code(403);
        // JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }
}