<?php
/**
 * Main Webhooks Controller
 */

namespace LL\controllers\webhooks;

use LL\lib\database;
use LL\services;

class webhooksController
{
    public function __construct($method = null, $token = null)
    {
        $this->db = new \LL\lib\database\mysql();
        $this->orderService        = new \LL\services\orderService();
        $this->productService      = new \LL\services\productService();
        $this->accountService      = new \LL\services\accountService();
        $this->notificationService = new \LL\services\notificationService();

        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        $this->$method($token);
    }

    /**
     * only for test purpose, create, update any webhooks.
    */
    private function get($token = null)
    {
            // $header = array(
            // "X-MBC-TOKEN: MTY5LmFwaV81OTZmZDAxMmJjNjQzNS43Mjc5MTI3Ni4xNTAwNDk5OTg2",
            // );
            // $searchURL = "https://apps.minibc.com/api/apps/recurring/v1/subscriptions/search";
            // $data = array(
            //     "order_id" => 320
            // );

            // $response = Curl($searchURL, $data, $header);

            // $data = json_decode($response);
            // RD($data);

            // if ($data){
            //     $profile_id = $data[0]->payment_method->id;
            //     $credit_card = $data[0]->payment_method->credit_card;
            // }
            
            // $last_digits = $credit_card->last_digits;
            // $expiry_month = $credit_card->expiry_month;
            // $expiry_year = $credit_card->expiry_year;

            // RD($last_digits);
            // RD($expiry_month);
            // RD($expiry_year);


            // die;
        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();

        // $order = $this->clientapi->getOrder(155);
        // RD($order);

        // die("done");

        // $query = "SELECT minibc_profile_id FROM accounts WHERE code = '5yju3wco4k11'";
        // $profile_id = $this->db->FetchOne($query);

        // // RD($profile_id['minibc_profile_id']);
        // if (!isset($profile_id["minibc_profile_id"])) {
        //     RD("Null");
        // }else{
        //     RD("Not Null");
        // }

        // die("here");

        // $lines = file(BASE_PATH."/controllers/webhooks/productsInventoryDiff.txt");
        // $productsIdArrayStr = "(";

        // foreach ($lines as $index => $line) {
        //     if ($index != 0 && $index != 546 ){
        //         $productId = explode(",", $line);
        //         if ($index == 545){
        //             $productsIdArrayStr .= $productId[0] . ")";
        //         }else{
        //             $productsIdArrayStr .= $productId[0] . ",";
        //         }
        //     }
        // }

        // $productsResults = $this->db->Fetch("
        //     SELECT p.product_id, p.sku, p.inventory_level, p.date_modified, ap.account_id
        //     FROM products p
        //     LEFT JOIN account_product ap
        //         ON p.product_id = ap.master_product_id AND ap.account_id = 303204
        //     WHERE p.sku IN $productsIdArrayStr
        // ");

        // foreach ($productsResults as $product) {
        //     print_r($product["product_id"] . "---" . $product['sku'] . "---" . $product["inventory_level"]. "---" .$product['date_modified'] . "---" . $product["account_id"] . "\n");
        // }

        // die("done reading");

        // $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
        // $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);

        // RD($inventorySubtractionResult["inventory_subtraction_value"]);
        // die;

        // if ( 0 < $inventorySubtractionResult["inventory_subtraction_value"]){
        //     RD("yes");
        // }else{
        //     RD("no");
        // }

        // $this->cronjobController->updateAccountProductInventoryLevel(6796, 0);
        // die("here");
        // $this->orderService->email_store_supplier(105, 805259, 10390);
        // die("weekhooks done");

        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();

        // RD($this->clientapi->getProduct(1400));
        // die;

        // $products = $this->api->getProducts(array('limit'=> 10, 'page'=> $productsPage, 'min_date_modified' => '2017-10-04 00:00:00'));
        // RD($products);
        // die;

        // $products = $this->api->getCollection("/orders/"."312"."/products");
        // foreach($products as $product) {
        //     if (in_array($product->product_id, array(113,114,115))) {
        //             foreach($product->product_options as $option) {
        //                 RD("****** product value");
        //                 RD($option->value);
        //             }
        //     }
        // }
        // die;

        // $orders = $this->api->getOrders(array('limit' => 250, 'page' => 1));


        // foreach ($orders as $order) {
        //     RD("================");
        //     RD($order->id);
        //     $products = $this->api->getCollection("/orders/".$order->id."/products");
        //     if (sizeof($products))
        //     foreach($products as $product) {
        //         if (in_array($product->product_id, array(113,114,115))) {
        //             foreach($product->product_options as $option) {
        //                 RD("****** product value");
        //                 RD($option->value);
        //             }
        //         }
        //     }
        // }

        // // RD($orders);
        // die("done");

        // RD($this->api->getProduct("12172")->primary_image);
        // $this->productService->syncProducts(1);
        
        // $this->db->Delete("product_categories", array("user_id" => null));
        // die("done");

        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();
        // RD("token");

        // RD($this->clientapi->getStore());
        // die;

        // RD($this->api->getProduct(9370)->inventory_level);
        // die;

        // $this->api->updateProduct(9672,array("inventory_level" => 88));
        // $this->api->updateProduct(10390,array("inventory_level" => 88));
        // $this->api->updateProduct(6571,array("inventory_level" => 88));
        // $this->api->updateProduct(6572,array("inventory_level" => 88));
        // $this->api->updateProduct(6573,array("inventory_level" => 88));
        // $this->api->updateProduct(6574,array("inventory_level" => 88));

        // RD($this->api->getCustomers());
        // die;
        // RD($this->api->getProductImages(6818));
        // RD($this->api->getProduct(6818)->images);
        // RD($this->api->getCollection("/products/6818/images"));
        // RD($this->api->getResource("/products/6818/images"));
        // die;
        // die;
        // RD($this->api->getProducts(array('limit'=>250, 'page'=> 26)));
        // die;
        // RD(isset($ca));

        // if(!isset($ca)){
            // RD("empty");
        // }else{
            // RD("has");
        // }

        // die;
        // $this->accountService->updateAccountProductInventoryLevel(9370, 42);
        // die;
        // RD($this->clientapi->getCategories());
        // die;
        // RD($this->api->getCategories());
        // die; 
        // $productQuery = "
        //     SELECT name, categories
        //     FROM products
        //     WHERE cat
        // ";
        // 9: 11,12,46,79
        // "SELECT * FROM table WHERE FIND_IN_SET(item, group) ";
        // $api = new \LL\lib\BCAPI\masterConnector();
        // $this->api = $api->getClient();

        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();

        // $this->productService->mapCategories(805257, $token);
        // die;
        // $this->orderService->syncOrderByIds(106, 805257, $token);
        // die;


        // $this->accountService->updateAccountProductInventoryLevel(103, 100);
        // die;
        // $this->productService->mapProductSkus(805257, $token, 77);
        // die;
        // $this->productService->syncProducts(1);
        // die;
        // RD($this->api->getProduct(93)->skus);
        // RD($this->api->getOptionSet(16)->options);
        // $this->mapOptionSets();
        // $this->productService->mapOptionSetOptions(805257, $token);
        // die;
        // $this->productService->mapProductSkus(805257, $token, 88);
        // die;
        // RD($this->api->getLastError());
        // die;

        // $this->api->updateProduct(77, array("is_visible" => true));
        // RD($this->api->getLastError());
        // die;
       
        // die;
        // $this->productService->syncProducts(1);
        // die;
        // $this->clientapi->updateOrder(103, array("status_id" => 9));
        // // RD($this->clientapi->getOrder(103));
        // RD($this->clientapi->getLastError());
        // die;
        // $this->notificationService->generateNotificationForNewProducts();

        // $userId = $token["user_id"];
        // $this->productService->mapProduct(805257, $token, 77);
        // die;
        // die;

       // $this->productService->importProductToAccount(77, $token);
       // die;
       // $product = $this->api->getProduct(77);
       // RD($product);
       // RD($this->api->getOptions());
       // die;

       // $skus = $product->options;
       // RD($skus);
       // die;

       // foreach ($skus as $sku ) {
       //      RD($sku);
       //     # code...
       // }
       // die;

       # One time call function. 
        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();
    
        #prepare webhook object for order...
       // $webhook_data_order = array(
       //      'scope' => 'store/order/*',
       //      'destination' => 'https://automatedarmy.com/api/v1/webhooks?access_token=6hH9qOc1sLvdL2uhIz8s4vymmj09MVjSc02DPfFh',  // [open issue] We hard code the token for test only
       //      'is_active' => true
       //  );

        # prepare webhook object for product...
        // $webhook_data_product = array(
        //     'scope' => 'store/product/deleted',
        //     'destination' => 'https://automatedarmy.com/api/v1/webhooks?access_token=qKVVMnO2E2MSUbHBnGUejsbivxPs2astUooHLdJq',  // redirect to Test server
        //     'is_active' => true
        // );

        // $response = $this->clientapi->createWebhook($webhook_data_order);
        // $response = $this->clientapi->createWebhook($webhook_data_product);
        // $response = $this->api->updateWebhook(12396191, $webhook_data);

        // $response = $this->clientapi->deleteWebhook(12635419);
        // $this->clientapi->createWebhook($webhook_data_product);

        // $api = new \LL\lib\BCAPI\masterConnector();
        // $this->api = $api->getClient();

        // $webhook_data = array(
        //     'scope' => 'store/product/deleted',
        //     'destination' => 'https://automatedarmy.com/api/v1/webhooks?access_token=eBnVBxe6cZpVCr1hcZ622PfdPER0y4tNFQJgoXnA',  // redirect to Test server
        //     'is_active' => true
        // );

        // $response = $this->clientapi->createWebhook($webhook_data);
        // $response = $this->clientapi->deleteWebhook(12642335);
        // $response = $this->clientapi->listWebhooks();
        // RD($response);

        RD("done");
        exit;
    } // End of Webhooks

    /**
     * Handle POST Request Sent By CustomerStore Webhook
     *
     * @return response with 200.
    */
    private function post($token = null)
    {   
        # analyze payload from BC Store
        $webhookContent = file_get_contents("php://input");
        $webhooks = json_decode($webhookContent, true);

        # give 200 for webhooks call
        http_response_code(200);

        // $myfile = fopen(BASE_PATH."/controllers/webhooks/productWeb.txt", "a") or die("Unable to open file!");
        // $txt = "Product call\n";
        // $now = date(DATE_RFC1123);
        // fwrite($myfile, $txt);
        // fwrite($myfile, $webhookContent);
        // fwrite($myfile, $now);
        // fclose($myfile);


        # Get Master/Customer store information
        $userId = $token["user_id"];
        $roleScope = explode("_", $token["scope"])[0];

        #Filter webhook scope.
        switch ($webhooks["scope"]) {
            ### Order ###
                case "store/order/created":
                    $orderId = $webhooks["data"]["id"];  // get orderId from the webhook
                    if ($roleScope == "master"){
                        # handle master store order created webhook call
                        $this->accountService->create_subscription_payment_record($orderId, null, false);
                    }else{
                        # handle customer store order created webhook call
                        # We insert the create request to DB.
                        $orderSyncInstance = array(
                            "order_id"     => $orderId,
                            "user_id"      => $userId,
                            "access_token" => $token['access_token'],
                            "client_id"    => $token['client_id'],
                            "expires"      => $token['expires'],
                            "scope"        => $token['scope']
                        );
                        $this->db->CheckInsert("orders_sync_queue", $orderSyncInstance, $orderSyncInstance);
                    }
                    break;
                case "store/order/statusUpdated":
                    if ($roleScope == "customerStore"){
                        $orderId = $webhooks["data"]["id"];
                        $updatedStatusId = $webhooks["data"]["status"]["new_status_id"];
                        $orderStatusRef = array(
                            0  => "Incomplete",
                            1  => "Pending",
                            2  => "Shipped",
                            3  => "Partially Shipped",
                            4  => "Refunded",
                            5  => "Cancelled", 
                            6  => "Declined",
                            7  => "Awaiting Payment",
                            8  => "Awaiting Pickup",
                            9  => "Awaiting Shipment",
                            10 => "Completed",
                            11 => "Partially Refunded"
                        );

                        $orderStatusUpdate = array(
                            "origin_store_status"    => $orderStatusRef[$updatedStatusId],
                            "origin_store_status_id" => $updatedStatusId
                        );

                        $this->db->Update("orders", $orderStatusUpdate, array("order_id" => $orderId, "account_id" => $userId));
                    }
                    break;
                case "store/order/archived":
                    // * Sync Order to delete ...
                    break;
            ### Product ###
                case "store/product/created":
                    # handle new Product added into the system
                    if ($roleScope == "master"){
                        #grab product Id
                        $productId = $webhooks["data"]["id"];
                        #Sync new Product into Our DB
                        $this->productService->syncProducts($userId);
                        #send Notification to All Customer store
                        $this->notificationService->generateNotificationForNewProducts($productId);
                    }
                    break;
                case "store/product/updated":
                    // if ($roleScope == 'master'){

                    //     ### make it as test for product inventory update
                    //     $productId = $webhooks["data"]["id"];

                    //     $api = new \LL\lib\BCAPI\masterConnector();
                    //     $this->api = $api->getClient();

                    //     # Compare Product information between master store and Our DB
                    //     ## Get Product By the master store API 
                    //     $masterProduct = $this->api->getProduct($productId);
                    //     $masterInventory = $masterProduct->inventory_level;
                    //     $masterPrice = $masterProduct->price;

                    //     ## Get Product from the Our DB
                    //     $productQuery = "
                    //         SELECT price, inventory_level 
                    //         FROM products 
                    //         WHERE product_id = $productId
                    //     ";

                    //     $product = $this->db->FetchOne($productQuery);

                    //     # Inventory Update
                    //     if ($masterInventory != $product["inventory_level"]){

                    //         # update the inventory for api DB
                    //         $this->db->Update(
                    //             "products", 
                    //             array("inventory_level" => $masterInventory),
                    //             array("product_id" => $productId)
                    //         );

                    //         # update the inventory for customer stores
                    //         $this->accountService->updateAccountProductInventoryLevel($productId, $masterInventory);
                    //     }

                    //     # Price Update. Comming soon.
                    //     ## code ...
                    // }
                    break;
                case "store/product/inventory/updated":
                    // if ($roleScope == "master"){
                    //     $productId = $webhooks["data"]["id"];
                    //     $newInventoryLevel = $webhooks["data"]["inventory"]["value"];
                    //     $this->accountService->updateAccountProductInventoryLevel($productId, $newInventoryLevel);
                    // }
                    break;
                case "store/product/deleted":
                    # If customer Store delete the product
                    if ($roleScope == "customerStore"){
                        #grab product Id
                        $productId = $webhooks["data"]["id"];
                        $where = array(
                            "account_id" => $userId,
                            "customer_product_id" => $productId,
                        );
                        # delete the product records from our DB ...
                        $this->db->Delete("account_product", $where);

                        // # delete product sku records from Our DB ...
                        // $whereProductSku = array(
                        //     "user_id"           => $userId,
                        //     "customer_product_id" => $productId,
                        // );
                        // $this->db->Delete("product_sku", $whereProductSku);
                    }
                    break;
                case "store/sku/inventory/updated":

                    // if ($roleScope == "master"){
                    //     # find the sku Id and new inventory level
                    //     $skuId = $webhooks["data"]["id"];
                    //     $newInventoryLevel = $webhooks["data"]["inventory"]["value"];
                    //     $this->accountService->updateAccountSkuInventoryLevel($skuId, $newInventoryLevel);
                    // }

                    break;
                default:
                    # code...
                    break;
        }

        # Send request back to webhook to prevent duplicate receiced
        http_response_code(200);

        echo "success";
    }

}
