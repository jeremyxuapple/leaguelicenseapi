<?php
/**
 * Main Products Controller
 */
namespace LL\controllers\management;
use LL\lib\database;
class managementIdController
{
    /**
     * Construct function that handles product requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific product ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = false)
    {
        $this->db = new \LL\lib\database\mysql();

        if (!method_exists($this, $method))
            throw new \Exception("Method doesn't exist: $method");
        
        $this->$method($id, $extra, $token);        
    }

    public function get($id, $extra, $token) 
    { 
      extract($token);

      switch ($scope) {
        case 'master':
          $s = "SELECT * FROM management_settings";
          $results = $this->db->Fetch($s);
          JSO($results);
          exit();
          break;
        case 'customerStore':
          $s = "SELECT * FROM store_management_settings WHERE account_id = $user_id";
          $results = $this->db->FetchOne($s);
          JSO($results);
          exit();
          break;
      }

        
        
    }

    public function post($id, $extra, $token) 
    {
        extract($token);
        switch ($scope) {
          case 'master':           
            $id = $token["user_id"];
          #Will need to add validation later on.        
            $shipping_markup = $_POST['shipping_markup'];
            $handling_markup = $_POST['handling_markup'];
            $inventory_subtraction_value = $_POST['inventory_subtraction_value'];
            $handling_ex_tax = $_POST['handling_ex_tax'];
            $handling_inc_tax = $_POST['handling_inc_tax'];

            $settings = array(
                "handling_markup" => $handling_markup,
                "shipping_markup" => $shipping_markup, 
                "inventory_subtraction_value" => $inventory_subtraction_value,
                "handling_ex_tax" => $handling_ex_tax,
                "handling_inc_tax" => $handling_inc_tax,
                "date_modified" => date("Y-m-d H:i:s")
                );
            $wheres = array("id" => $id);
        #Will need to add validation later on.       
          $this->db->CheckInsert("management_settings", $settings, $wheres);
              break;
          case 'customerStore': 

              $map_percentage = $_POST["map_percentage"];
              $shipping_markup = $_POST["shipping_markup"];

              $store_settings = array(
                "account_id" => $user_id,
                "map_percentage" => $map_percentage,
                "shipping_markup" => $shipping_markup,
                "date_created" => date("Y-m-d H:i:s"),
                "date_modified" => date("Y-m-d H:i:s"),
                );

              $this->db->CheckInsert("store_management_settings", $store_settings, array(
                "account_id" => $user_id));

            break;
          default:
            # code...
            break;
        }  
    }

    public function put($id, $extra, $token) 
    { 
      parse_str(file_get_contents('php://input'), $_PUT);
        extract($token);

        switch ($scope) {
          case 'master':
            $id = $token["user_id"];
          #Will need to add validation later on.        
            $shipping_markup = $_PUT['shipping_markup'];
            $handling_markup = $_PUT['handling_markup'];
            $inventory_subtraction_value = $_PUT['inventory_subtraction_value'];$handling_ex_tax = $_PUT['handling_ex_tax'];
            $handling_inc_tax = $_PUT['handling_inc_tax'];

            $settings = array(
                "handling_markup" => $handling_markup,
                "handling_ex_tax" => $handling_ex_tax,
                "handling_inc_tax" => $handling_inc_tax,
                "shipping_markup" => $shipping_markup, 
                "inventory_subtraction_value" => $inventory_subtraction_value,
                "date_modified" => date("Y-m-d H:i:s")
                );
            
            $this->db->Update("management_settings", $settings, array("id" => $id));

            JSO(array("success" => true));
            break;
          
          case 'customerStore': 
                 $map_percentage = $_PUT["map_percentage"];
              // $shipping_markup = $_PUT["shipping_markup"];

              $store_settings = array(
                "account_id" => $user_id,
                "map_percentage" => $map_percentage,
                // "shipping_markup" => $shipping_markup,
                "date_modified" => date("Y-m-d H:i:s"),
                );

              $this->db->Update("store_management_settings", $store_settings, array(
                "account_id" => $user_id));

              JSO(array("success" => true));
            break;
          default:
            # code...
            break;
        }
    }

}