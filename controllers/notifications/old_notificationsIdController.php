<?php
/**
 * Main Orders Controller
 */
namespace LL\controllers\notifications;
use LL\lib\database;
use LL\services;

class notificationsIdController
{
    /**
     * Construct function that handles supplier requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific supplier ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = null)
   {           
        $this->db = new \LL\lib\database\mysql();
        $this->service = new \LL\services\notificationService();

        if($method == "put") {
            parse_str(file_get_contents('php://input'), $_POST);
        }
        if (!method_exists($this, $method))
            throw new \Exception("Method doesn't exist: $method");
       
        # accessibility check
        $this->accessiblity = $this->checkAccessible($id, $token);
        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
        
   }

    public function get($id, $extra, $token) 
    {   
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array(); 

        if ($id == null) {
           $notifications = $this->service->getNotifications($options);
           $data["notifications"] = $notifications;

           $count = "SELECT count(*) AS count FROM notifications";
           $max = $this->db->FetchOne($count); 

           if (sizeof($data["notifications"]) > 0){
                # define basic data array for feedback
                $data["page"]          = $page + 1;
                $data["limit"]         = $limit;
                $data["count"]         = $max["count"];
                $data["maxpages"]      = ceil($max["count"] / $limit);
                $data["order_option"]  = $order_option;
                $data["order_sort"]    = $order_sort;
                
                JSO($data);
                http_response_code(200);
                exit;
            }
        } else {
           $this->service->getNotificationById($id, $extra);
           return;
        }
    }

    public function post()
    {
        
    }

    public function put()
    {

    }

    public function delete($id, $extra)
    {   
        $date_modified = date("Y-m-d H:i:s");
    
        if ($this->accessiblity == "master"){
            
            if($extra == "active") {

                $status = "Active";
                $notification = array(
                    "status" => $status,
                    "date_modified" => $date_modified
                    );
                ## reactive the warning status
                $result = $this->db->Update("notifications", $notification, array("id" => $id));
                  if ($result){
            # updated successed
                    http_response_code(202);
                    JSO(array("response" => "Notification With $id Has Been Activated!"));
                    exit;
                    }
            } 

            if(!isset($extra)) {
                $status = "Suspended";
                $notification = array(
                    "status" => $status,
                    "date_modified" => $date_modified
                    );
                $result = $this->db->Update("notifications", $notification, array("id" => $id));
                if ($result){
                # updated successed
                http_response_code(202);
                JSO(array("response" => "Notification With $id Has Been Suspended"));
                exit;
                }
            }
           
        }else{
            throw new \Exception("Unable To Identify, Access Forbidden");
        }

        http_response_code(404);
        JSO(array("Error" => "Notification Already Suspended Or Not Exist"));
        exit;
    }


      private function checkAccessible($warningId = null, $token)
    {  
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        switch($roleScope) {
            case "master":
                return "master";
                break;
        }

        http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }
}
