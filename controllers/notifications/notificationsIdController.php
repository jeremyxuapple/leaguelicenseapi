<?php

/**
 * Main Notification Controller
 */
namespace LL\controllers\notifications;
use LL\lib\database;
use LL\services;
use Mandrill;

class notificationsIdController
{
    /**
     * Construct function that handles supplier requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific supplier ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = null)
   {           
        $this->db = new \LL\lib\database\mysql();
        $this->notificationService = new \LL\services\notificationService();

        if($method == "put") {
            parse_str(file_get_contents('php://input'), $_POST);
        }
        if (!method_exists($this, $method))
            throw new \Exception("Method doesn't exist: $method");
       
        # accessibility check
        $this->accessiblity = $this->checkAccessible($id, $token);
        if ($this->accessiblity){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
        
   }

    public function get($id = null, $extra = null, $token = null) 
    {
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array();

        if (isset($id)){
            $data["notification"] = $this->notificationService->getNotificationById($id);
        }else{
            if ($this->accessiblity === "master"){
                $userId = $token["user_id"];
                $data["notifications"] = $this->notificationService->getNotifications($userId, $options);

                if ($data["notifications"]){
                    $count = "SELECT count(*) AS count FROM notifications";
                    $max = $this->db->FetchOne($count);
                }

                # define basic data array for feedback
                $data["page"]          = $page + 1;
                $data["limit"]         = $limit;
                $data["count"]         = $max["count"];
                $data["maxpages"]      = ceil($max["count"] / $limit);
                $data["order_option"]  = $order_option;
                $data["order_sort"]    = $order_sort;

            }else{
                # fail for accessible check 
                // http_response_code(403);
                JSO(array("Error" => "403 Access Forbidden"));
                throw new \Exception("Unable To Identify, Access Forbidden");
            }
        }

        // if (sizeof($data["notification"]) > 0 || sizeof($data["notifications"]) >0 ){
                
        // }
        // http_response_code(200);
        JSO($data);
        exit;

        # no data found
        // http_response_code(404);
        // JSO(array("Error" => "404 Not Found"));
        // exit;
    }

    /** 
     * Open Endpoint for future development
    */
    private function post()
    {
        // http_response_code(501);
        
        throw new \Exception("Not Implemented, Might Open In The Future");
    }

    /** 
     * Open Endpoint for future development
    */
    private function put($id = null, $extra = null, $token = null)
    {
        switch ($extra) {
            case 'delete':
                // Update the notification status to close
                $this->db->Update('notifications', array('status' => 'closed'), array('id' => $id));
                break;
            case 'activate':
                // Update the notification status to open
                $this->db->Update('notifications', array('status' => 'open'), array('id' => $id));
                break;
            default:
                break;
        }
        
        throw new \Exception("Not Implemented, Might Open In The Future");
    }

    /**
     * Handle Close Notification Case By Notification Send From System Only, Stand out of RMA  
     * Notification from_user_id = 0 denotes send by system directly
     *
     * @param $id
     * @return response
    */  
    public function delete($id = null)
    {   
        # check notification 
        $query = "SELECT from_user_id FROM notifications WHERE id = $id";
        $result = $this->db->FetchOne($query);

        # notification sent by system not RMA
        if ($result["from_user_id"] == 0 ){
            $updatedResult = $this->db->Update("notifications", array("status" => "Closed"), array("id" => $id));
            if ($updatedResult){
                http_response_code(200);
                JSO(array("success" => "Notification With Id $id Has Been Closed"));
                exit;
            }
        }

        throw new \Exception("RMA Notification in Proccessing, Can Not Modified By This Operation");
    }

    /** 
     * Accessibility Check For Masterstore, CustomerStore, Supplier
     *
     * @param $id - int - auto increament id in reutrns table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($id = null, $token)
    {  
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        if ($id){
            $query = "SELECT id FROM notifications WHERE from_user_id = $userId OR to_user_id = $userId";
            $result = $this->db->FetchOne($query);

            if (isset($result["id"])){
                return $roleScope;
            }
        }else{
            return $roleScope;
        }

        // http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }
}
