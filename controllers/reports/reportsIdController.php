<?php
/**
 * Main Reports Controller
*/

namespace LL\controllers\reports;

use LL\lib\database;
use LL\services;

class reportsIdController
{
	
	public function __construct($method = 'get', $id = null, $extra = null, $token = false)
	{
		# define service instance
		$this->orderService   = new \LL\services\orderService();
        $this->accountService = new \LL\services\accountService();
        $this->productService = new \LL\services\productService();

		# sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessibility = $this->checkAccessible($id, $token);
        $this->$method($id, $extra);
	}

	/**
	 * Export CSV File And Offer To Download
	 *
	 * @param $id - int - userId
	 *
	*/
	public function get($id = null, $extra = null, $token = null)
	{
		$options = array(
			"date_start" => isset($_GET["date_start"]) ? $_GET["date_start"] : "2017-05-01 00:00:00", // Demo Only for the very beginning day
			"date_end" => isset($_GET["date_end"]) ? $_GET["date_end"] : date("Y-m-d H:i:s"),  // make current time 
			"type" => isset($_GET["type"]) ? $_GET["type"] : null, //mostly is the status of report object
		);
		extract($options);

		if ($date_start && $date_end && $type ){
			switch ($this->accessibility) {
				case 'master':
                case 'customerStore':
					# master extra path 
					switch ($extra) {
						case 'stores':
							# get all accounts information
							$results = $this->accountService->getAccountsForReports($options);
							header('Content-Disposition: attachment; filename="stores_reports.csv"');
							break;
						case 'subscriptions':
							switch ($type) {
								case 'total_subscriptions_sales':
									$results = $this->accountService->getTotalSubscriptionsSalesForReports($options);
									header('Content-Disposition: attachment; filename="subscriptions_total_sales_reports.csv"');
									break;
								case 'total_product_sales':
									$results = $this->productService->geTotalProductSalesForReports($options);
									header('Content-Disposition: attachment; filename="subscriptions_total_products_sales_reports.csv"');
									break;
								case 'total_cost_of_product':
									# code ...
									break;
							}
							break;
						case 'suppliers':
							# code ... 
							break;
					}
					break;
				case 'customerStore':
					# customer store extra path
					switch ($extra) {
						case 'allOrders':
							break;
						case 'UnshippedOrders':
							break;
						case 'Sales':
							break;
					}
					break;
				case 'supplier':
					break;
			}

			if (isset($results) && sizeof($results) >= 1){
				$fp = fopen('php://output', 'w');

				foreach ($results[0] as $key => $value) {
					$header[] = $key;
				}

				fputcsv($fp, $header);

				foreach ($results as $result) {
					$out = array();
					foreach ($result as $key => $value) {
						$value = $value == null ? "null" : $value;
						$out[] = $value;
					}
					fputcsv($fp, $out);
				}

				# header
				header('Content-type: text/csv');
				header('Pragma: no-cache');  // not cache the file ...
				header('Expires: 0'); // not cache the file ...
				exit;
			}else{
				http_response_code(404);
				JSO(array("Error" => "404 Not Found"));
	            exit;
			}
		}else{
			// http_response_code(400);
			JSO(array("Error" => "400 Bad Request. Please Select Start Time/End Time/Type of Report"));
			exit;
		}
	}

	/**
     * Accessiblity Check For Masterstore, Supplier And Customer 
     *
     * @param $orderId - int - Auto Increment id in orders table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($id, $token)
    {
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];
        $userId = $token["user_id"];

        if ($id == $userId){
        	switch ($roleScope) {
            case "master":
                return "master";
            case "supplier":
                return "supplier";
            case "customerStore":
                return "customerStore";
        	}
        }

        // for dev purposes
        return "master";

        # fail for accessible check
        // http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

}

