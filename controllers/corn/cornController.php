<?php
/**
 * Main App Controller
 */

namespace LL\controllers\corn;

use LL\lib\database;


class cornController
{
   public function __construct($method = 'get', $extra = null)
    {   
        $this->db = new \LL\lib\database\mysql();

        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        $this->productService = new \LL\services\productService();
        $this->accountService = new \LL\services\accountService();
        $this->orderService   = new \LL\services\orderService();
        $this->notificationService = new \LL\services\notificationService();
        
        if (!method_exists($this, $method))
            throw new \Exception("Method does not exist: $method");

        $this->$method($extra);
    }

    // $extra would be the store_hash for the store to grab the account information
    public function get($extra)
    {  

        switch ($extra) {
            case 'updateInventory':
                $this->updateInventory();
                break;

            case 'syncProducts':
                $this->syncProducts();
                break;

            case 'cleanCustomerProducts':
                $this->cleanCustomerProducts();
                break;

            case 'importProducts':
                $this->importProducts();
                break;

            case 'syncNewOrders':
                $this->syncNewOrdersIntoDB();
                break;
        }

        die("Job's Done");
    }


    /*
     * Handle Products sync
    */
    public function syncProducts() {

        $syncTime = date("Y-m-d H:i:s"); // ref to handle deleted data from master store
        # Sync Service Only Apply To master store. So Let's define user_id at the very beginning
        $userId  = 1;

        $myfile = fopen(BASE_PATH."/controllers/corn/cornJobs.txt", "w") or die("Unable to open file!"); // re-create the file
        fwrite($myfile, "Update Start Time: " . date("Y-m-d H:i:s" . "\n"));
    
       # init the sync product api.
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        ### Insert / Update product_categories table
        $categoriesAvailable = true;
        $categoriesPage = 1;

        // Loop Through 
        while ($categoriesAvailable){
            fwrite($myfile, "Category Page \n");
            fwrite($myfile, $categoriesPage);
            fwrite($myfile, "\n");
            $allCategories = $this->api->getCategories(array('limit' => 250, 'page' => $categoriesPage));

            // fwrite($myfile, print_r($allCategories));

            if (!isset($allCategories)){
                $categoriesAvailable = false;
                break; // break out the while loop
            }

            foreach ($allCategories as $category) {
                $categoryInstance = $this->generateProductCategoryInstance($category, $userId, "master", $syncTime);
                $whereCategory = array(
                    "user_id"            => $userId,
                    "master_category_id" => $category->id,
                    "master_parent_id"   => $category->parent_id,
                );
                $categoryObject = $this->db->CheckInsert("product_categories", $categoryInstance, $whereCategory, true);
            }
            $categoriesPage += 1; // go to next page
        } // End of While loop for categories

        ### Insert / Update Product_brands table
        $brandsAvailable = true;
        $brandPage = 1;

        // Loop through
        while ($brandsAvailable){
            fwrite($myfile, "Brand Page \n");
            fwrite($myfile, $brandPage);
            fwrite($myfile, "\n");
            $allbrands = $this->api->getBrands(array('limit' => 250, 'page' => $brandPage));

            if (!isset($allbrands)){
                $brandsAvailable = false;
                break; // break out the brand while loop
            }

            foreach ($allbrands as $brand) {
                $brandInstance = $this->generateProductBrandInstance($brand, $userId, $syncTime);
                $whereBrand  = array(
                    "user_id"  => $userId,
                    "brand_id" => $brand->id,
                    "name"     => $brand->name
                );
                $brandObject = $this->db->CheckInsert("product_brands", $brandInstance, $whereBrand, true);
            }
            $brandPage += 1; // go to next page
        } // End of While loop for brands


        # Insert / Update products, product skus /sku options and product_image tables
        $productsAvailable = true;
        $productsPage = 1;

        while ($productsAvailable){

            fwrite($myfile, "===============Product Page=============== \n");
            fwrite($myfile, $productsPage);
            fwrite($myfile, "\n");

            $allProducts = $this->api->getProducts(array('limit'=> 250, 'page'=> $productsPage));

            if (!isset($allProducts)){
                $productsAvailable = false;
                break; // break out the while loop
            }

            foreach ($allProducts as $product) {
                # product_image table
                // RD("############################");
                // RD("product id is $product->id");
                // RD("product id is $product->sku");
                // RD("product id is $product->name");
                if ($product->primary_image->id > 0) {
                    // RD($product->primary_image);
                    // $images = $this->api->getResource("/products/6818/images");
                    $image = $product->primary_image;

                    $imageInstance = array(
                        "user_id"              => $userId,
                        "master_image_id"      => $image->id,
                        "master_product_id"    => $product->id,
                        "zoom_url"             => $image->zoom_url,
                        "thumbnail_url"        => $image->thumbnail_url,
                        "standard_url"         => $image->standard_url,
                        "tiny_url"             => $image->tiny_url,
                    );

                    $whereImage = array(
                        "user_id"              => $userId,
                        "master_image_id"      => $image->id,
                        "master_product_id"    => $product->id,
                    );

                    $imageObject = $this->db->CheckInsert("product_image", $imageInstance, $whereImage, true);
                  }
                #products table
                $productInstance = $this->generateProductInstance($product, $syncTime);

                $whereProduct = array(
                    "product_id" => $product->id,
                    "sku"        => $product->sku,
                );
                $productObject = $this->db->CheckInsert("products", $productInstance, $whereProduct, true);
            } // end of foreach loop

            $productsPage += 1; // go to next page 
        } // end of while loop

        # clean Deleted Sync Data
        $this->cleanDeletedSyncData($syncTime, $myfile);

        fwrite($myfile, "Sync Product Done \n");
        fwrite($myfile, "Last Update time: " . date("Y-m-d H:i:s"));        
        fclose($myfile);

        die("Sync Done");
    } // End of function synproducts

    /*
     * Handle Inventory update for all products
    */
    public function updateInventory()
    {
       // init inventory update log
        $myfile = fopen(BASE_PATH."/controllers/corn/inventoryCorn.txt", "w") or die("Unable to open file!");

        // init master store api connector
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        //init condition for while loop
        $productsAvailable = true;
        $productsPage = 1;

        # define check Update Time as reference for next time call
        $refUpdateTime = Date("Y-m-d H:i:s");

        # init define product array to pass them to customer store
        $updatedProducts = array();

        # fetch last updated time.
        $updateTimeQuery = "SELECT MAX(date_modified) AS date_modified FROM products";
        $updateTimeResult = $this->db->FetchOne($updateTimeQuery);
        $updateTime = $updateTimeResult['date_modified'];

        # Get All Products By filter
        while ($productsAvailable){

            fwrite($myfile, "===============Product Page=============== \n");
            fwrite($myfile, $productsPage);
            fwrite($myfile, "\n");
            
            $allProducts = $this->api->getProducts(array('limit'=> 250, 'page'=> $productsPage, 'min_date_modified' => $updateTime ));

            // fail condition for product not available
            if (!isset($allProducts)){
                $productsAvailable = false;
                break; // break out the while loop
            }else{
                if (sizeof($allProducts) > 0){
                    if (is_array($allProducts) || is_object($allProducts)) {
                        ## if products Available, check product inventory updated or not. if yes, then we push to updated Product array
                        foreach ($allProducts as $product) {

                            fwrite($myfile, "product name: " . $product->name);
                            fwrite($myfile, "\n");
                            fwrite($myfile, "product new inventory_level: " . $product->inventory_level);
                            fwrite($myfile, "\n");

                            $productId = $product->id;
                            $productCurrentInventory = $product->inventory_level;

                            # Check If it's inventory update
                            $productQuery = "SELECT product_id FROM products WHERE product_id = $productId AND inventory_level = $productCurrentInventory";
                            $productQueryResult = $this->db->FetchOne($productQuery);

                            if (!isset($productQueryResult['product_id'])){
                                # Update Inventory and updateTime for Our API DB
                                $this->db->Update("products", array('inventory_level' => $productCurrentInventory, 'date_modified' => $refUpdateTime), array('product_id' => $productId));

                                # buiild updated products array
                                array_push($updatedProducts, array("productId" => $productId, "inventory_level" => $productCurrentInventory));
                            }
                        } // End of foreach
                    } //end of if check for invalid or not 
                } // End of size of $all products
            } // End of else for has allproducts

            $productsPage += 1; // go to next page
        }// End of while loop 

        # loop thorugh updatedProducts array and pass them into customer store
        foreach ($updatedProducts as $updatedProduct) {
            $this->updateAccountProductInventoryLevel($updatedProduct["productId"], $updatedProduct["inventory_level"]);
        }

        fwrite($myfile, "Update inventory Done \n");
        fwrite($myfile, "Last Update time: " . $refUpdateTime);
        fwrite($myfile, "\n");
        fwrite($myfile, "Completed Update time: " . date("Y-m-d H:i:s"));
        fclose($myfile);
        // die("updateInventory done");
    } // End - function updateInventory();


    /**
     * Handle Update Customer Store Inventory
     * 
     * @param productId
     * @param newInventoryLevel
     * @return void 
    */
    public function updateAccountProductInventoryLevel($productId, $newInventoryLevel)
    {
        # Find all active Customer Store with this ProductId
        $customerStoreInfosQuery = "
            SELECT ap.account_id, ap.customer_product_id, p.name
            FROM account_product ap
            LEFT JOIN products p
                ON p.product_id = ap.master_product_id
            LEFT JOIN oauth_access_tokens oat
                ON ap.account_id = oat.user_id
            WHERE ap.master_product_id = $productId AND oat.expires >= CURDATE()
        ";

        $customerStoresInfos = $this->db->Fetch($customerStoreInfosQuery);

        # check it has customer store has this product
        if (sizeof($customerStoresInfos) > 0 ){
            foreach($customerStoresInfos as $info){

                $accountId = $info["account_id"];
                $customerProductId = $info["customer_product_id"];
                $productName = $info["name"];
                $accountQuery = "
                    SELECT client_id, user_id, access_token 
                    FROM oauth_access_tokens 
                    WHERE user_id = $accountId
                ";

                $account = $this->db->FetchOne($accountQuery);

                # make token here
                $token = array(
                    "access_token" => $account["access_token"],
                    "client_id"    => $account["client_id"],
                    "user_id"      => $account["user_id"],
                );

                # init customer store api 
                $api = new \LL\lib\BCAPI\customerStoreConnector($token);
                $this->clientapi = $api->getClient();

                # get inventory Subtraction value
                $inventorySubtractionQuery = "SELECT inventory_subtraction_value FROM management_settings";
                $inventorySubtractionResult = $this->db->FetchOne($inventorySubtractionQuery);

                if ($newInventoryLevel < $inventorySubtractionResult["inventory_subtraction_value"]){
                    # disable product 
                    $disableObject = array(
                        "availability" => "disabled",
                        // "is_visible" => false // Hide from the List
                    );

                    $this->clientapi->updateProduct($customerProductId, $disableObject);
                    $this->clientapi->updateProduct($customerProductId, array("inventory_level" => $newInventoryLevel));

                    #send notification to Customer Store
                    // $notification = array(
                    //     "from_user_id" => 0, //system
                    //     "to_user_id"   => $account["user_id"],
                    //     "entity_id"    => $customerProductId,
                    //     "entity_type"  => "products",
                    //     "entity_name"  => "Your Store",
                    //     "details"      => "Product: $productName - Inventory Low. Has been Disable",
                    //     "status"       => "Open",
                    //     "msg_type"     => "Disable Product"
                    // );

                    #send notification to the customer store
                    // $this->notificationService->createNotification($notification);

                }else{
                    # enable object
                    $enableObject = array(
                        "availability" => "available",
                        "is_visible" => true,
                    );

                    $this->clientapi->updateProduct($customerProductId, $enableObject);
                    # update customer Store product inventory
                    $this->clientapi->updateProduct($customerProductId, array("inventory_level" => $newInventoryLevel));
                }
            } // End - Foreach Loop
        } // End - if customerStoresInfos
    } // End - Function updateAccountProductInventoryLevel(...)


    /**
     * Hanldle Product Instance.
     * Why Should not just call product service? Because each time, we call. we set up a MySQL Connection.
     * We have to prevent this happenc
     * @param $product - object
     * @return $product - object with essential fields
    */
    public function generateProductInstance($product, $syncTime)
    {
        $newProduct = array(
                # List below Confirmed with Doug...
                "product_id"                => $product->id,
                "name"                      => $product->name,
                "type"                      => "physical", // Always physical
                "sku"                       => $product->sku, // UPC (double check scope doc), manufactory sku
                "description"               => $product->description,
                "search_keywords"           => $product->search_keywords,            
                "price"                     => $product->price, // wholesale price * mark-up for customer
                // "calculated_price"          => $product->calculated_price,  // wholesale price * mark-up for customer // Read Only
                "is_visible"                => $product->is_visible,
                "inventory_level"           => $product->inventory_level,
                "inventory_warning_level"   => 5, // 5 always (for now)
                "warranty"                  => $product->warranty,
                "weight"                    => $product->weight,
                "width"                     => $product->width,
                "height"                    => $product->height,
                "depth"                     => $product->depth,
                "inventory_tracking"        => $product->inventory_tracking, // YES
                "date_created"              => $product->date_created,
                "page_title"                => $product->name, //Same as Product "Name"
                "meta_keywords"             => $product->meta_keywords,
                "meta_description"          => $product->meta_description,
                "categories"                => implode(",", $product->categories),  // We Save the categories to be text
                "brand_id"                  => $product->brand_id,
                // "date_modified"             => $product->date_modified,
                "condition"                 => $product->condition, // always NEW
                "upc"                       => $product->upc, // Same as SKU
                // "date_last_imported"        => $product->date_last_imported,  // it doesn't matter
                "option_set_id"             => $product->option_set_id,
                "bin_picking_number"        => $product->bin_picking_number,
                "availability"              => $product->availability, //|| "In-Stock" (this is a string sentenence, not a number)
                "date_created"              => $syncTime
            );
        return $newProduct;
    }

    /**
     * Handle Categories instance. Copy form product service. 
     * Why Should not just call product service? Because each time, we call. we set up a MySQL Connection.
     * We have to prevent this happend
     * @param $category - object 
     * @return $category - object with essential fields
    */
    public function generateProductCategoryInstance($category, $userId, $role, $syncTime)
    {
        $newCategory = array(
            "user_id"              => $userId,
            // "master_category_id"   => $category->id,
            // "master_parent_id"     => $category->parent_id,
            "name"                 => $category->name,
            "description"          => $category->description,
            // "sort_order"           => $category->sort_order, // sort order is not a option we update
            "page_title"           => $category->page_title,
            "meta_keywords"        => $category->meta_keywords,
            "meta_description"     => $category->meta_description,
            "layout_file"          => $category->layout_file,
            "parent_category_list" => implode("," , $category->parent_category_list), // Read Only. We convert array into string
            "image_file"           => $category->image_file,
            // "is_visible"           => $category->is_visible,   // Read Only
            "search_keywords"      => $category->search_keywords,
            "url"                  => $category->url,
            "date_created"         => $syncTime // time ref to handle deleted categories.
        );

        if ($role == "master"){
            $newCategory["master_category_id"] = $category->id;
            $newCategory["master_parent_id"]   = $category->parent_id;
        }
        return $newCategory;
    }

    /**
     * Handle create brand instance. 
     */
    public function generateProductBrandInstance($brand, $userId, $syncTime)
    {
        $newBrand = array(
            "user_id"          => $userId,
            "brand_id"         => $brand->id,
            "name"             => $brand->name,
            "page_title"       => $brand->page_title,
            "meta_keywords"    => $brand->meta_keywords,
            "meta_description" => $brand->meta_description,
            "image_file"       => $brand->image_file,
            "search_keywords"  => $brand->search_keywords,
            "date_created"     => $syncTime,
            "date_modified"    => $syncTime
        );
        return $newBrand;
    }

    /**
     * Handle Products which not deleted by webhooks.
     * we have to loop through each customer. and get all customer store products 
     * Then compare the products id in our table. if it's not. Then We deleted the products in our table.
     */
    public function cleanCustomerProducts(){

        # For Customer Stores has products
        $accountsIdQuery = "
            SELECT DISTINCT ap.account_id
            FROM account_product ap
            LEFT JOIN oauth_access_tokens oat
            ON ap.account_id = oat.user_id
            WHERE oat.expires >= CURDATE() AND ap.account_id <> 649305
        ";

        $accountsIdResult = $this->db->Fetch($accountsIdQuery);

        # Loop Through All customer store accounts
        // foreach ($accountsIdResult as $account) {
            // $accountId = $account["account_id"];
            $accountId = 649305;

            $accountQuery = "
                    SELECT client_id, user_id, access_token
                    FROM oauth_access_tokens 
                    WHERE user_id = $accountId
                ";

                $account = $this->db->FetchOne($accountQuery);

                # make token here
                $token = array(
                    "access_token" => $account["access_token"],
                    "client_id"    => $account["client_id"],
                    "user_id"      => $account["user_id"],
                );

                # init customer store api 
                $api = new \LL\lib\BCAPI\customerStoreConnector($token);
                $this->clientapi = $api->getClient();
                # get store
                $store = $this->clientapi->getStore();

                if ($store){

                    # Get All products in the Store and Only store product id and prepare for the matching
                    $productsAvailable = true;
                    $productsPage = 1;

                    $customerProductsArray = array(); // init the empty array

                    while ($productsAvailable){
                        RD("product page is $productsPage");
                        $allProducts = $this->api->getProducts(array('limit'=> 250, 'page'=> $productsPage));

                        // fail condition for product not available
                        if (!isset($allProducts)){
                            $productsAvailable = false;
                            break; // break out the while loop
                        }else{
                            ## if products Available. add product into array
                            foreach ($allProducts as $product) {
                                $productId = $product->id;
                                array_push($customerProductsArray, $productId);
                            }
                        }
                        # product page + 1
                        $productsPage ++;
                    }

                    # process the products array, delete the products from our DB which not in the actual store
                    // $storeDbProductsQuery = "
                    //     DELETE FROM account_product
                    //     WHERE account_id = $accountId
                    //     AND customer_product_id NOT IN ('" . implode($customerProductsArray, "', '") . "' )";

                    // $storeDbProductsResult = $this->db->Query($storeDbProductsQuery);

                    $storeDbProductsQuery = "
                        SELECT customer_product_id 
                        FROM account_product
                        WHERE account_id = $accountId
                        AND customer_product_id NOT IN ('" . implode($customerProductsArray, "', '") . "' )";

                    $storeDbProductsResult = $this->db->fetch($storeDbProductsQuery);


                    RD("account : $accountId");
                    RD($storeDbProductsResult);
                    RD("==================");
                }else{
                    RD("This store - $accountId is close");
                }
                
        // } // End of foreach accounts
    } // End of public function cleanCustomerProducts()


    /**
     * Handle importProduct to customer store actions
     * excute from the products_import_queue table
     */
    public function importProducts(){
        $importProductsQuery = "SELECT * FROM products_import_queue";
        $importProductsResults = $this->db->Fetch($importProductsQuery);

        if (sizeof($importProductsResults) > 0){
            # After fetch all data, then we deleted all rows and unset this table for next time use.
            $this->db->Query("DELETE FROM products_import_queue"); // delete all data.
            
            foreach ($importProductsResults as $importProductsResult) {
                sleep(5); // sleep 5s to prevent too many api call

                # define account id and customerCategoryId
                $accountId = $importProductsResult['account_id'];
                $customerCategoryId = $importProductsResult['customer_category_id'];

                # prepare generate account token
                $accountQuery = "
                    SELECT client_id, user_id, access_token
                    FROM oauth_access_tokens 
                    WHERE user_id = $accountId
                ";
                $account = $this->db->FetchOne($accountQuery);

                # make token 
                $token = array(
                    "access_token" => $account["access_token"],
                    "client_id"    => $account["client_id"],
                    "user_id"      => $account["user_id"],
                );
                # make str into ids array
                $productIdsArray = explode(",", $importProductsResult["master_product_ids"]);

                foreach ($productIdsArray as $productId ) {
                    $this->productService->importProductToAccount($productId, $token, true, $customerCategoryId, true);
                }
            } // End of foreach
        } // End Of if importProductsResults > 0
    } // End of public function importProducts

    /**
     * Handle Deleted store Products, categories, brands
     * Deleted Those data without created_date match the syncTime
     */
    public function cleanDeletedSyncData($syncTime, $myfile)
    {   
        fwrite($myfile, "clean Job Start \n");
        fwrite($myfile, "sync created time is $syncTime");

        $tables = array("products", "product_categories", "product_brands");
        $deletedProductIdsArray = array(); // init

        foreach ($tables as $table) {
            # make deleted Products Id ref
            if ($table == "products"){
                // RD("job table is products now");
                $deletedProductIds = $this->db->Fetch("
                    SELECT product_id FROM $table WHERE user_id = 1 AND date_created <> '$syncTime';
                ");
                if (sizeof($deletedProductIds) > 0) {
                    foreach ($deletedProductIds as $deletedProductId) {
                        // RD("deleted Product Id is $deletedProductId['product_id']");
                        // array_push($deletedProductIdsArray, $deletedProductId['product_id']); // add to ref array
                        $this->masterStoreDeletedProduct($deletedProductId['product_id']);
                    }
                }
            } // End Of if $table == "products"

            # do the job for clean table
            $this->db->Query("
                DELETE FROM $table WHERE user_id = 1 AND date_created <> '$syncTime';
            ");
            fwrite($myfile, "$table clean done \n");

            RD("clean Table $table");
        } // End Of foreach $tables as $table

        // # when master store deletes products. We need to call this function
        // if (sizeof($deletedProductIdsArray) > 0){
        //     foreach ($deletedProductIdsArray as $deletedProductId) {
        //         $this->masterStoreDeletedProduct($deletedProductId);
        //     }   
        // }
    } // End of function cleanDeletedSyncData(...)

    /**
     * Handle Sync New Orders from customer store into Our DB.
     * Loop through table and do the job
     */
    public function syncNewOrdersIntoDB()
    {
        $syncProductsQuery = "SELECT * FROM orders_sync_queue";
        $syncProductsResults = $this->db->Fetch($syncProductsQuery);

        if (sizeof($syncProductsResults) > 0 ){
            # After fetch all data, then we deleted all rows and unset this table for next time use.
            $this->db->Query("DELETE FROM orders_sync_queue"); // delete all data.

            foreach ($syncProductsResults as $syncProductResult) {
                sleep(1); // sleep 1s to prevent too many api call
                $orderId = $syncProductResult['order_id'];
                $userId = $syncProductResult['user_id'];
                $token = array(
                    "user_id"      => $syncProductResult['user_id'],
                    "access_token" => $syncProductResult['access_token'],
                    "client_id"    => $syncProductResult['client_id'],
                    "expires"      => $syncProductResult['expires'],
                    "scope"        => $syncProductResult['scope']
                );

                # call order sync function
                $this->orderService->syncOrderByIds($orderId, $userId, $token);
            }
        }
    } // End of function syncNewOrdersIntoDB()

    /**
     * Hande master store deleted products
     * 1. disable and hide this product in customer store.
     * 2. Send customer store information. 
     */
    public function masterStoreDeletedProduct($productId){
        RD("into masterStoreDeletedProduct function");
        RD($productId);
        # Find all active Customer Store with this ProductId
        $customerStoreInfosQuery = "
            SELECT ap.account_id, ap.customer_product_id, p.name
            FROM account_product ap
            LEFT JOIN products p
                ON p.product_id = ap.master_product_id
            LEFT JOIN oauth_access_tokens oat
                ON ap.account_id = oat.user_id
            WHERE ap.master_product_id = $productId AND oat.expires >= CURDATE()
        ";
        $customerStoresInfos = $this->db->Fetch($customerStoreInfosQuery);
        if (sizeof($customerStoresInfos) > 0 ){
            foreach($customerStoresInfos as $info){
                $accountId = $info["account_id"];
                $customerProductId = $info["customer_product_id"];
                $productName = $info["name"];
                $accountQuery = "
                    SELECT client_id, user_id, access_token 
                    FROM oauth_access_tokens 
                    WHERE user_id = $accountId
                ";
                $account = $this->db->FetchOne($accountQuery);
                # make token here
                $token = array(
                    "access_token" => $account["access_token"],
                    "client_id"    => $account["client_id"],
                    "user_id"      => $account["user_id"],
                );
                # init customer store api 
                $api = new \LL\lib\BCAPI\customerStoreConnector($token);
                $this->clientapi = $api->getClient();

                # make disable object
                $disableObject = array(
                        "availability" => "disabled",
                        // "is_visible" => false // Hide from the List
                        "inventory_level" => 0, // set inventory level to be 0 in the customer store.
                );

                $this->clientapi->updateProduct($customerProductId, $disableObject); // update product

                #send notification to Customer Store
                $notification = array(
                    "from_user_id" => 0, //system
                    "to_user_id"   => $account["user_id"],
                    "entity_id"    => $customerProductId,
                    "entity_type"  => "products",
                    "entity_name"  => "Your Store",
                    "details"      => "Product: $productName - Deleted By LL Admin. Has been Disable In Your Store",
                    "status"       => "Open",
                    "msg_type"     => "Disable Product"
                );

                #send notification to the customer store
                $this->notificationService->createNotification($notification);

            }// End of foreach($customerStoresInfos as $info)
        }// End of  if (sizeof($customerStoresInfos) > 0 )
    } // End of public function masterStoreDeletedProduct(...)

    /**
     * Handle To Find out What products is in master store remove category.
     * Then We pass Those Products to function masterStoreDeletedProduct($productId) 
     */
    // public function helper_master_store_products_in_removal_category(){
    //     $removalCategoryId = 100000; // set the fixed number for the removel Category
        
    //     # find out all products in the removel category
    //     $productIds = $this->db->Fetch("SELECT ");

    // } // End of public function helper_master_store_products_in_remove_category()

} // End - cornController




