<?php
/**
 * Main App Controller
 */

namespace LL\controllers\beta;

use LL\lib\database;


class betaController
{
	 public function __construct($method = 'get', $extra = null)
    {		
        $this->db = new \LL\lib\database\mysql();
       	
       	if (!method_exists($this, $method))
            throw new \Exception("Method does not exist: $method");

        $this->$method($extra);
    }

    // $extra would be the store_hash for the store to grab the account information
    public function get($extra)
    {	
    	// $extra = (string)$extra;

      # extra would be store hash right now
    	$query = "SELECT a.account_id, oat.access_token FROM accounts a 
                  LEFT JOIN oauth_access_tokens oat
                  ON a.code = oat.client_id
                  WHERE a.code = '$extra'";

    	$account_info = $this->db->FetchOne($query);
      $account_info["success"] = true;
    	JSO($account_info);

    	exit();
    }

}