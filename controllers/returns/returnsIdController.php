<?php
/** 
 * Main Returns Controller
*/

namespace LL\controllers\returns;

use LL\lib\database;

class returnsIdController
{
    public function __construct($method = 'get', $id = null, $extra = null, $token = false)
    {
        # define service instance
        $this->db = new \LL\lib\database\mysql();
        $this->orderService   = new \LL\services\orderService();
        $this->accountService = new \LL\services\accountService();
        $this->productService = new \LL\services\productService();
        $this->returnService = new \LL\services\returnService();
        $this->notificationService = new \LL\services\notificationService();

        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessibility = $this->checkAccessible($id, $token);

        if ($this->accessibility){
            $this->$method($id, $extra, $token);
        }else{
            throw new Exception("Unable To Identify, Access Forbidden");
        }
    }

    /**
     * Handle Return Get Method 
     *
     * @param void / $id - int - auto increment id from order_product_return table
     * @param $extra - string - URI path
     * 
     * @return mix return - array
    */ 
    public function get($id = null, $extra = null, $token = null)
    {
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_created',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : null
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options);
        $data = array();

        # get returns information
        if (isset($id)){
            $data["return"] = $this->returnService->getReturnById($id);
        }else{
            $data["returns"] = $this->returnService->getReturns($options);
            $count = "SELECT count(*) AS count FROM order_product_return";
            $max = $this->db->FetchOne($count);

            # define basic data array for feedback
            $data["page"]          = $page + 1;
            $data["limit"]         = $limit;
            $data["count"]         = $max["count"];
            $data["maxpages"]      = ceil($max["count"] / $limit);
            $data["order_option"]  = $order_option;
            $data["order_sort"]    = $order_sort;
        }

        // if (sizeof($data["returns"]) > 0 || sizeof($data["return"]) > 0){
        //     http_response_code(200);
            
        // }
        
        JSO($data);
        exit;

        # no data found
        // http_response_code(404);
        // JSO(array("Error" => "404 Not Found"));
        // exit;
    }

    /**
     * Create Return 
     *
     * @param $_POST ... 
     * @return $response
    */
    public function post()
     {  
        # check order completed ... 
        $order = $this->orderService->getOrderBaseByIds($_POST["order_id"], $_POST["account_id"]);

        if ($order["status"] == "Shipped" || $order["origin_store_status"] == "Shipped"){

            # Only customer store can create return product
            if ($this->accessibility == "customerStore"){

                $product = $this->returnService->generateProductReturnInstance();
                $product["status"] = "Open"; // initial status
                // RD($_POST);
                // RD($product);
                $where = array(
                    "order_id"      => $_POST["order_id"],
                    "account_id"    => $_POST["account_id"],
                    "supplier_id"   => $_POST["supplier_id"],
                    "sku"           => $_POST["sku"]
                );

                # insert order_product_return, returnId is the auto_increament id in order_product_return
                $result = $this->db->CheckInsert("order_product_return", $product, $where, true);
           
                $this->db->Update("order_product_return", $product, $where, true);
                
                $query = "SELECT * FROM order_product_return WHERE order_id =:order_id AND account_id =:account_id
                    AND supplier_id =:supplier_id AND sku =:sku";

                $return_product = $this->db->FetchOne($query, $where);
                $returnId = $return_product['id'];
            
            if ($result){
                # send notification
                $notification = $this->returnService->create_return_notification($returnId, $_POST["account_id"]);
                $this->notificationService->createNotification($notification);

                # send Email
                $this->returnService->email_store_master($returnId, $_POST["account_id"]);

                JSO(array("success" => true));
                exit;
                }

                JSO(array("Error" => "403 Access Forbidden"));
                throw new \Exception("error create return");
            }

            # fail for accessible
            // http_response_code(403);
            // JSO(array("Error" => "403 Access Forbidden"));
            throw new \Exception("Only Store Owner Can Create Order Return");
        }

        # Order is not completed. It's not allow to create return
        // http_response_code(400);
        /// JSO(array("Error" => "Order It's not Completed"));
        throw new \Exception("You Can't Return UnShipped Order");

    }

    /**
     * Update Order Product Retun Status. Accepted or Declined. Access by master store only
     *
     * @param $id - int - auto increament id in order product return table
     * @return 
     *
    */ 
    public function put($id)
    {   
        parse_str(file_get_contents('php://input'), $_POST);

        # status:  Accepted ||  Declined 
        $statusUpdate = array(
            "status"        => rawurldecode($_POST["status"]),
            // "date_modified" => date("Y-m-d H:i:s")
        );
        $statusUpdate["date_modified"] = date("Y-m-d H:i:s");

        # only master store can update return products status
        if ($this->accessibility == "master"){
            $result = $this->db->Update("order_product_return", $statusUpdate, array("id" => $id));

            if ($result){
                # update notification here
                # get information from this return
                $returnInstance = $this->returnService->getReturnById($id);

                $notificationUpdate = array(
                    "from_user_id" => 1, // [open issue ] - need to get master store user_id
                    "to_user_id"   => $returnInstance["account_id"],
                    "status"       => "Closed"   // Since It's finalized. decision made by master store. Then we close this notification
                );

                # update notification
                $notification_where = array(
                    "entity_id" => $id,
                    "entity_type" => "returns"
                );

                $this->db->Update("notifications", $notificationUpdate, $notification_where);

                $status = $statusUpdate["status"];

                if ($status == "Approved" ||  $status == "Declined") {
                    $this->returnService->email_master_customer($id, $status);
                } else {
                    $this->returnService->email_master_supplier($id);
                }

                // http_response_code(200);
                JSO(array("success" => "Return Product With id $id Status Updated"));
                exit;
            }

            // http_response_code(400);
            // JSO(array("Error" => "Fail For Update Return Product Status" ));
            throw new \Exception("Error Update Return");
        }

        # fail for accessible
        // http_response_code(403);
        // JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

    /**
     * Cancel Return Product. Access by Customer Store only
     *  
     * @param $id - int - auto_increament id in order_product_return tables
     * @return response
    */
    public function delete($id)
    {
        if ($this->accessibility == "customerStore"){
            $result = $this->db->Update("order_product_return", array("status" => "Canceled"), array("id" => $id));

            if ($result){

                # update notification
                $notification_where = array(
                    "entity_id" => $id,
                    "entity_type" => "returns"
                );
                $this->db->Update("notifications", array("status" => "Closed"), $notification_where);

                # send Email
                # code ...
                
                http_response_code(200);
                JSO(array("response" => "Order Product Return With ID: $id Has Been Canceled"));
                exit;
            }

            # Fail for Archived
            // http_response_code(400);
            JSO(array("Error" => "Fail For Cancel Return Product Status" ));
            throw new \Exception("Error Canceled Return");
        }

        # fail for accessible
        // http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

    /**
     * Accessibility Check For Masterstore, CustomerStore, Supplier
     *
     * @param $id - int - auto increament id in reutrns table
     * @param $token - object
     *
     * @return identity - string
    */
    private function checkAccessible($id = null, $token)
    { 
       
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];
        $userId = $token["user_id"];
        

            switch ($roleScope) {
                case "master":
                    return "master";
                case "customerStore":
                    if ($id){
                        $query = "SELECT account_id FROM order_product_return WHERE account_id = $userId AND id = $id";
                        $result = $this->db->FetchOne($query);
                        if (isset($result["account_id"])){
                            return "customerStore";
                        }
                    }else{
                        return "customerStore";
                    }
                    break;
                case "supplier":
                    if ($id){
                        $query = "SELECT supplier_id FROM order_product_return WHERE supplier_id = $userId AND id = $id";
                        $result = $this->db->FetchOne($query);
                        if (isset($result["supplier_id"])){
                            return "supplier";
                        }
                    }else{
                        return "supplier";
                    }
                    break;
        }


        # fail for accessible
        // http_response_code(403);
        // JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

}