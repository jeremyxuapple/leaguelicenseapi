<?php
/**
 * Main Products Controller
 */

namespace LL\controllers\products;

use LL\lib\BCAPI;
use LL\lib\Oauth;
use LL\lib\database;

class productsIdController
{
    /**
     * Construct function that handles product requests with specific ID's
     * Can also accept requests without an ID, but need to return appropriate error messages
     *
     * @param $method - request method, get, post, put etc....
     * @param $id - the specific product ID
     *
     * @return $data - need to return json Data with response; Can use JSO(array());
     *
     */
    public function __construct($method = 'get', $id = null, $extra = null, $token = false)
    {
        // # enable APi service For Customer Store
        // $oauth = new \LL\lib\Oauth\OauthServer();
        // $token = $oauth->verifyToken();
        // $api = new \LL\lib\BCAPI\customerStoreConnector($token);
        // $this->clientapi = $api->getClient();

        # enable APi Service For Master Store
        $api = new \LL\lib\BCAPI\masterConnector();
        $this->api = $api->getClient();

        # define service instance
        $this->db  = new \LL\lib\database\mysql();
        $this->productService  = new \LL\services\productService();
        $this->supplierService = new \LL\services\supplierService();
        
        
        # sanity check
        if (!isset($method) || $method == "" || !method_exists($this, $method))
            throw new \Exception("Invalid method");

        # accessibility check
        $this->accessibility = $this->checkAccessible($token);

        if ($this->accessibility){
            # check product existence for choosing which method to call
            if ($method == "post" || $method == "put"){
                
                # customer store not allow to post or put 
                if ($this->accessibility == "customerStore"){
                    throw new \Exception("Unable To Identify, Access Forbidden");
                }
                # master store and supplier allow to post and put
                else{

                    # either POST or PUT, parse data into _POST.
                    parse_str(file_get_contents('php://input'), $_POST);
                    $productId = $this->checkProductExists($id);

                    if (isset($productId)){
                        #update product
                        $this->put($productId);
                    }else{
                        #Create a new product
                        $this->post();
                    }
                }
            }else{
                # this will handle Get and Delect Method
                $this->$method($id, $extra, $token);
            }
        }else{
            throw new \Exception("Unable To Identify, Access Forbidden");
        }
    }

    /**
     * GET Product or GET Products
     *
     * @param $id or null
     *
     * @return $data - jason Data with response
     *
    */
    private function get($id = null, $extra = null, $token = null)
    {
        # define GET options, for pagination, query term.
        $options = array(
            "limit"        => isset($_REQUEST["limit"]) ? $_REQUEST["limit"] : 10,
            "page"         => (isset($_GET["page"]) && $_GET["page"] >= 1) ? $_GET["page"] - 1 : 0,
            "order_option" => isset($_GET["order_option"]) ? $_GET["order_option"] : 'date_modified',
            "order_sort"   => isset($_GET["order_sort"]) ? $_GET["order_sort"] : 'DESC',
            "where_query"  => isset($_GET["query"]) ? $_GET["query"] : "1 = 1"
        );
        $options["offset"] = $options["limit"] * $options["page"];
        extract($options); 
        $data = array();

        if (isset($id))
        {
            switch ($extra) {
                case "supplier" :
                    $suppliers = $this->supplierService->getSuppliersByProductId($id, $options);
                    $data["suppliers"] = $suppliers; 
                    $max = sizeof($suppliers);
                    break;
                # get all information of a product by productId
                default:
                    $product = $this->productService->getProductById($id);
                    if ($product){
                        $data["product"] = $product;
                        http_response_code(200);
                        JSO($data);
                        exit;
                    }
                    break;
            }
        }
        # get product list, access by all roles
        else{
            #GET the most updated Products
            $products = $this->productService->getProducts($options);
            if (isset($products)){
                $data["products"] = $products;
                $count = "SELECT count(*) AS count FROM products";
                $max = $this->db->FetchOne($count);
            }
        }
          # define basic data array for feedback
            $data["page"]          = $page + 1;
            $data["limit"]         = $limit;
            $data["count"]         = $max["count"];
            $data["maxpages"]      = ceil($max["count"] / $limit);
            $data["order_option"]  = $order_option;
            $data["order_sort"]    = $order_sort;

        // if (sizeof($data["products"]) > 1 || sizeof($data["suppliers"]) > 1){
        //     http_response_code(200);
            
        // }

        JSO($data);
        exit;

        # no data found
        // http_response_code(404);
        // JSO(array("Error" => "There has no product been added to the store yet."));
        // exit;
    }

    /**
     * Create a new Product on both Local and Master store
     * productId on the local store is associtate with id in the products tbale of master store
     *
     * @return $response - status
     *
    */
    private function post()   // * we disable the post method for now, we only allow product create on master store admin panel
    {
        // # create basic product object array
        // $product = $this->productService->generateProductInstance();

        // # check the request comes from master store or supplier, identity comes from Oauth
        // if ($this->accessibility == "supplier")
        // {
        //     # product created by supplier
        //     $supplierId = $token["user_id"];

        //     # define product object array for supplier_product relational table
        //     $supplierProduct = array(
        //         "supplier_id"             => $supplierId,
        //         "price"                   => _POST["price"],
        //         "sku"                     => _POST["sku"],
        //         "inventory_level"         => _POST["inventory_level"],
        //         "inventory_warning_level" => _POST["inventory_warning_level"]
        //     );

        //     # set products field default value by Supplier ONLY
        //     $product["categories"] = [1];
        // }

        // # set products field default value general case
        // $product["availability"] = "disabled";
        // $product["date_created"] = date(DATE_RFC1123);
        // $product["date_modified"] = date(DATE_RFC1123);

        // # insert into masterStore and get the associate productId from master store
        // $productId = $this->api->createProduct($product);

        // if ($productId){
        //     $product["product_id"] = $productId;
        //     $product["manufactory_sku"] = _POST["manufactory_sku"];
        
        //     # insert to local DB
        //     $this->db->Insert("products", $product);

        //     if ($supplierId){

        //         # insert into supplier_product table
        //         $supplierProduct["product_id"] = $productId;
        //         $this->db->CheckInsert(
        //             "supplier_product", 
        //             $supplierProduct, 
        //             array("product_id" => $productId, "supplier_id" => $supplierId)
        //         );
        //     } 
        // }else{
        //     throw new \Exception("Errot Insertion to Master Store");
        // }
    }

    /**
     * Update a Product
     *
     * @param int - $productId
     *
     * @return $response - status
     *
    */
    private function put($productId, $extra = null, $token = null)
    {
        # define product basic info for update
        $productBase = array(
            "sku" => _POST["sku"],
            "inventory_level" => _POST["inventory_level"],
            "inventory_warning_level" => _POST["inventory_warning_level"]
        );

        # check put request comes from supplier or master store
        if ($this->accessibility == "supplier"){

            # product updated by supplier
            $supplierId = $token["user_id"];
            $supplierProduct = $this->productService->getSupplierProductByIds($productId, $supplierId);

            # check if price changed
            if ($supplierProduct["price"] !== _POST["price"]){
                $productSupplement = array(
                    "price_future"        =>  _POST["price"],
                    "price_date_modified" => date(DATE_RFC1123)
                );

                $product = array_merge($productBase, $productSupplement);

            }else{
                $product = $productBase;
            }

            $result = $this->db->Update("supplier_product", $product, array("product_id" => $productId, "supplier_id" => $supplierId));

            # recalculate and update inventory of this product
            $updatedProduct = $this->updateProductInventory($productId);

        }         
        # if product update comes from master store
        else {

            # create basic product object array
            $product = $this->productService->generateProductInstance();
            $product["date_modified"] = date(DATE_RFC1123);

            # update data on both DBs directly
            $result = $this->db->Update("products", $product, array("product_id" => $productId));
            $updatedProduct = $this->api->updateProduct($productId, $product);
        }
    }

    /**
     * Handle Delete Product For Master Store And Customer Store
     *
     * @param interger - product id 
     * 
     * @return $mixed
     *
    */
    private function delete($id, $extra = null, $token = null)
    {
        # delete method called only by master store.
        if($this->accessibility == "master"){
            if (isset($id)){
                # we do not actually delete any records, when delete call, set its availability to be disabled
                $deleteProduct = array("availability" => "disabled");
                $this->db->Update("products", $deleteProduct, array("product_id" => $id));
                $this->api->updateProduct($id, $deleteProduct);

                // * response...
            }
        # Delete Product For Customer Store
        }elseif ($this->accessibility == "customerStore") {
            if ($id){
                # get customer store api information
                $apiConnector = new \LL\lib\BCAPI\customerStoreConnector($token);
                $this->clientapi = $apiConnector->getClient();

                # Remove product from customer store
                # We actually Delete the relationship here. API DB account_product
                $accountId = $token["user_id"];
                //$deleteDBResult = $this->db->Delete("account_product", array("master_product_id" => $id, "account_id" => $accountId));

                # Remove product from Customer Store
                $customerProductIdQuery = "
                    SELECT customer_product_id 
                    FROM account_product 
                    WHERE account_id = $accountId AND master_product_id = $id
                ";
                $customerProductIdQueryResult = $this->db->FetchOne($customerProductIdQuery);
                $customerProductId = $customerProductIdQueryResult["customer_product_id"];
                $deleteProduct = $this->clientapi->deleteProduct($customerProductId);

                //$error = $this->clientapi->getLastError();

                if ($deleteDBResult /*&& $deleteProduct*/){
                    //http_response_code(202);
                    JSO(array("success" => "Product With Id $id Has Been Remove From Your Store"));
                    exit;
                }
                //http_response_code(404);
                throw new \Exception("Your Store Does Not Have This Product");
            }
            //http_response_code(400);
            throw new \Exception("400 Bad Request, Need Id For DELETE Operation");
            
        }else{
            throw new \Exception("Unable To Identify, Access Forbidden"); 
        }
    }


    private function checkAccessible($token)
    {
        # identify who use this token
        $roleScope = explode("_", $token["scope"])[0];

        switch ($roleScope) {
            case "master":
                return "master";
            case "supplier":
                return "supplier";
            case "customerStore":
                return "customerStore";
        }

        # fail for accessible check 
        // http_response_code(403);
        JSO(array("Error" => "403 Access Forbidden"));
        throw new \Exception("Unable To Identify, Access Forbidden");
    }

}
