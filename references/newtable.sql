-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 22, 2017 at 02:01 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `league_licence`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `platform` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tax_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_markup` int(11) DEFAULT NULL,
  `shipping_markup` int(11) DEFAULT NULL,
  `certificate_path` text CHARACTER SET utf8,
  `billing_first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_address_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_address_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_zip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active_subscription_level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `life_time_orders` int(11) DEFAULT '0',
  `life_time_sales` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `account_id`, `token`, `code`, `platform`, `name`, `contact_first_name`, `contact_last_name`, `contact_email`, `contact_phone`, `tax_id`, `currency`, `product_markup`, `shipping_markup`, `certificate_path`, `billing_first_name`, `billing_last_name`, `billing_company`, `billing_phone`, `billing_address_1`, `billing_address_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `status`, `active_subscription_level`, `life_time_orders`, `life_time_sales`, `date_created`, `date_modified`, `staff_notes`) VALUES
(1, 1, 'token12345', 'code12345', 'BC', 'account_num_1', 'first name 1', 'last name 1', 'email@email.com', '1234567890', '123456', 'USD', 7, 4, 'rfrewfrewffgtrwegtwrefrewf', 'Koha', 'Maker', 'Be a part of ', NULL, '9890', 'Nba ', 'Assocation', 'ON', '23224', 'CA', 'Active', 'playoff', 0, 0, '2017-05-26 00:00:00', '2017-07-05 00:00:00', ''),
(2, 2, 'token2222222', 'code 222222', 'SP', 'fefefr', 'afdytrjyutr', 'ghfjhgf', 'nhhgfnhgf@dfsa.com', '6543124536', '143214', 'usd', 2, 3, NULL, 'name', 'is', 'unknown', NULL, 'it', 'a ', 'secret', 'on', '443232', 'CA', 'Active', 'contender', 0, 0, '2017-05-24 00:00:00', '2017-07-26 00:00:00', ''),
(3, 3, 'dksgjfdklsgre', 'grgrtged', 'SP', 'account3', 'test', 'WHY', 'do ', 'you ', 'care', 'USD', 2, 3, NULL, 'care', 'about ', 'the ', NULL, 'company', 'last', 'name', 'on', '12314', 'CA', 'Active', 'champion', 0, 0, '2017-06-25 00:00:00', '2017-07-13 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `account_invoice`
--

CREATE TABLE `account_invoice` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `base_shipping_cost` float NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `base_cost_price` float DEFAULT NULL,
  `base_handling_cost` int(11) NOT NULL,
  `price_inc_tax` float NOT NULL,
  `price_ex_tax` float NOT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_invoice`
--

INSERT INTO `account_invoice` (`id`, `order_id`, `supplier_id`, `account_id`, `product_id`, `sku`, `base_shipping_cost`, `quantity`, `base_cost_price`, `base_handling_cost`, `price_inc_tax`, `price_ex_tax`, `tracking_number`, `date_created`, `date_modified`) VALUES
(1, 1, 1, 1, 3, '13034457', 3, 12, 23.99, 4, 56.6, 50, 'CA105549878', '2017-07-05 00:00:00', '2017-07-14 00:00:00'),
(2, 1, 1, 1, 13, '130455789', 3, 12, 21.88, 1, 98.9, 95, 'CA6544789542', '2017-07-10 00:00:00', '2017-07-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `account_product`
--

CREATE TABLE `account_product` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `master_product_id` int(11) NOT NULL,
  `customer_product_id` int(11) NOT NULL,
  `sku` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_product`
--

INSERT INTO `account_product` (`id`, `account_id`, `master_product_id`, `customer_product_id`, `sku`) VALUES
(1, 793723, 94, 133, 'OCG'),
(2, 793723, 104, 134, 'OFSUC'),
(3, 793723, 88, 135, 'CC3C'),
(4, 793723, 80, 136, 'OTL');

-- --------------------------------------------------------

--
-- Table structure for table `management_settings`
--

CREATE TABLE `management_settings` (
  `id` int(11) NOT NULL,
  `shipping_markup` float DEFAULT NULL,
  `inventory_subtraction_value` float DEFAULT NULL,
  `handling_markup` float NOT NULL,
  `handling_ex_tax` float NOT NULL,
  `handling_inc_tax` float NOT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `management_settings`
--

INSERT INTO `management_settings` (`id`, `shipping_markup`, `inventory_subtraction_value`, `handling_markup`, `handling_ex_tax`, `handling_inc_tax`, `date_modified`) VALUES
(1, 6, 5, 3, 3, 3.2, '2017-07-19 16:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `msg_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `entity_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `entity_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `from_user_id`, `to_user_id`, `status`, `msg_type`, `entity_id`, `entity_type`, `entity_name`, `details`, `date_created`, `date_modified`) VALUES
(1, 1, NULL, 'Active', 'Map', 1, 'account', 'John NFL ', 'Map Violation', '2017-06-29 00:00:00', '2017-07-07 00:00:00'),
(2, 1, NULL, 'active', 'Payment', 1, 'account', 'Tony NFL', 'Monthly Payment', '2017-06-29 00:00:00', '2017-06-29 00:00:00'),
(3, 1, NULL, 'active', 'Payment', 1, 'account', 'NFL Super Store', 'Payment Declined', '2017-06-29 00:00:00', '2017-06-29 00:00:00'),
(4, 1, NULL, 'active', 'lower stock', 1, 'product', 'NG schoolbag', 'Needs to be refilled.', '2017-06-29 00:00:00', '2017-06-29 00:00:00'),
(5, 1, NULL, 'active', 'lower stock', 1, 'product', 'American Apple', 'Needs to be refilled.', '2017-06-29 00:00:00', '2017-06-29 00:00:00'),
(6, 793723, 1, 'Open', 'Return', 37, 'returns', NULL, 'A new return record has been created', '2017-07-13 20:20:40', '2017-07-13 20:20:40'),
(8, 793723, 1, 'Open', 'Return', 39, 'returns', NULL, 'A new return record has been created', '2017-07-13 23:41:38', '2017-07-13 23:41:38'),
(9, 793723, 1, 'Open', 'Return', 40, 'returns', NULL, 'A new return record has been created', '2017-07-14 00:10:41', '2017-07-14 00:10:41'),
(10, 0, 793723, 'Open', 'Order Fulfillment', 104, 'Orders', 'Your Store', 'Order (Id: 104) Has Fulfillment. Sent to Supplier', '2017-07-14 14:11:50', '2017-07-14 14:11:50'),
(11, 0, 1, 'Open', 'Order Fulfillment', 104, 'Orders', NULL, 'Order (Id: 104) Has Fulfillment. Sent to Supplier', '2017-07-14 14:11:50', '2017-07-14 14:11:50'),
(12, 793723, 1, 'Open', 'Return', 41, 'returns', NULL, 'Store() is request a return request on the Order Id of  for product()', '2017-07-14 14:14:11', '2017-07-14 14:14:11'),
(13, 0, 793723, 'Open', 'Order Fulfillment', 105, 'Orders', 'Your Store', 'Order (Id: 105) Has Fulfillment. Sent to Supplier', '2017-07-14 15:03:00', '2017-07-14 15:03:00'),
(14, 0, 1, 'Open', 'Order Fulfillment', 105, 'Orders', NULL, 'Order (Id: 105) Has Fulfillment. Sent to Supplier', '2017-07-14 15:03:00', '2017-07-14 15:03:00'),
(15, 0, 793723, 'Open', 'Order Fulfillment', 109, 'Orders', 'Your Store', 'New Order (Id: 109) Has Been Created. Waitting For Fulfillment', '2017-07-19 17:04:30', '2017-07-19 17:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('336d0a3f638daa9c594d638ada277f3143c5a2d7', 'test-customerStore', '10001', '2019-06-13 18:42:35', 'customerStore_payoff'),
('37f19d8fce1ffbf32d2ec3709ea19e82927e1fbb', 'test-one', '10000', '2017-06-27 18:51:08', 'master'),
('52970b7e71e0eff6f911505364db9ed6f92c88b2', 'test-one', '10000', '2019-06-13 18:42:26', 'master'),
('c69c763ce88283e4d6e2ea6836a4d09ea2a44a01', 'test-supplier', '10002', '2019-06-13 15:43:43', 'supplier'),
('e83911b0b4c84b78c0ee327c8b5df15f26aaa624', 'pbbto38xen', '1', '2018-11-15 21:41:12', 'master');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(100) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('super-master', 'W12IngJM0NCW4cHz3cFPH6k2aS', 'http://leaguelicence.dev', 'client_credentials', 'master', '1'),
('test-customerStore', 'klISF9fRM0K1Sycmbs2hbsDe0S', 'http://leaguelicence.dev', 'client_credentials', 'customerStore_payoff', '10001'),
('test-one', 'TWJBo1DI4uMF6mhB2fWLfEUQGB', 'http://leaguelicence.dev', 'client_credentials', 'master', '10000'),
('test-supplier', 'aGF4qmmrhgl0TiPXh1GbH5pVe5', 'http://leaguelicence.dev', 'client_credentials', 'supplier', '10002');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` text,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_users`
--

CREATE TABLE `oauth_users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(2000) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `origin_store_status` varchar(255) DEFAULT NULL,
  `origin_store_status_id` int(11) DEFAULT NULL,
  `subtotal_ex_tax` float DEFAULT NULL,
  `subtotal_inc_tax` float DEFAULT NULL,
  `subtotal_tax` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `profit` float DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_provider_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `refunded_amount` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `currency_exchange_rate` float DEFAULT NULL,
  `default_currency_id` int(11) DEFAULT NULL,
  `default_currency_code` varchar(255) DEFAULT NULL,
  `discount_amount` int(11) DEFAULT NULL,
  `coupon_discount` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `items_total` int(11) DEFAULT NULL,
  `items_shipped` int(11) DEFAULT NULL,
  `is_deleted` varchar(255) DEFAULT NULL,
  `geoip_country` varchar(255) DEFAULT NULL,
  `geoip_country_iso2` varchar(255) DEFAULT NULL,
  `staff_notes` text,
  `customer_message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `account_id`, `customer_name`, `status`, `origin_store_status`, `origin_store_status_id`, `subtotal_ex_tax`, `subtotal_inc_tax`, `subtotal_tax`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `profit`, `payment_method`, `payment_provider_id`, `payment_status`, `refunded_amount`, `currency_id`, `currency_code`, `currency_exchange_rate`, `default_currency_id`, `default_currency_code`, `discount_amount`, `coupon_discount`, `date_created`, `date_modified`, `items_total`, `items_shipped`, `is_deleted`, `geoip_country`, `geoip_country_iso2`, `staff_notes`, `customer_message`) VALUES
(3, 101, 1, ' ', 'Awaiting Fulfillment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-04 17:12:25', '2017-07-20 12:31:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 110, 1, ' ', 'Awaiting Fulfillment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-05 12:19:08', '2017-07-17 09:30:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 109, 1, 'Robin QC Testing', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 74, 74, 0, 74, 74, 0, NULL, 'test', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-05 11:38:21', NULL, 2, 0, '', 'Canada', 'CA', '', ''),
(6, 108, 1, ' ', 'Awaiting Fulfillment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-04 14:28:02', '2017-07-19 16:12:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 107, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 2556.5, 2556.5, 0, 2556.5, 2556.5, 0, NULL, 'manual payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:10', NULL, 24, 0, '', 'Canada', 'CA', 'staff notes', 'customer notes'),
(8, 106, 1, 'michael mao', 'Awaiting Fulfillment', 'Pending', 1, 321.75, 321.75, 0, 321.75, 321.75, 0, NULL, 'test', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:16', NULL, 26, 0, '', 'Canada', 'CA', 'new msg', 'new msg'),
(9, 105, 1, 'michael mao', 'Completed', 'Completed', 11, 4298.85, 4298.85, 0, 4298.85, 4298.85, 0, NULL, 'manual payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:22', NULL, 123, 0, '', 'Canada', 'CA', '', ''),
(10, 104, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 50575, 50575, 0, 50575, 50575, 0, NULL, 'manul', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:29', NULL, 1033, 0, '', 'Canada', 'CA', 'yo man', 'hey man'),
(11, 102, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 260, 260, 0, 260, 260, 0, NULL, '', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:42', NULL, 6, 0, '', 'Canada', 'CA', 'send michael mao something', 'michael mao needs something'),
(12, 100, 1, 'Robin QC Testing', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 97.95, 97.95, 0, 97.95, 97.95, 0, NULL, 'test payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-05 14:13:57', NULL, 4, 0, '', 'Canada', 'CA', 'staff notes here', 'order comments'),
(13, 111, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 182.95, 182.95, 0, 182.95, 182.95, 0, NULL, 'this is manual', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 16:07:50', NULL, 4, 0, '', 'Canada', 'CA', 'this is the staff notes ', 'this is the customer msg'),
(14, 112, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 34.95, 34.95, 0, 34.95, 34.95, 0, NULL, 'testing', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-07 15:18:52', '2017-07-11 13:02:28', 1, 0, '', 'Canada', 'CA', '', ''),
(21, 102, 793723, 'Robin Testing', 'Completed', 'Completed', 10, 45.95, 45.95, 0, 45.95, 45.95, 0, NULL, 'Test Payment Gateway', '', 'captured', 0, 1, 'CAD', 1, 1, 'CAD', 0, 0, '2017-07-13 20:02:23', '2017-07-18 18:31:32', 1, 0, '', 'Canada', 'CA', '', ''),
(22, 103, 793723, 'Robin Testing', 'Awaiting Fulfillment', 'Completed', 10, 45.95, 45.95, 0, 45.95, 45.95, 0, NULL, 'Test Payment Gateway', '', 'captured', 0, 1, 'CAD', 1, 1, 'CAD', 0, 0, '2017-07-13 23:22:41', '2017-07-18 18:31:29', 1, 0, '', 'Canada', 'CA', '', ''),
(23, 104, 793723, 'Robin Testing', 'Awaiting Fulfillment', 'Completed', 10, 45.95, 45.95, 0, 45.95, 45.95, 0, NULL, 'Manual', NULL, '', 0, 1, 'CAD', 1, 1, 'CAD', 0, 0, '2017-07-14 14:11:24', '2017-07-18 18:46:15', 1, 0, '', 'Canada', 'CA', '', ''),
(24, 105, 793723, 'Robin Testing', 'Awaiting Fulfillment', 'Completed', 10, 193.45, 193.45, 0, 193.45, 193.45, 0, NULL, 'test', NULL, '', 0, 1, 'CAD', 1, 1, 'CAD', 0, 0, '2017-07-14 15:02:43', '2017-07-19 10:16:36', 3, 0, '', 'Canada', 'CA', '', ''),
(25, 109, 793723, 'Robin Testing', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 1272.45, 1272.45, 0, 1272.45, 1272.45, 0, NULL, '123', NULL, '', 0, 1, 'CAD', 1, 1, 'CAD', 0, 0, '2017-07-19 17:04:30', '2017-07-20 10:17:23', 26, 0, '', 'Canada', 'CA', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_billing`
--

CREATE TABLE `order_billing` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `street_1` varchar(255) DEFAULT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_iso2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_billing`
--

INSERT INTO `order_billing` (`id`, `order_id`, `account_id`, `first_name`, `last_name`, `email`, `phone`, `company`, `street_1`, `street_2`, `zip`, `city`, `state`, `country`, `country_iso2`) VALUES
(3, 101, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 110, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 109, 1, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(6, 108, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 107, 1, 'michaelmao1233', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(8, 106, 1, 'michaeleeee', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(9, 105, 1, 'michaelmao', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(10, 104, 1, 'michael', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(11, 102, 1, 'michael', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(12, 100, 1, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(13, 111, 1, 'michaeleeee', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(14, 112, 1, 'michaeleeee', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(15, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(16, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(17, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(18, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(19, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(20, 101, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(21, 102, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(22, 103, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(23, 104, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(24, 105, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(25, 109, 793723, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'City', 'New York', 'United States', 'US');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `account_product_ref_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `master_product_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `base_price` float DEFAULT NULL,
  `base_cost_price` int(11) DEFAULT NULL,
  `price_ex_tax` float DEFAULT NULL,
  `price_inc_tax` float DEFAULT NULL,
  `price_tax` float DEFAULT NULL,
  `base_total` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `fulfillment` varchar(255) DEFAULT NULL,
  `cost_price_inc_tax` float DEFAULT NULL,
  `cost_price_ex_tax` float DEFAULT NULL,
  `cost_price_tax` float DEFAULT NULL,
  `is_refunded` varchar(255) DEFAULT NULL,
  `quantity_refunded` int(11) DEFAULT NULL,
  `refund_amount` float DEFAULT NULL,
  `quantity_shipped` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `account_product_ref_id`, `order_id`, `shipping_id`, `supplier_id`, `account_id`, `product_id`, `master_product_id`, `return_id`, `name`, `sku`, `type`, `base_price`, `base_cost_price`, `price_ex_tax`, `price_inc_tax`, `price_tax`, `base_total`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `weight`, `quantity`, `fulfillment`, `cost_price_inc_tax`, `cost_price_ex_tax`, `cost_price_tax`, `is_refunded`, `quantity_refunded`, `refund_amount`, `quantity_shipped`) VALUES
(31, NULL, 108, 11, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'FCS11452410', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(32, NULL, 107, 9, NULL, 1, 80, NULL, 0, '[Sample] Orbit Terrarium - Large', 'OTL', 'physical', 109, 0, 109, 109, 0, 2507, 2507, 2507, 0, 1, 23, 'External Product', 0, 0, 0, '', 0, 0, 0),
(33, NULL, 107, 10, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 49.5, 49.5, 49.5, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(34, NULL, 106, 8, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 174.75, 174.75, 174.75, 0, 1, 5, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(35, NULL, 106, 8, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-0BAF36BA', 'physical', 7, 0, 7, 7, 0, 147, 147, 147, 0, 1, 21, 'External Product', 0, 0, 0, '', 0, 0, 0),
(36, NULL, 105, 7, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 4298.85, 4298.85, 4298.85, 0, 1, 123, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(37, NULL, 104, 6, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-D334BA28', 'physical', 7, 0, 7, 7, 0, 7, 7, 7, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(38, NULL, 104, 6, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 50568, 50568, 50568, 0, 1, 1032, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(39, NULL, 102, 4, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-20D88EFC', 'physical', 7, 0, 7, 7, 0, 35, 35, 35, 0, 1, 5, 'External Product', 0, 0, 0, '', 0, 0, 0),
(40, NULL, 102, 4, 1, 1, 86, 4, 0, '[Sample] Able Brewing System', 'ABS', 'physical', 225, 0, 225, 225, 0, 225, 225, 225, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(50, NULL, 111, 14, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(51, NULL, 111, 14, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 99, 99, 99, 0, 1, 2, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(52, NULL, 111, 15, NULL, 1, 107, NULL, 0, '[Sample] Dustpan & Brush', 'DPB', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(84, NULL, 109, 12, NULL, 1, 111, NULL, 0, '[Sample] Smith Journal 13', 'SM13', 'physical', 25, 0, 25, 25, 0, 25, 25, 25, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(85, NULL, 109, 12, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(94, NULL, 110, 13, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(95, NULL, 110, 13, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-CD28D4F0', 'physical', 7, 0, 7, 7, 0, 7, 7, 7, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(111, NULL, 100, 1, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-0BAF36BA', 'physical', 7, 0, 7, 7, 0, 14, 14, 14, 0, 1, 2, 'External Product', 0, 0, 0, '', 0, 0, 0),
(112, NULL, 100, 1, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(113, NULL, 100, 1, NULL, 1, 77, NULL, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-CCAEAE80', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(159, NULL, 101, 2, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 49.5, 49.5, 49.5, 0, 1, 1, 'Internal', 0, 0, 0, '', 0, 0, 0),
(160, NULL, 101, 3, NULL, 1, 77, NULL, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-B6062BD4', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(161, NULL, 101, 2, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-973630F5', 'physical', 7, 0, 7, 7, 0, 7, 7, 7, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(162, NULL, 112, 16, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(181, 3, 102, 3, 1, 793723, 122, 104, 0, '[Sample] Utility Caddy', 'OFSUC', 'physical', 45.95, 0, 45.95, 45.95, 0, 45.95, 45.95, 45.95, 0, 1, 1, 'Seller Licence', 0, 0, 0, '', 0, 0, 0),
(182, NULL, 103, 4, 1, 793723, 127, 104, 0, '[Sample] Utility Caddy', 'OFSUC', 'physical', 45.95, 0, 45.95, 45.95, 0, 45.95, 45.95, 45.95, 0, 1, 1, 'Seller Licence', 0, 0, 0, '', 0, 0, 0),
(183, NULL, 104, 5, 1, 793723, 131, 104, 0, '[Sample] Utility Caddy', 'OFSUC', 'physical', 45.95, 0, 45.95, 45.95, 0, 45.95, 45.95, 45.95, 0, 1, 1, 'Seller Licence', 0, 0, 0, '', 0, 0, 0),
(184, NULL, 105, 6, 1, 793723, 135, 88, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 49.5, 49.5, 49.5, 0, 1, 1, 'Seller Licence', 0, 0, 0, '', 0, 0, 0),
(185, NULL, 105, 6, 1, 793723, 133, 94, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'Seller Licence', 0, 0, 0, '', 0, 0, 0),
(186, NULL, 105, 6, NULL, 793723, 136, NULL, 0, '[Sample] Orbit Terrarium - Large', 'OTL', 'physical', 109, 0, 109, 109, 0, 109, 109, 109, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(187, 14, 109, 10, 1, 793723, 135, 88, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 1237.5, 1237.5, 1237.5, 0, 1, 25, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(188, 15, 109, 10, 1, 793723, 133, 94, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_invoice`
--

CREATE TABLE `order_product_invoice` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `base_cost_price` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `shipping_cost_ex_tax` float DEFAULT NULL,
  `shipping_cost_inc_tax` float DEFAULT NULL,
  `shipping_cost_tax` float DEFAULT NULL,
  `shipping_method` varchar(255) DEFAULT NULL,
  `shipping_provider` varchar(255) DEFAULT NULL,
  `tracking_carrier` varchar(255) DEFAULT NULL,
  `date_shipped` datetime DEFAULT NULL,
  `profit` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product_invoice`
--

INSERT INTO `order_product_invoice` (`id`, `order_id`, `supplier_id`, `account_id`, `product_id`, `sku`, `quantity`, `customer_name`, `base_cost_price`, `selling_price`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `tracking_number`, `shipping_cost_ex_tax`, `shipping_cost_inc_tax`, `shipping_cost_tax`, `shipping_method`, `shipping_provider`, `tracking_carrier`, `date_shipped`, `profit`, `status`, `create_date`, `update_date`) VALUES
(1, 1, 1, 1, 1, 'fwfrew', 12, 'christ', NULL, NULL, 1233.63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12331200, NULL, NULL, NULL),
(2, 1, 1, 1, 13, 'frefrew', NULL, NULL, NULL, NULL, 432424, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 342.3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_return`
--

CREATE TABLE `order_product_return` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product_return`
--

INSERT INTO `order_product_return` (`id`, `order_id`, `shipping_id`, `account_id`, `supplier_id`, `sku`, `name`, `quantity`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `reason`, `type`, `status`, `date_created`, `date_modified`) VALUES
(12, 108, NULL, 1, 1, '120457785', 'NFL baseball', 7, 78.88, 90.99, 12.12, 'shippment damage', 'exchange', 'declined', '2017-07-10 14:14:27', '2017-07-11 15:35:37'),
(36, 108, NULL, 1, 1, '120457785', 'NBA baseketball', 7, 78.89, 90.89, 12.13, 'shippment damage', 'exchange', 'declined', '2017-07-10 16:16:07', '2017-07-11 15:35:37'),
(37, 102, NULL, 793723, 1, 'OFSUC', '%5BSample%5D%20Utility%20Caddy', 1, 45.95, 45.95, 0, NULL, 'physical', 'Open', '2017-07-13 20:16:12', '2017-07-13 20:16:12'),
(40, 103, 4, 793723, 1, 'OFSUC', '[Sample] Utility Caddy', 1, 45.95, 45.95, 0, 'Defective', 'Partial Refund', 'Open', '2017-07-14 00:10:41', '2017-07-14 00:10:41'),
(41, 104, 5, 793723, 1, 'OFSUC', '[Sample] Utility Caddy', 1, 45.95, 45.95, 0, 'Defective', 'Partial Refund', 'Open', '2017-07-14 14:14:11', '2017-07-14 14:14:11');

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping`
--

CREATE TABLE `order_shipping` (
  `id` int(11) NOT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `street_1` varchar(255) DEFAULT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_iso2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_shipping`
--

INSERT INTO `order_shipping` (`id`, `shipping_id`, `order_id`, `account_id`, `first_name`, `last_name`, `company`, `email`, `phone`, `street_1`, `street_2`, `zip`, `city`, `state`, `country`, `country_iso2`) VALUES
(25, 11, 108, 1, 'test_first_namemaomao', 'test_last_name', 'test_company', 'testing@beapartof.com', '5555555555', '300 yorkland', '300 yorkland', '12300', 'toronto', 'Hawaii', 'United States', 'US'),
(26, 9, 107, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(27, 10, 107, 1, 'michaelmaomao', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(28, 8, 106, 1, 'michaelmaomao', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(29, 7, 105, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(30, 6, 104, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(31, 4, 102, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(37, 14, 111, 1, 'michaeleeee', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(38, 15, 111, 1, 'michaelmao1233', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(58, 12, 109, 1, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(63, 13, 110, 1, 'michaelmao1233', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(69, 1, 100, 1, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(110, 16, 112, 1, 'michaeleeee', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(111, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(112, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(113, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(114, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(115, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(116, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(117, 2, 101, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(118, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(119, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(120, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(121, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(122, 2, 101, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(123, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(124, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(125, 2, 101, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(126, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(127, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(128, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(129, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(130, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(131, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(132, 2, 101, 793723, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(133, 3, 101, 793723, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(134, 2, 101, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(135, 2, 101, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(136, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(137, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(138, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(139, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(140, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(141, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(142, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(143, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(144, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(145, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(146, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(147, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(148, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(149, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(150, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(151, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(152, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(153, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(154, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(155, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(156, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(157, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(158, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(159, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(160, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(161, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(162, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(163, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(164, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(165, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(166, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(167, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(168, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(169, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(170, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(171, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(172, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(173, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(174, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(175, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(176, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(177, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(178, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(179, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(180, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(181, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(182, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(183, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(184, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(185, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(186, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(187, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(188, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(189, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(190, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(191, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(192, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(193, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(194, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(195, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(196, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(197, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(198, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(199, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(200, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(201, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(202, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(203, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(204, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(205, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(206, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(207, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(208, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(209, 4, 103, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(210, 3, 102, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(211, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(212, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(213, 5, 104, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(214, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(215, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(216, 6, 105, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US'),
(217, 10, 109, 793723, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'City', 'New York', 'United States', 'US');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `manufactory_sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `description` text,
  `search_keywords` text,
  `availability_description` text,
  `price` float DEFAULT NULL,
  `cost_price` float DEFAULT NULL,
  `retail_price` float DEFAULT NULL,
  `sale_price` float DEFAULT NULL,
  `calculated_price` float DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT NULL,
  `related_products` varchar(255) DEFAULT NULL,
  `inventory_level` int(11) DEFAULT NULL,
  `inventory_warning_level` int(11) DEFAULT NULL,
  `warranty` text,
  `weight` float DEFAULT NULL,
  `width` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `depth` float DEFAULT NULL,
  `fixed_cost_shipping_price` float DEFAULT NULL,
  `is_free_shipping` tinyint(1) DEFAULT NULL,
  `inventory_tracking` varchar(255) DEFAULT NULL,
  `rating_total` int(11) DEFAULT NULL,
  `rating_count` int(11) DEFAULT NULL,
  `total_sold` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `layout_file` varchar(255) DEFAULT NULL,
  `is_price_hidden` tinyint(1) DEFAULT NULL,
  `price_hidden_label` varchar(255) DEFAULT NULL,
  `categories` text,
  `event_date_field_name` varchar(255) DEFAULT NULL,
  `event_date_type` varchar(255) DEFAULT NULL,
  `event_date_start` datetime DEFAULT NULL,
  `event_date_end` datetime DEFAULT NULL,
  `myob_asset_account` varchar(255) DEFAULT NULL,
  `myob_income_account` varchar(255) DEFAULT NULL,
  `myob_expense_account` varchar(255) DEFAULT NULL,
  `peachtree_gl_account` varchar(255) DEFAULT NULL,
  `condition` varchar(255) DEFAULT NULL,
  `is_condition_shown` tinyint(1) DEFAULT NULL,
  `preorder_release_date` datetime DEFAULT NULL,
  `is_preorder_only` tinyint(1) DEFAULT NULL,
  `preorder_message` varchar(255) DEFAULT NULL,
  `order_quantity_minimum` int(11) DEFAULT NULL,
  `order_quantity_maximum` int(11) DEFAULT NULL,
  `open_graph_type` varchar(255) DEFAULT NULL,
  `open_graph_title` varchar(255) DEFAULT NULL,
  `open_graph_description` text,
  `is_open_graph_thumbnail` tinyint(1) DEFAULT NULL,
  `upc` varchar(255) DEFAULT NULL,
  `avalara_product_tax_code` varchar(255) DEFAULT NULL,
  `date_last_imported` datetime DEFAULT NULL,
  `option_set_id` int(11) DEFAULT NULL,
  `tax_class_id` int(11) DEFAULT NULL,
  `option_set_display` varchar(255) DEFAULT NULL,
  `bin_picking_number` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_id`, `manufactory_sku`, `name`, `type`, `sku`, `description`, `search_keywords`, `availability_description`, `price`, `cost_price`, `retail_price`, `sale_price`, `calculated_price`, `sort_order`, `is_visible`, `is_featured`, `related_products`, `inventory_level`, `inventory_warning_level`, `warranty`, `weight`, `width`, `height`, `depth`, `fixed_cost_shipping_price`, `is_free_shipping`, `inventory_tracking`, `rating_total`, `rating_count`, `total_sold`, `brand_id`, `view_count`, `page_title`, `meta_keywords`, `meta_description`, `layout_file`, `is_price_hidden`, `price_hidden_label`, `categories`, `event_date_field_name`, `event_date_type`, `event_date_start`, `event_date_end`, `myob_asset_account`, `myob_income_account`, `myob_expense_account`, `peachtree_gl_account`, `condition`, `is_condition_shown`, `preorder_release_date`, `is_preorder_only`, `preorder_message`, `order_quantity_minimum`, `order_quantity_maximum`, `open_graph_type`, `open_graph_title`, `open_graph_description`, `is_open_graph_thumbnail`, `upc`, `avalara_product_tax_code`, `date_last_imported`, `option_set_id`, `tax_class_id`, `option_set_display`, `bin_picking_number`, `custom_url`, `availability`, `date_created`, `date_modified`, `staff_notes`) VALUES
(4, 77, 'SLCTBS', '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'physical', 'SLCTBS-3E102384', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 88, 'CC3C', 'Name : [Sample] Chemex Coffeemaker 3 Cup', 'physical', 'CC3C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 94, 'OCG', 'Name: [Sample] Oak Cheese Grater', 'physical', 'OCG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 86, 'ABS', '[Sample] Able Brewing System', 'physical', 'ABS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 104, 'OFSUC', '[Sample] Utility Caddy', 'physical', 'OFSUC', '<p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as a mop bucket too. The wood carrying handle ensures a comfortable grip when toting it from room to room.</p>\r\n<p>Measures 19 h x 36 w x 20 dia cm/7.5 h x 14.1 w x 7.8 dia in</p>', NULL, NULL, 45.95, 45.95, 45.95, 45.95, 45.95, NULL, 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 77, NULL, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'physical', 'SLCTBS', '<p>The perfect beach towel: thin, lightweight and highly absorbent. Crafted by Fog Linen in Japan using soft Lithuanian linen, each towel rolls up for compact stowaway. Dry off after a refreshing dip in the ocean and stretch out on it for a sun bath. The thinness ensures a quick dry so you can have it rolled back up in your bag without soaking your belongings.</p>\r\n<p>Measures 75 x 145 cm/29.5 x 57 in</p>\r\n<p>100% Linen</p>', '', '', 49, 0, 0, 0, 49, 0, 1, 0, '-1', 0, 0, '', 1, 0, 0, 0, 0, 0, 'sku', 0, 0, 1043, 0, 66, '', '', '', 'product.html', 0, '', 'Array', 'Delivery Date', 'none', NULL, NULL, '', '', '', '', 'New', 0, '0000-00-00 00:00:00', 0, '', 0, 0, 'product', '', '', 1, '', '', '0000-00-00 00:00:00', 14, 0, 'right', '', '/fog-linen-chambray-towel-beige-stripe/', 'available', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `layout_file` varchar(255) DEFAULT NULL,
  `parent_category_list` text,
  `image_file` text,
  `is_visible` varchar(255) DEFAULT NULL,
  `search_keywords` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `category_id`, `parent_id`, `name`, `description`, `sort_order`, `page_title`, `meta_keywords`, `meta_description`, `layout_file`, `parent_category_list`, `image_file`, `is_visible`, `search_keywords`, `url`) VALUES
(1, 18, 0, 'Bath', '', 1, '', '', '', 'category_with_facets.html', 'Array', '0', '1', '', '/bath/'),
(2, 23, 0, 'Shop All', '', 0, '', '', '', 'category_with_facets.html', 'Array', '0', '1', '', '/shop-all/');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image_file` text,
  `zoom_url` text,
  `thumbnail_url` text,
  `standard_url` text,
  `tiny_url` text,
  `is_thumbnail` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `description` text,
  `is_sample` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `image_id`, `product_id`, `image_file`, `zoom_url`, `thumbnail_url`, `standard_url`, `tiny_url`, `is_thumbnail`, `sort_order`, `description`, `is_sample`, `date_created`, `date_modified`) VALUES
(1, 265, 77, '../app/assets/img/sample_images/stencil/foglinenbeigestripetowel3b.jpg', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/265/foglinenbeigestripetowel3b.1495569971.1280.1280.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/265/foglinenbeigestripetowel3b.1495569971.500.750.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/265/foglinenbeigestripetowel3b.1495569971.190.285.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/265/foglinenbeigestripetowel3b.1495569971.60.90.jpg?c=2', '', 2, '', '1', '2017-07-21 18:50:23', NULL),
(2, 266, 77, '../app/assets/img/sample_images/stencil/foglinenbeigestripetowel1b.jpg', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/266/foglinenbeigestripetowel1b.1495206947.1280.1280.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/266/foglinenbeigestripetowel1b.1495206947.500.750.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/266/foglinenbeigestripetowel1b.1495206947.190.285.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/266/foglinenbeigestripetowel1b.1495206947.60.90.jpg?c=2', '1', 0, '', '1', '2017-07-21 18:50:23', NULL),
(3, 267, 77, '../app/assets/img/sample_images/stencil/foglinenbeigestripetowel2b.jpg', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/267/foglinenbeigestripetowel2b.1495569971.1280.1280.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/267/foglinenbeigestripetowel2b.1495569971.500.750.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/267/foglinenbeigestripetowel2b.1495569971.190.285.jpg?c=2', 'https://cdn6.bigcommerce.com/s-pbbto38xen/products/77/images/267/foglinenbeigestripetowel2b.1495569971.60.90.jpg?c=2', '', 1, '', '1', '2017-07-21 18:50:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_options`
--

CREATE TABLE `product_options` (
  `id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `product_optionsets`
--

CREATE TABLE `product_optionsets` (
  `id` int(11) NOT NULL,
  `option_set_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `product_optionset_option`
--

CREATE TABLE `product_optionset_option` (
  `id` int(11) NOT NULL,
  `option_set_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_required` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

CREATE TABLE `product_option_value` (
  `id` int(11) NOT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `is_default` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `product_product_option`
--

CREATE TABLE `product_product_option` (
  `id` int(11) NOT NULL,
  `product_option_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `display_name` text,
  `sort_order` int(11) DEFAULT NULL,
  `is_required` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `product_product_option`
--

INSERT INTO `product_product_option` (`id`, `product_option_id`, `option_id`, `display_name`, `sort_order`, `is_required`, `date_created`, `date_modified`) VALUES
(1, 108, 18, 'Size', 0, '1', NULL, NULL),
(2, 109, 3, 'Color', 0, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_sku`
--

CREATE TABLE `product_sku` (
  `id` int(11) NOT NULL,
  `product_sku_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `adjusted_price` float DEFAULT NULL,
  `cost_price` float DEFAULT NULL,
  `upc` text,
  `inventory_level` int(11) DEFAULT NULL,
  `inventory_warning_level` int(11) DEFAULT NULL,
  `bin_picking_number` text,
  `weight` varchar(255) DEFAULT NULL,
  `adjusted_weight` varchar(255) DEFAULT NULL,
  `is_purchasing_disabled` varchar(255) DEFAULT NULL,
  `purchasing_disabled_message` varchar(255) DEFAULT NULL,
  `image_file` text,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_sku_option`
--

CREATE TABLE `product_sku_option` (
  `id` int(11) NOT NULL,
  `sku_id` int(11) DEFAULT NULL,
  `product_option_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `registered_app_details`
--

CREATE TABLE `registered_app_details` (
  `id` int(24) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `store` varchar(255) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `scope` text,
  `user_id` int(24) DEFAULT NULL,
  `username` text,
  `email` varchar(255) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `account_scope` text NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_app_details`
--

INSERT INTO `registered_app_details` (`id`, `client_id`, `client_secret`, `store`, `redirect_uri`, `access_token`, `scope`, `user_id`, `username`, `email`, `context`, `account_scope`, `token`) VALUES
(1, 'atgmaj3g71r8ad6bm0ph9l8k6b29goa', '3z3pkwj5s4z5x9rzkrmfvslv8zxufay', 'pbbto38xen', 'https://automatedarmy.com/api/v1/app/install/bc', '7nmyafaf9j3pm1j02kebvlv5s8o8kkg', 'store_v2_content,store_v2_customers,store_v2_customers_login,store_v2_default,store_v2_information_read_only,store_v2_marketing,store_v2_orders,store_v2_products,store_v2_shipping,store_v2_transactions', 760925, 'leaguebapo@mailinator.com', 'leaguebapo@mailinator.com', 'stores/pbbto38xen', '', 'e83911b0b4c84b78c0ee327c8b5df15f26aaa624');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(24) NOT NULL,
  `setting` varchar(128) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting`, `value`) VALUES
(1, 'master_store', 'https://store-pbbto38xen.mybigcommerce.com'),
(2, 'client_id', 'e1t2f6jcjyuyv99r9h9rbllr53mjx8x'),
(3, 'client_secret', 'hfkmfhq4toa2tslu9x7uhtstqr5ixsu'),
(4, 'store_hash', 'pbbto38xen'),
(5, 'access_token', 'ht2kh7c30pdwzpqbzwv48dil7qnukfd'),
(6, 'username', 'llapi'),
(7, 'legacy_token', '999ac86511e6efd9ad362219679fddf9c08e754e'),
(8, 'agreement', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et arcu et augue aliquet tempor. Morbi sit amet aliquam magna. Pellentesque egestas lacinia massa vel vehicula. Maecenas sed accumsan erat. Integer non ligula sit amet sapien porttitor consequat sit amet quis nisl. Cras tincidunt, neque id euismod viverra, orci magna porta mauris, non varius lectus arcu eget justo. Sed fringilla neque sed mi consequat, quis elementum leo posuere. Vivamus finibus libero id ante consectetur iaculis. Suspendisse semper lorem in convallis feugiat. Nunc eu magna vel magna imperdiet sollicitudin quis nec justo. Vestibulum non velit sit amet turpis porta accumsan. Morbi eros lorem, suscipit nec condimentum at, finibus lacinia elit. In sagittis tempus dui, non maximus metus fringilla vel. Praesent aliquam orci eu ante rhoncus, ut laoreet ipsum tempor. Phasellus vulputate aliquet euismod. Vivamus venenatis libero eros, id congue metus rhoncus non.</p>\r\n<p>Cras viverra ultricies neque, volutpat semper orci efficitur sit amet. Quisque ac lorem est. Praesent nec sagittis velit. Proin iaculis sapien quis nibh venenatis iaculis. Aenean quis consectetur urna. Ut faucibus, ligula a blandit mollis, elit lorem vehicula massa, quis luctus enim nisi vitae dui. Nunc pulvinar enim tortor, a tincidunt turpis commodo vitae. Maecenas dui arcu, interdum a erat et, egestas luctus orci. Praesent interdum erat est, eu vestibulum elit congue a. Quisque suscipit pulvinar viverra. Praesent tempus ligula ex, ut luctus arcu mattis ac. Nunc vitae ante erat. Cras eu metus in purus posuere varius sit amet quis magna. Aenean imperdiet magna diam, et aliquet erat lobortis eget.</p>\r\n<p>Vivamus vehicula, lectus et congue laoreet, purus sapien commodo mi, aliquam facilisis nisl justo sit amet elit. Sed libero velit, convallis tempus blandit eu, rhoncus id dui. Quisque porttitor nibh felis, vel pellentesque neque lobortis vitae. Cras ligula urna, tempor eu dolor vel, aliquam tempus libero. In a porttitor odio, sit amet sollicitudin leo. Nam ante sem, blandit feugiat tellus et, imperdiet convallis lacus. Quisque hendrerit nibh in consectetur vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam auctor arcu rutrum felis suscipit tincidunt. Fusce pharetra neque sem, eget bibendum dui tincidunt ac. Integer euismod iaculis condimentum. Sed auctor sit amet nisl vehicula ornare. Pellentesque vel imperdiet mauris. Donec volutpat nec nisl at egestas. Nulla ornare congue finibus. Fusce condimentum ultrices mauris, id venenatis dui.</p>\r\n<p>Aliquam tempus non metus eu egestas. Phasellus efficitur laoreet cursus. Proin molestie convallis tortor. In hac habitasse platea dictumst. Etiam ultricies dui tempor, convallis nibh non, dictum nisl. In placerat, massa ut consectetur vehicula, lectus sem aliquam justo, a vehicula tellus est sit amet diam. Aenean a est neque. Integer nunc velit, placerat nec felis ut, sagittis malesuada nulla. Donec feugiat magna quis viverra consequat. Proin ac lobortis turpis, a vehicula sapien. Pellentesque sed vulputate velit. In hac habitasse platea dictumst. In a mattis erat.</p>\r\n<p>Proin interdum arcu eget eros ultrices, a dignissim magna consectetur. Donec commodo lacinia ligula eu cursus. Sed nibh quam, tempor ac sem sed, euismod consequat purus. Morbi nec augue metus. Aenean risus orci, tristique eget velit in, efficitur euismod ligula. Mauris et tellus turpis. Nam consectetur justo odio, ac consequat nisi pulvinar at. Sed vel porta quam. Nunc hendrerit a lacus a dignissim. Vivamus eu massa non purus cursus fringilla.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `store_management_settings`
--

CREATE TABLE `store_management_settings` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `map_percentage` int(11) NOT NULL,
  `shipping_markup` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store_management_settings`
--

INSERT INTO `store_management_settings` (`id`, `account_id`, `map_percentage`, `shipping_markup`, `date_created`, `date_modified`) VALUES
(1, 1, 5, 3, '2017-07-11 16:39:50', '2017-07-11 16:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `subscription_level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `account_id`, `subscription_level`, `price`, `discount`, `status`, `payment_type`, `date_created`) VALUES
(1, 1, 'Playoff', 79.99, 4, 'Paid', 'credit card', '2017-05-01 00:00:00'),
(2, 1, 'Contender', 39.95, 3, 'Paid', 'credit card', '2017-06-01 00:00:00'),
(3, 1, 'Championship', 99.95, 5, 'Declined', 'credit card', '2017-07-01 00:00:00'),
(4, 2, 'Championship', 99.95, 5, 'paid', 'credit card', '2017-05-01 00:00:00'),
(5, 3, 'Playoff', 79.95, 4, 'paid', 'pay pal', '2017-04-01 00:00:00'),
(6, 3, 'Contender', 79.95, 3, 'paid', 'credit card', '2017-05-01 00:00:00'),
(7, 2, 'Championship', 99.95, 5, 'Not Paid', NULL, '2017-06-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(24) NOT NULL,
  `name` varchar(64) NOT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `email_return` varchar(255) DEFAULT NULL,
  `data_feed` varchar(255) DEFAULT NULL,
  `billing_first_name` varchar(255) DEFAULT NULL,
  `billing_last_name` varchar(255) DEFAULT NULL,
  `billing_company` varchar(255) DEFAULT NULL,
  `billing_address_1` varchar(255) DEFAULT NULL,
  `billing_address_2` varchar(255) DEFAULT NULL,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zip` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_phone` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `contact_first_name`, `contact_last_name`, `contact_email`, `contact_phone`, `status`, `email_return`, `data_feed`, `billing_first_name`, `billing_last_name`, `billing_company`, `billing_address_1`, `billing_address_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_email`, `billing_phone`, `date_created`, `date_modified`, `staff_notes`) VALUES
(1, 'supplier_2', 'Amy', 'Plumber\r\n', 'example@test.com', '1233412534', 'Active', 'amy@saveyourplunk.com', 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-07 13:23:24', '2017-07-10 00:00:00', 'this is a good supplier'),
(2, 'supplier_2', 'Amy', 'Plumber\r\n', 'example@test.com', '1233412534', 'Active', 'amy@saveyourplunk.com', 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', ''),
(3, 'supplier_3', 'Water', 'Drinker', 'example@test.com', '1233412534', 'active', NULL, 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', ''),
(4, 'supplier_4', 'Dom', 'Walmer', 'example@test.com', '780-998-5589', 'active', 'amy@aveyourplunk.com', 'Bright Pearl', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-07 13:24:43', '2017-07-10 00:00:00', 'this is a good supplier'),
(5, 'supplier_5', 'Wonston', 'Fermer', 'example@test.com', '558-998-8878', 'active', NULL, 'Bright Pearl', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_location`
--

CREATE TABLE `supplier_location` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `contact_first_name` varchar(255) NOT NULL,
  `contact_last_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_location`
--

INSERT INTO `supplier_location` (`id`, `supplier_id`, `contact_first_name`, `contact_last_name`, `contact_email`, `contact_phone`, `address_1`, `address_2`, `zip`, `city`, `state`, `country`, `level`) VALUES
(3, 1, 'Bomb ', 'tester', 'test@example.com', '1-887-8879-6475', '800', 'Wingepeg', '887712', 'toronto', 'ON', 'ca', 'primary'),
(4, 1, 'Chrishtoper', 'Warmer', 'christoper@warmer.com', '1-888-9997-8874', 'frew', 'gwg', 'bgf', 'bgd', 'fw', NULL, 'secondary'),
(5, 2, 'Damin', 'Xero', 'Damin@Xero.com', '1-9998-8888-9986', '8879', 'Alberta', '22314', 'Halifax', 'mx', 'ca', 'primary'),
(6, 3, 'Erin', 'Polmer', 'erin@polmner.com', '1-8888-9878-998', '8879', 'Alberta', '22314', 'Halifax', 'mx', 'ca', 'primary'),
(7, 2, 'Damin', 'Xero', 'Damin@Xero.com', '1-9998-8888-9986', '8879', 'Alberta', NULL, 'Halifax', 'mx', 'ca', 'primary'),
(9, 1, 'greg', 'Johnson', 'greg@beapartof.com', '1-888-9878-9985', '200 yorkland blvd', 'street', NULL, 'Scarborough', 'ON', 'CA', 'primary');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_product`
--

CREATE TABLE `supplier_product` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `inventory_level` int(11) DEFAULT NULL,
  `inventory_warning_level` int(11) DEFAULT NULL,
  `sku` int(255) DEFAULT NULL,
  `price` decimal(11,4) DEFAULT NULL,
  `price_future` decimal(11,4) DEFAULT NULL,
  `price_date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_product`
--

INSERT INTO `supplier_product` (`id`, `supplier_id`, `product_id`, `inventory_level`, `inventory_warning_level`, `sku`, `price`, `price_future`, `price_date_modified`) VALUES
(1, 1, 1, 1000, 5, NULL, NULL, NULL, NULL),
(2, 2, 1, 100, 5, NULL, NULL, NULL, NULL),
(3, 3, 1, NULL, 5, NULL, NULL, NULL, NULL),
(4, 1, 4, NULL, 5, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `stock_value` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_modified` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warnings`
--

INSERT INTO `warnings` (`id`, `user_id`, `product_name`, `status`, `stock_value`, `date_created`, `date_modified`) VALUES
(1, 1, 'Pariors Cup Blue', 'active', 2, '2017-06-28', '2017-06-19'),
(2, 1, 'Wincraft NFL New York Gaints', 'Active', 4, '2017-06-19', '2017-07-07'),
(3, 1, 'Wincraft NFL Pittsburgh Steelers', 'Active', 5, '2017-06-07', '2017-07-07'),
(4, 1, '12V wireless charger', 'active', 5, '2017-06-28', '2017-06-19'),
(5, 1, '80 squqre meter mirror', 'active', 1, '2017-06-28', '2017-06-19'),
(6, 1, 'Full season tires', 'active', 6, '2017-06-28', '2017-06-19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_id` (`account_id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `account_invoice`
--
ALTER TABLE `account_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_product`
--
ALTER TABLE `account_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `management_settings`
--
ALTER TABLE `management_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Indexes for table `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Indexes for table `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_billing`
--
ALTER TABLE `order_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product_invoice`
--
ALTER TABLE `order_product_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product_return`
--
ALTER TABLE `order_product_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shipping`
--
ALTER TABLE `order_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_optionsets`
--
ALTER TABLE `product_optionsets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_option_value`
--
ALTER TABLE `product_option_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_product_option`
--
ALTER TABLE `product_product_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku_option`
--
ALTER TABLE `product_sku_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_app_details`
--
ALTER TABLE `registered_app_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_management_settings`
--
ALTER TABLE `store_management_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_location`
--
ALTER TABLE `supplier_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_product`
--
ALTER TABLE `supplier_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `account_invoice`
--
ALTER TABLE `account_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `account_product`
--
ALTER TABLE `account_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `management_settings`
--
ALTER TABLE `management_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `order_billing`
--
ALTER TABLE `order_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT for table `order_product_invoice`
--
ALTER TABLE `order_product_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_product_return`
--
ALTER TABLE `order_product_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `order_shipping`
--
ALTER TABLE `order_shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_optionsets`
--
ALTER TABLE `product_optionsets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_option_value`
--
ALTER TABLE `product_option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_product_option`
--
ALTER TABLE `product_product_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_sku_option`
--
ALTER TABLE `product_sku_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registered_app_details`
--
ALTER TABLE `registered_app_details`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `store_management_settings`
--
ALTER TABLE `store_management_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier_location`
--
ALTER TABLE `supplier_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `supplier_product`
--
ALTER TABLE `supplier_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;