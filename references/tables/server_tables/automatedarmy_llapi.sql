-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2017 at 04:59 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automatedarmy_llapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `platform` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tax_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_markup` int(11) DEFAULT NULL,
  `shipping_markup` int(11) DEFAULT NULL,
  `certificate_path` text CHARACTER SET utf8,
  `billing_first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_address1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_address2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_zip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `billing_country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active_subscription_level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `life_time_orders` int(11) DEFAULT '0',
  `life_time_sales` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `account_id`, `token`, `code`, `platform`, `name`, `contact_first_name`, `contact_last_name`, `contact_email`, `contact_phone`, `tax_id`, `currency`, `product_markup`, `shipping_markup`, `certificate_path`, `billing_first_name`, `billing_last_name`, `billing_company`, `billing_address1`, `billing_address2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `status`, `active_subscription_level`, `life_time_orders`, `life_time_sales`, `date_created`, `date_modified`, `staff_notes`) VALUES
(1, 1, 'token12345', 'code12345', 'BC', 'account_num_1', 'first name 1', 'last name 1', 'email@email.com', '1234567890', '123456', 'USD', 7, 4, 'rfrewfrewffgtrwegtwrefrewf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'good', NULL, 0, 0, '2017-05-26 00:00:00', NULL, ''),
(2, 2, 'token2222222', 'code 222222', 'SP', 'fefefr', 'afdytrjyutr', 'ghfjhgf', 'nhhgfnhgf@dfsa.com', '6543124536', '143214', 'usd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-05-24 00:00:00', NULL, ''),
(3, 3, 'dksgjfdklsgre', 'grgrtged', 'SP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-06-25 00:00:00', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `management_settings`
--

CREATE TABLE `management_settings` (
  `id` int(11) NOT NULL,
  `shipping_markup` int(11) DEFAULT NULL,
  `inventory_subtraction_value` int(11) DEFAULT NULL,
  `handling_markup` int(11) NOT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `management_settings`
--

INSERT INTO `management_settings` (`id`, `shipping_markup`, `inventory_subtraction_value`, `handling_markup`, `date_modified`) VALUES
(1, 6666, 5, 666, '2017-06-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `msg_type` varchar(255) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(255) NOT NULL,
  `entity_name` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `status`, `msg_type`, `entity_id`, `entity_type`, `entity_name`, `details`, `date_created`, `date_modified`) VALUES
(1, 1, 'active', 'Map', 1, 'account', 'John NFL ', 'Map Violation', '2017-06-29', '2017-06-29'),
(2, 1, 'active', 'Payment', 1, 'account', 'Tony NFL', 'Monthly Payment', '2017-06-29', '2017-06-29'),
(3, 1, 'active', 'Payment', 1, 'account', 'NFL Super Store', 'Payment Declined', '2017-06-29', '2017-06-29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('336d0a3f638daa9c594d638ada277f3143c5a2d7', 'test-customerStore', '10001', '2019-06-13 18:42:35', 'customerStore_payoff'),
('37f19d8fce1ffbf32d2ec3709ea19e82927e1fbb', 'test-one', '10000', '2017-06-27 18:51:08', 'master'),
('52970b7e71e0eff6f911505364db9ed6f92c88b2', 'test-one', '10000', '2019-06-13 18:42:26', 'master'),
('c69c763ce88283e4d6e2ea6836a4d09ea2a44a01', 'test-supplier', '10002', '2019-06-13 15:43:43', 'supplier'),
('e83911b0b4c84b78c0ee327c8b5df15f26aaa624', 'pbbto38xen', '1', '2019-02-16 19:03:29', 'master');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(100) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('super-master', 'W12IngJM0NCW4cHz3cFPH6k2aS', 'http://leaguelicence.dev', 'client_credentials', 'master', '1'),
('test-customerStore', 'klISF9fRM0K1Sycmbs2hbsDe0S', 'http://leaguelicence.dev', 'client_credentials', 'customerStore_payoff', '10001'),
('test-one', 'TWJBo1DI4uMF6mhB2fWLfEUQGB', 'http://leaguelicence.dev', 'client_credentials', 'master', '10000'),
('test-supplier', 'aGF4qmmrhgl0TiPXh1GbH5pVe5', 'http://leaguelicence.dev', 'client_credentials', 'supplier', '10002');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` text,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_users`
--

CREATE TABLE `oauth_users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(2000) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `origin_store_status` varchar(255) DEFAULT NULL,
  `origin_store_status_id` int(11) DEFAULT NULL,
  `subtotal_ex_tax` float DEFAULT NULL,
  `subtotal_inc_tax` float DEFAULT NULL,
  `subtotal_tax` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `profit` float DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_provider_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `refunded_amount` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `currency_exchange_rate` float DEFAULT NULL,
  `default_currency_id` int(11) DEFAULT NULL,
  `default_currency_code` varchar(255) DEFAULT NULL,
  `discount_amount` int(11) DEFAULT NULL,
  `coupon_discount` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `items_total` int(11) DEFAULT NULL,
  `items_shipped` int(11) DEFAULT NULL,
  `is_deleted` varchar(255) DEFAULT NULL,
  `geoip_country` varchar(255) DEFAULT NULL,
  `geoip_country_iso2` varchar(255) DEFAULT NULL,
  `staff_notes` text,
  `customer_message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `account_id`, `customer_name`, `status`, `origin_store_status`, `origin_store_status_id`, `subtotal_ex_tax`, `subtotal_inc_tax`, `subtotal_tax`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `profit`, `payment_method`, `payment_provider_id`, `payment_status`, `refunded_amount`, `currency_id`, `currency_code`, `currency_exchange_rate`, `default_currency_id`, `default_currency_code`, `discount_amount`, `coupon_discount`, `date_created`, `date_modified`, `items_total`, `items_shipped`, `is_deleted`, `geoip_country`, `geoip_country_iso2`, `staff_notes`, `customer_message`) VALUES
(3, 101, 1, 'testaccount testaccount', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 98.5, 98.5, 0, 93.5, 93.5, 0, NULL, 'manual payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 5, 0, '2017-07-04 15:52:29', NULL, 2, 0, '', 'Canada', 'CA', 'staff notes goes here', 'this is the customer  notes'),
(4, 110, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 56, 56, 0, 56, 56, 0, NULL, 'test', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 16:11:30', NULL, 2, 0, '', 'Canada', 'CA', 'staff notes', 'customer msg'),
(5, 109, 1, 'Robin QC Testing', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 74, 74, 0, 74, 74, 0, NULL, 'test', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 16:11:27', NULL, 2, 0, '', 'Canada', 'CA', '', ''),
(6, 108, 1, 'testaccount testaccount', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 49, 49, 0, 49, 49, 0, NULL, 'manual payment ', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:02', NULL, 1, 0, '', 'Canada', 'CA', '', ''),
(7, 107, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 2556.5, 2556.5, 0, 2556.5, 2556.5, 0, NULL, 'manual payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:10', NULL, 24, 0, '', 'Canada', 'CA', 'staff notes', 'customer notes'),
(8, 106, 1, 'michael mao', 'Awaiting Fulfillment', 'Pending', 1, 321.75, 321.75, 0, 321.75, 321.75, 0, NULL, 'test', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:16', NULL, 26, 0, '', 'Canada', 'CA', 'new msg', 'new msg'),
(9, 105, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 4298.85, 4298.85, 0, 4298.85, 4298.85, 0, NULL, 'manual payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:22', NULL, 123, 0, '', 'Canada', 'CA', '', ''),
(10, 104, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 50575, 50575, 0, 50575, 50575, 0, NULL, 'manul', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:29', NULL, 1033, 0, '', 'Canada', 'CA', 'yo man', 'hey man'),
(11, 102, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 260, 260, 0, 260, 260, 0, NULL, '', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:42', NULL, 6, 0, '', 'Canada', 'CA', 'send michael mao something', 'michael mao needs something'),
(12, 100, 1, 'Robin QC Testing', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 97.95, 97.95, 0, 97.95, 97.95, 0, NULL, 'test payment', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 14:28:51', NULL, 4, 0, '', 'Canada', 'CA', 'staff notes here', 'order comments'),
(13, 111, 1, 'michael mao', 'Awaiting Fulfillment', 'Awaiting Fulfillment', 11, 182.95, 182.95, 0, 182.95, 182.95, 0, NULL, 'this is manual', NULL, '', 0, 1, 'USD', 1, 1, 'USD', 0, 0, '2017-07-04 16:07:50', NULL, 4, 0, '', 'Canada', 'CA', 'this is the staff notes ', 'this is the customer msg');

-- --------------------------------------------------------

--
-- Table structure for table `order_billing`
--

CREATE TABLE `order_billing` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `street_1` varchar(255) DEFAULT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_iso2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_billing`
--

INSERT INTO `order_billing` (`id`, `order_id`, `account_id`, `first_name`, `last_name`, `email`, `phone`, `company`, `street_1`, `street_2`, `zip`, `city`, `state`, `country`, `country_iso2`) VALUES
(3, 101, 1, 'test_first_namemaomao', 'test_last_name', 'testing@beapartof.com', '5555555555', 'test_company', '300 yorkland', '300 yorkland', '12300', 'toronto', 'Hawaii', 'United States', 'US'),
(4, 110, 1, 'michaelmao1233', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(5, 109, 1, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(6, 108, 1, 'test_first_namemaomao', 'test_last_name', 'testing@beapartof.com', '5555555555', 'test_company', '300 yorkland', '300 yorkland', '12300', 'toronto', 'Hawaii', 'United States', 'US'),
(7, 107, 1, 'michaelmao1233', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(8, 106, 1, 'michaeleeee', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(9, 105, 1, 'michaelmao', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(10, 104, 1, 'michael', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(11, 102, 1, 'michael', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(12, 100, 1, 'Robin', 'QC Testing', 'robin@beapartof.com', '555-555-5555', 'BAPO', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(13, 111, 1, 'michaeleeee', 'mao', 'slepber@gmail.com', '6478887995', 'beapartof', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `master_product_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `base_price` float DEFAULT NULL,
  `base_cost_price` int(11) DEFAULT NULL,
  `price_ex_tax` float DEFAULT NULL,
  `price_inc_tax` float DEFAULT NULL,
  `price_tax` float DEFAULT NULL,
  `base_total` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `fulfillment` varchar(255) DEFAULT NULL,
  `cost_price_inc_tax` float DEFAULT NULL,
  `cost_price_ex_tax` float DEFAULT NULL,
  `cost_price_tax` float DEFAULT NULL,
  `is_refunded` varchar(255) DEFAULT NULL,
  `quantity_refunded` int(11) DEFAULT NULL,
  `refund_amount` float DEFAULT NULL,
  `quantity_shipped` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `shipping_id`, `supplier_id`, `account_id`, `product_id`, `master_product_id`, `return_id`, `name`, `sku`, `type`, `base_price`, `base_cost_price`, `price_ex_tax`, `price_inc_tax`, `price_tax`, `base_total`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `weight`, `quantity`, `fulfillment`, `cost_price_inc_tax`, `cost_price_ex_tax`, `cost_price_tax`, `is_refunded`, `quantity_refunded`, `refund_amount`, `quantity_shipped`) VALUES
(31, 108, 11, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(32, 107, 9, NULL, 1, 80, NULL, 0, '[Sample] Orbit Terrarium - Large', 'OTL', 'physical', 109, 0, 109, 109, 0, 2507, 2507, 2507, 0, 1, 23, 'External Product', 0, 0, 0, '', 0, 0, 0),
(33, 107, 10, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 49.5, 49.5, 49.5, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(34, 106, 8, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 174.75, 174.75, 174.75, 0, 1, 5, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(35, 106, 8, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-0BAF36BA', 'physical', 7, 0, 7, 7, 0, 147, 147, 147, 0, 1, 21, 'External Product', 0, 0, 0, '', 0, 0, 0),
(36, 105, 7, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 4298.85, 4298.85, 4298.85, 0, 1, 123, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(37, 104, 6, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-D334BA28', 'physical', 7, 0, 7, 7, 0, 7, 7, 7, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(38, 104, 6, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 50568, 50568, 50568, 0, 1, 1032, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(39, 102, 4, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-20D88EFC', 'physical', 7, 0, 7, 7, 0, 35, 35, 35, 0, 1, 5, 'External Product', 0, 0, 0, '', 0, 0, 0),
(40, 102, 4, 1, 1, 86, 4, 0, '[Sample] Able Brewing System', 'ABS', 'physical', 225, 0, 225, 225, 0, 225, 225, 225, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(41, 100, 1, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-0BAF36BA', 'physical', 7, 0, 7, 7, 0, 14, 14, 14, 0, 1, 2, 'External Product', 0, 0, 0, '', 0, 0, 0),
(42, 100, 1, 1, 1, 94, 3, 0, '[Sample] Oak Cheese Grater', 'OCG', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(43, 100, 1, NULL, 1, 77, NULL, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-CCAEAE80', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(48, 101, 2, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 49.5, 49.5, 49.5, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(49, 101, 3, NULL, 1, 77, NULL, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-B6062BD4', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(50, 111, 14, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(51, 111, 14, 1, 1, 88, 2, 0, '[Sample] Chemex Coffeemaker 3 Cup', 'CC3C', 'physical', 49.5, 0, 49.5, 49.5, 0, 99, 99, 99, 0, 1, 2, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(52, 111, 15, NULL, 1, 107, NULL, 0, '[Sample] Dustpan & Brush', 'DPB', 'physical', 34.95, 0, 34.95, 34.95, 0, 34.95, 34.95, 34.95, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(53, 109, 12, NULL, 1, 111, NULL, 0, '[Sample] Smith Journal 13', 'SM13', 'physical', 25, 0, 25, 25, 0, 25, 25, 25, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0),
(54, 109, 12, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(55, 110, 13, 1, 1, 77, 1, 0, '[Sample] Fog Linen Chambray Towel - Beige Stripe', 'SLCTBS-3E102384', 'physical', 49, 0, 49, 49, 0, 49, 49, 49, 0, 1, 1, 'Awaiting Fulfillment', 0, 0, 0, '', 0, 0, 0),
(56, 110, 13, NULL, 1, 93, NULL, 0, '[Sample] 1 L Le Parfait Jar', 'SLLPJ-CD28D4F0', 'physical', 7, 0, 7, 7, 0, 7, 7, 7, 0, 1, 1, 'External Product', 0, 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_invoice`
--

CREATE TABLE `order_product_invoice` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `base_cost_price` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `shipping_cost_ex_tax` float DEFAULT NULL,
  `shipping_cost_inc_tax` float DEFAULT NULL,
  `shipping_cost_tax` float DEFAULT NULL,
  `shipping_method` varchar(255) DEFAULT NULL,
  `shipping_provider` varchar(255) DEFAULT NULL,
  `tracking_carrier` varchar(255) DEFAULT NULL,
  `date_shipped` datetime DEFAULT NULL,
  `profit` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product_invoice`
--

INSERT INTO `order_product_invoice` (`id`, `order_id`, `supplier_id`, `account_id`, `product_id`, `sku`, `quantity`, `customer_name`, `base_cost_price`, `selling_price`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `tracking_number`, `shipping_cost_ex_tax`, `shipping_cost_inc_tax`, `shipping_cost_tax`, `shipping_method`, `shipping_provider`, `tracking_carrier`, `date_shipped`, `profit`, `status`, `create_date`, `update_date`) VALUES
(1, 1, 1, 1, 1, 'fwfrew', 12, 'christ', NULL, NULL, 1233.63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12331200, NULL, NULL, NULL),
(2, 1, 1, 1, 13, 'frefrew', NULL, NULL, NULL, NULL, 432424, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 342.3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_return`
--

CREATE TABLE `order_product_return` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sku` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total_ex_tax` float DEFAULT NULL,
  `total_inc_tax` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product_return`
--

INSERT INTO `order_product_return` (`id`, `order_id`, `account_id`, `supplier_id`, `sku`, `name`, `customer_name`, `quantity`, `total_ex_tax`, `total_inc_tax`, `total_tax`, `reason`, `type`, `status`, `date_created`, `date_modified`) VALUES
(1, 1, 1, 1, 'frewfrew', NULL, 'passico', NULL, 3123, NULL, NULL, NULL, NULL, 'ferfef', NULL, NULL),
(2, 2, 1, 3, 'fwf', NULL, 'piano', NULL, 444.88, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping`
--

CREATE TABLE `order_shipping` (
  `id` int(11) NOT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `street_1` varchar(255) DEFAULT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_iso2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_shipping`
--

INSERT INTO `order_shipping` (`id`, `shipping_id`, `order_id`, `account_id`, `first_name`, `last_name`, `company`, `email`, `phone`, `street_1`, `street_2`, `zip`, `city`, `state`, `country`, `country_iso2`) VALUES
(25, 11, 108, 1, 'test_first_namemaomao', 'test_last_name', 'test_company', 'testing@beapartof.com', '5555555555', '300 yorkland', '300 yorkland', '12300', 'toronto', 'Hawaii', 'United States', 'US'),
(26, 9, 107, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(27, 10, 107, 1, 'michaelmaomao', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(28, 8, 106, 1, 'michaelmaomao', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(29, 7, 105, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(30, 6, 104, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(31, 4, 102, 1, 'michael', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(32, 1, 100, 1, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(35, 2, 101, 1, 'test2_first_name', 'test2_first_name', 'test2_company', 'testing@beapartof.com', '6478893342', 'test 1 address 1', 'test 1 address 1', 'M1J0A9', 'toronto', 'Ontario', 'Canada', 'CA'),
(36, 3, 101, 1, 'e23e23e2', 'e32e23', 'r43r432456', 'testing@beapartof.com', '4559123456', 'hghjhrr', 'grtw', 'm8J2a4', 'toronto', 'Ontario', 'Canada', 'CA'),
(37, 14, 111, 1, 'michaeleeee', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '200 ', '', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(38, 15, 111, 1, 'michaelmao1233', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA'),
(39, 12, 109, 1, 'Robin', 'QC Testing', 'BAPO', 'robin@beapartof.com', '555-555-5555', 'Address', '', '10162', 'New York', 'New York', 'United States', 'US'),
(40, 13, 110, 1, 'michaelmao1233', 'mao', 'beapartof', 'slepber@gmail.com', '6478887995', '20000', 'frewfrefw', 'M2T6S9', 'toronto', 'Ontario', 'Canada', 'CA');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `manufactory_sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `description` text,
  `search_keywords` text,
  `availability_description` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `cost_price` float DEFAULT NULL,
  `retail_price` float DEFAULT NULL,
  `sale_price` float DEFAULT NULL,
  `calculated_price` float DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT NULL,
  `related_products` varchar(255) DEFAULT NULL,
  `inventory_level` int(11) DEFAULT NULL,
  `inventory_warning_level` int(11) DEFAULT NULL,
  `warranty` text,
  `weight` float DEFAULT NULL,
  `width` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `depth` float DEFAULT NULL,
  `fixed_cost_shipping_price` float DEFAULT NULL,
  `is_free_shipping` tinyint(1) DEFAULT NULL,
  `inventory_tracking` varchar(255) DEFAULT NULL,
  `rating_total` int(11) DEFAULT NULL,
  `rating_count` int(11) DEFAULT NULL,
  `total_sold` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `layout_file` varchar(255) DEFAULT NULL,
  `is_price_hidden` tinyint(1) DEFAULT NULL,
  `price_hidden_label` varchar(255) DEFAULT NULL,
  `categories` text,
  `event_date_field_name` varchar(255) DEFAULT NULL,
  `event_date_type` varchar(255) DEFAULT NULL,
  `event_date_start` datetime DEFAULT NULL,
  `event_date_end` datetime DEFAULT NULL,
  `myob_asset_account` varchar(255) DEFAULT NULL,
  `myob_income_account` varchar(255) DEFAULT NULL,
  `myob_expense_account` varchar(255) DEFAULT NULL,
  `peachtree_gl_account` varchar(255) DEFAULT NULL,
  `condition` varchar(255) DEFAULT NULL,
  `is_condition_shown` tinyint(1) DEFAULT NULL,
  `preorder_release_date` datetime DEFAULT NULL,
  `is_preorder_only` tinyint(1) DEFAULT NULL,
  `preorder_message` varchar(255) DEFAULT NULL,
  `order_quantity_minimum` int(11) DEFAULT NULL,
  `order_quantity_maximum` int(11) DEFAULT NULL,
  `open_graph_type` varchar(255) DEFAULT NULL,
  `open_graph_title` varchar(255) DEFAULT NULL,
  `open_graph_description` text,
  `is_open_graph_thumbnail` tinyint(1) DEFAULT NULL,
  `upc` varchar(255) DEFAULT NULL,
  `date_last_imported` datetime DEFAULT NULL,
  `option_set_id` int(11) DEFAULT NULL,
  `tax_class_id` int(11) DEFAULT NULL,
  `option_set_display` varchar(255) DEFAULT NULL,
  `bin_picking_number` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_id`, `manufactory_sku`, `name`, `type`, `sku`, `description`, `search_keywords`, `availability_description`, `price`, `cost_price`, `retail_price`, `sale_price`, `calculated_price`, `sort_order`, `is_visible`, `is_featured`, `related_products`, `inventory_level`, `inventory_warning_level`, `warranty`, `weight`, `width`, `height`, `depth`, `fixed_cost_shipping_price`, `is_free_shipping`, `inventory_tracking`, `rating_total`, `rating_count`, `total_sold`, `brand_id`, `view_count`, `page_title`, `meta_keywords`, `meta_description`, `layout_file`, `is_price_hidden`, `price_hidden_label`, `categories`, `event_date_field_name`, `event_date_type`, `event_date_start`, `event_date_end`, `myob_asset_account`, `myob_income_account`, `myob_expense_account`, `peachtree_gl_account`, `condition`, `is_condition_shown`, `preorder_release_date`, `is_preorder_only`, `preorder_message`, `order_quantity_minimum`, `order_quantity_maximum`, `open_graph_type`, `open_graph_title`, `open_graph_description`, `is_open_graph_thumbnail`, `upc`, `date_last_imported`, `option_set_id`, `tax_class_id`, `option_set_display`, `bin_picking_number`, `custom_url`, `availability`, `date_created`, `date_modified`, `staff_notes`) VALUES
(4, 1, 'SLCTBS-3E102384', '[Sample] Fog Linen Chambray Towel - Beige Stripe', NULL, 'SLCTBS-3E102384', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 2, 'CC3C', 'Name : [Sample] Chemex Coffeemaker 3 Cup', NULL, 'CC3C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, 'OCG', 'Name: [Sample] Oak Cheese Grater', NULL, 'OCG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 4, 'ABS', '[Sample] Able Brewing System', NULL, 'ABS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `registered_app_details`
--

CREATE TABLE `registered_app_details` (
  `id` int(24) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `store` varchar(255) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `scope` text,
  `user_id` int(24) DEFAULT NULL,
  `username` text,
  `email` varchar(255) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `account_scope` text NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_app_details`
--

INSERT INTO `registered_app_details` (`id`, `client_id`, `client_secret`, `store`, `redirect_uri`, `access_token`, `scope`, `user_id`, `username`, `email`, `context`, `account_scope`, `token`) VALUES
(1, 'atgmaj3g71r8ad6bm0ph9l8k6b29goa', '3z3pkwj5s4z5x9rzkrmfvslv8zxufay', 'pbbto38xen', 'https://leaguelicence.dev/api/v1/app/install/bc', '7nmyafaf9j3pm1j02kebvlv5s8o8kkg', 'store_v2_content,store_v2_customers,store_v2_customers_login,store_v2_default,store_v2_information_read_only,store_v2_marketing,store_v2_orders,store_v2_products,store_v2_shipping,store_v2_transactions', 760925, 'leaguebapo@mailinator.com', 'leaguebapo@mailinator.com', 'stores/pbbto38xen', '', 'e83911b0b4c84b78c0ee327c8b5df15f26aaa624');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(24) NOT NULL,
  `setting` varchar(128) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting`, `value`) VALUES
(1, 'master_store', 'https://store-pbbto38xen.mybigcommerce.com'),
(2, 'client_id', 'e1t2f6jcjyuyv99r9h9rbllr53mjx8x'),
(3, 'client_secret', 'hfkmfhq4toa2tslu9x7uhtstqr5ixsu'),
(4, 'store_hash', 'pbbto38xen'),
(5, 'access_token', 'ht2kh7c30pdwzpqbzwv48dil7qnukfd'),
(6, 'username', 'llapi'),
(7, 'legacy_token', '999ac86511e6efd9ad362219679fddf9c08e754e');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `subscription_level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `account_id`, `subscription_level`, `price`, `discount`, `status`, `payment_type`, `date_created`) VALUES
(1, 1, 'payoff', 99.99, 5, 'Paid', 'credit card', '2017-06-14 00:00:00'),
(2, 1, 'contender', 79.99, 2, 'Paid', 'credit card', '2017-06-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(24) NOT NULL,
  `name` varchar(64) NOT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `email_return` varchar(255) DEFAULT NULL,
  `data_feed` varchar(255) DEFAULT NULL,
  `billing_first_name` varchar(255) DEFAULT NULL,
  `billing_last_name` varchar(255) DEFAULT NULL,
  `billing_company` varchar(255) DEFAULT NULL,
  `billing_address_1` varchar(255) DEFAULT NULL,
  `billing_address_2` varchar(255) DEFAULT NULL,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zip` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_phone` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `staff_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `contact_first_name`, `contact_last_name`, `contact_email`, `contact_phone`, `status`, `email_return`, `data_feed`, `billing_first_name`, `billing_last_name`, `billing_company`, `billing_address_1`, `billing_address_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_email`, `billing_phone`, `date_created`, `date_modified`, `staff_notes`) VALUES
(1, 'supplier_1', 'doson', 'mat', 'example@test.com', '1233412534', 'active', NULL, 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', 'this is a good supplier'),
(2, 'supplier_2', 'doson', 'mat', 'example@test.com', '1233412534', 'active', NULL, 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', ''),
(3, 'supplier_3', 'doson', 'mat', 'example@test.com', '1233412534', 'active', NULL, 'dadasdasdf', 'greg', 'johnson', 'beapartof', '200', 'yorkland blvd', 'north york', 'on', 'x4d 4s3', 'on', 'john@bepartof.com', '1-888-798-98798', '2017-07-01 00:00:00', '2017-07-10 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_location`
--

CREATE TABLE `supplier_location` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_location`
--

INSERT INTO `supplier_location` (`id`, `supplier_id`, `address1`, `address2`, `zip`, `city`, `state`, `country`, `status`) VALUES
(3, 1, '800', 'Wingepeg', '887712', 'toronto', 'ON', 'ca', 'primary'),
(4, 1, 'frew', 'gwg', 'bgf', 'bgd', 'fw', NULL, 'secondary'),
(5, 2, 'grwegw', NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_product`
--

CREATE TABLE `supplier_product` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `inventory_level` int(11) DEFAULT NULL,
  `inventory_warning_level` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` decimal(11,4) DEFAULT NULL,
  `price_future` decimal(11,4) DEFAULT NULL,
  `price_date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_product`
--

INSERT INTO `supplier_product` (`id`, `supplier_id`, `product_id`, `inventory_level`, `inventory_warning_level`, `sku`, `price`, `price_future`, `price_date_modified`) VALUES
(1, 1, 1, 1000, 5, NULL, NULL, NULL, NULL),
(2, 2, 1, 100, 5, NULL, NULL, NULL, NULL),
(3, 3, 1, NULL, 5, NULL, NULL, NULL, NULL),
(4, 1, 4, NULL, 5, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `stock_value` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_modified` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warnings`
--

INSERT INTO `warnings` (`id`, `user_id`, `product_name`, `status`, `stock_value`, `date_created`, `date_modified`) VALUES
(1, 1, 'Pariors Cup Blue', 'active', 2, '2017-06-28', '2017-06-19'),
(2, 1, 'Wincraft NFL New York Gaints', 'active', 4, '2017-06-19', '2017-06-13'),
(3, 1, 'Wincraft NFL Pittsburgh Steelers', 'active', 5, '2017-06-07', '2017-06-03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_id` (`account_id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `management_settings`
--
ALTER TABLE `management_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Indexes for table `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Indexes for table `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_billing`
--
ALTER TABLE `order_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product_invoice`
--
ALTER TABLE `order_product_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product_return`
--
ALTER TABLE `order_product_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shipping`
--
ALTER TABLE `order_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`product_id`),
  ADD UNIQUE KEY `manufactory_sku` (`manufactory_sku`);

--
-- Indexes for table `registered_app_details`
--
ALTER TABLE `registered_app_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_location`
--
ALTER TABLE `supplier_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_product`
--
ALTER TABLE `supplier_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `management_settings`
--
ALTER TABLE `management_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `order_billing`
--
ALTER TABLE `order_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `order_product_invoice`
--
ALTER TABLE `order_product_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_product_return`
--
ALTER TABLE `order_product_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_shipping`
--
ALTER TABLE `order_shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `registered_app_details`
--
ALTER TABLE `registered_app_details`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(24) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `supplier_location`
--
ALTER TABLE `supplier_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier_product`
--
ALTER TABLE `supplier_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
