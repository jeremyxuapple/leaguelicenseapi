<?php
/**
 *  Base LL Api Init file
 */

//define some basics
error_reporting(E_ALL ^ E_NOTICE || E_STRICT);
error_reporting(E_ALL);
ini_set('display_errors', 0);

session_start();
date_default_timezone_set('America/New_York');
ini_get('date.timezone');

//lets define some constants
define('BASE_PATH', dirname(__FILE__));
define('APP_PATH', dirname(__FILE__)."/app");
define('ADMIN_APP_PATH', dirname(__FILE__)."/app/admin");
define('STORE_PATH', 'https://automatedarmy.com');
define('APP_URL', 'https://automatedarmy.com/app');
define('ADMIN_APP_URL', 'https://automatedarmy.com/app/admin');
define('MASTER_STORE', "http://league-licence-master-store4.mybigcommerce.com");

# define dev environment
define('DEV', true);

# database definitions
include('./config.php');

# Database failed in unexpected manner, should
# only be raised on logic errors but may also
# need raising if the DB connection drops, etc
class DatabaseError extends \Exception {}
# The integrity of the database is violated
# by something missing or corrupt; should never
# come up, but if it does it should be addressed
class IntegrityError extends \Exception {}

# include composer dependencies
require_once BASE_PATH . '/vendor/autoload.php';

/**
 * Register the autoloader for the classes.
 *
 * @param string $class The fully-qualified class name.
 *
 * @return void
 */
spl_autoload_register(function ($class) {
    # replace all the wrong slashes
    $class = str_replace('\\', '/', $class);

    # this is only the autoload for LL classses
    $match = preg_match("/LL\//", $class);

    if ($match != 1) {
        return false;
        //RD($class);
        //exit();
    }
        

    # remove the base namespace
    $class = str_replace(array("LL/"), "", $class);

    $path = BASE_PATH."/$class.php";

    if (file_exists($path)) {
        require_once($path);
    }
    else {
        throw new \Exception("Unable to load class: $path: $class : ".BASE_PATH);
    }
});

/**
 * Simple function to calculate difference between two dates
 *
 * @param integer $start starting timestamp
 * @param integer $end ending timestamp
 *
 * @return boolean return true if current time is between start and end
 */
function BetweenDates($start, $end) {
    if (
        //start date
        ($start == 0 || $start > 0 && $start < time()) &&
        //end date
        ($end == 0 || $end > 0 && $end > time())
    ) {
        return true;
    }

    return false;
}

/**
 * Simple print function, mostly for arrays
 *
 * @param string or array to be printed
 *
 * @return void - print's out something
 */
function RD($p) {
    echo "<pre>",print_r($p),"</pre>";
}

/**
 * Basic curl function
 *
 * @param string $url - the url we wish to curl
 * @param array $post - post values as an array if available
 *
 * @return repsonse - response we get from the server
 *
 */
function Curl($url, $post=false, $headers = false)
{
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($headers)
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    // $output contains the output string
    $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);
    return $output;
}

/**
 * Output the data into a json readable string
 *
 * @param array $data - converts the array to a compressed readable json string
 */
function JSO($data)
{
    if (isset($_SERVER['HTTP_ACCEPT_ENCODING'])){
        if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
        header('Content-type: application/json');
        header('Content-Encoding: gzip');
        echo json_encode($data);
        exit();
    }else{
        exit();
    }
}

/**
 * Will check if string is an email
 *
 * @param $email - the email string
 *
 * @return boolean
 *
 */
function isEmail($string)
{
    if (filter_var($string, FILTER_VALIDATE_EMAIL))
        return true;
    else
        return false;
}

/**
 * This will convery javascript datetimes to php datetimes and vice versa
 *
 * @param $time - the time string that needs to be converted
 * @param $reverse - if true will convert a php timestamp to a javascript readable time string
 * 
 * @return $totime - the time string that you want
 *
 */
function Timeconvert($time, $reverse = false)
{
    if ($reverse) {
        if ($time == "" || $time == 0) {
            $totime = "";
        } else {
            $totime = date('D M d Y H:i:s O', $time);
        }
    } else {
        /*if ($time == 0)
            return "";*/

        $replace = strstr($time, "GMT");
        $time = str_replace($replace, "", $time);
        $totime = strtotime($time);
    }

    return $totime;
}

/**
 * Calculate the days between two php timestamps
 *
 * @param $start timestamp
 * @param $end timestamp
 *
 * @return $days - number of workable days between the two time periods
 */
function calculateWorkingDateInterval($start, $end) {
    $day = $start;
    $days = 0;
    while($day <= $end) {
        $today = date('D', $day);

        # increment the amount of days if it's not saturday or sunday
        if ($day != 'Sat' && $day != 'Sun') {
            $days++;
        }

        # increment the day by 24 hours
        $day += 60*60*24;
    }

    return $days;
}

/**
 * Generates a random string
 *
 * @param $length - the length of the desired string
 *
 * @return $randomString - the random string
 *
 */
function generateRandomString($length = 16)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}